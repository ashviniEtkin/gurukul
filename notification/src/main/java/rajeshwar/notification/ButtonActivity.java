package rajeshwar.notification;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.notification.Notification;

import java.util.ArrayList;
import java.util.List;


public class ButtonActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
    }

    public  void m1(View view)
    {

        List<String> regids=new ArrayList<>();
        regids.add("APA91bGDeMkbMAEMSjtU8N8vKwJemHjSwRKXFIQAekRGD3aL4c9EG2iBBx0uQdRiBUaa_jdCG65WUQ65tQ9wH83-N4JBPB7aeCp0fcGr-piYomjfYkAd41bmn6-FGb-MmILFQNP5e_UdegIdIp2V_7ZEhmMwJm9DOw");
        regids.add("APA91bHzIibIrnmGdDL7nbTO1Q4Bwoa9SSbjl_Nb7ANBE6uDGeAMk4Ty226NIUJfo4KVPTUa2euPKPFpox1cJxkAH4GeM7URU7AAdkH81mLXXnDNMOZ4MqQTKgU7l4XseL3GgrY4YZqesDO7rt8JU6XwJaD7eLA5BQ");

        Notification.sendNotification(regids,"hi hello","Inbox");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
