package com.etkin.gurukul;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/8/2015.
 */
public class FeedbackAdapter  extends RecyclerView.Adapter<FeedbackAdapter.FeedbackViewHolder> {

    private ArrayList<Feedback_Pojo> feedbackData;
    public static Context con;

    public FeedbackAdapter(Context context,ArrayList<Feedback_Pojo> feedbackData) {
        //this.cardData = cardData;
        this.feedbackData=feedbackData;
        con=context;

    }

    @Override
    public FeedbackAdapter.FeedbackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.feedback_list_row, parent, false);
        return new FeedbackViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final FeedbackAdapter.FeedbackViewHolder holder, int i) {

        final Feedback_Pojo feedback_pojo=feedbackData.get(i);


        holder.date.setText(feedback_pojo.getDate());
        holder.title.setText(feedback_pojo.getTitle());
        holder.by.setText(feedback_pojo.getFeedbackBy());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Feedback_List.class);
                intent.putExtra("by", feedback_pojo.getFeedbackBy());
                intent.putExtra("date", feedback_pojo.getDate());
                intent.putExtra("title", feedback_pojo.getTitle());
                intent.putExtra("description", feedback_pojo.getDescription());
                holder.context.startActivity(intent);

            }
        });
        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return feedbackData.size();
    }

    public static class FeedbackViewHolder extends RecyclerView.ViewHolder {

        protected TextView date,title,by;
        private Context context;

        public FeedbackViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            date=(TextView)itemView.findViewById(R.id.date);
            title=(TextView)itemView.findViewById(R.id.feedbackTitle);
            by=(TextView)itemView.findViewById(R.id.feedbackBy);

        }
    }
}
