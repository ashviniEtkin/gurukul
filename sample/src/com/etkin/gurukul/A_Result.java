package com.etkin.gurukul;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class A_Result extends Fragment {

    RippleView Result;
    private int Color;
    ConnectionDetector cd;

    public A_Result() {
        // Required empty public constructor
    }

    public void A_Result_color(int color){
        Color=color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_a__result, container, false);
        cd=new ConnectionDetector(getActivity());
        Result=(RippleView)root.findViewById(R.id.result);
        Result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(), "You seem to be offline !", Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_Result_Page.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });


        return root;
    }


}
