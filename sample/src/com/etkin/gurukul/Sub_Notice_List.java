package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Sub_Notice_List extends ActionBarActivity {

    TextView date,noticeFor,noticeTitle,noticeDescription;
    String Date,NoticeFor,NoticeTitle,NoticeDescription,id;
    Button Verify,Update,Delete,Create;
    ProgressDialog pb,pb1;
    String verify,delete,update;
    public static ActionBarActivity sub_notice_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__notice__list);

        pb=new ProgressDialog(Sub_Notice_List.this);
        pb1=new ProgressDialog(Sub_Notice_List.this);
        sub_notice_list=this;

        date=(TextView)findViewById(R.id.date);
        noticeFor=(TextView)findViewById(R.id.noticeFor);
        noticeTitle=(TextView)findViewById(R.id.noticeTitle);
        noticeDescription=(TextView)findViewById(R.id.noticeDescription);

        Verify=(Button)findViewById(R.id.verify);
        Update=(Button)findViewById(R.id.update);
        Delete=(Button)findViewById(R.id.delete);
        Create=(Button)findViewById(R.id.create);

        Intent intent=getIntent();
        id=intent.getStringExtra("srNo");
        Date=intent.getStringExtra("date");
        NoticeFor=intent.getStringExtra("noticeFor");
        NoticeTitle=intent.getStringExtra("title");
        NoticeDescription=intent.getStringExtra("description");

        date.setText(Date);
        noticeFor.setText(NoticeFor);
        noticeTitle.setText(NoticeTitle);
        noticeDescription.setText(NoticeDescription);

        Verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VerifyNotice().execute(getString(R.string.AdminIp)+"/VerifyNotice", id);
            }
        });

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteNotice().execute(getString(R.string.AdminIp)+"/DeleteNotice",id);
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String date1,noticeFor1,noticeTitle1,noticeDescription1;
                date1=date.getText().toString().trim();
                noticeFor1=noticeFor.getText().toString().trim();
                noticeTitle1=noticeTitle.getText().toString().trim();
                noticeDescription1=noticeDescription.getText().toString().trim();
                new UpdateNotice().execute(getString(R.string.AdminIp)+"/UpdateNotice",date1,noticeFor1,noticeTitle1,noticeDescription1,id);
            }
        });

        Create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Sub_Notice_List.this,A_Create_Notice.class);
                startActivity(intent);

            }
        });


    }

    public class VerifyNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                verify = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                verify = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (verify.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (verify.equals("1")) {

                Toast.makeText(getApplicationContext(), "Notice verified successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Notice_List.this, A_Notice_List.class);
                startActivity(intent1);
                A_Notice_List.notice_list.finish();
                Sub_Notice_List.sub_notice_list.finish();

            }
        }
    }

    public class DeleteNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog1();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog1();

            if (delete.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (delete.equals("1")) {

                Toast.makeText(getApplicationContext(), "Notice deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Notice_List.this, A_Notice_List.class);
                startActivity(intent1);
                A_Notice_List.notice_list.finish();
                Sub_Notice_List.sub_notice_list.finish();


            }
        }
    }


    public class UpdateNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog2();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("noticeFor", params[2]));
                nameValuePairList.add(new BasicNameValuePair("noticeTitle", params[3]));
                nameValuePairList.add(new BasicNameValuePair("description", params[4]));
                nameValuePairList.add(new BasicNameValuePair("id", params[5]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                update = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                update = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog2();

            if (update.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (update.equals("1")) {

                Toast.makeText(getApplicationContext(), "Notice updated successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Notice_List.this, A_Notice_List.class);
                startActivity(intent1);
                A_Notice_List.notice_list.finish();
                Sub_Notice_List.sub_notice_list.finish();
            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Verifying Notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

    public void showProgressDialog1() {

        pb.setMessage("Deleting Notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog1() {

        pb.dismiss();
    }

    public void showProgressDialog2() {

        pb.setMessage("Updating Notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog2() {

        pb.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__notice__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
