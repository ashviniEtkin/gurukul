package com.etkin.gurukul;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class TestSubject extends Fragment {
    RippleView test;
    private int Color;
    ConnectionDetector cd;
    EditText data;
    String output;
    ProgressDialog pg;

    public TestSubject() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_test_subject, container, false);
        cd=new ConnectionDetector(getActivity());
        data= (EditText) root.findViewById(R.id.testSubject);
        test=(RippleView)root.findViewById(R.id.add);
        pg = new ProgressDialog(getActivity());
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Please wait....");
        pg.setCancelable(false);

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(), "You seem to be offline !", Toast.LENGTH_SHORT).show();
                }else if (data.length()==0){
                    Toast.makeText(getActivity(), "Enter test subject first !", Toast.LENGTH_SHORT).show();
                }else {
                    new AddTest().execute(getString(R.string.AdminIp) + "/EnterTestSubject", data.getText().toString().trim());
                }
            }
        });

        return root;
    }
    public class AddTest extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }


        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("testSubject", params[1]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                output = data;
                String s="a";
            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {
                output = "6";
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return output;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pg.dismiss();
            if (output.equals("1")) {
                Toast.makeText(getActivity(), "Test Subject Added Successfully...", Toast.LENGTH_LONG).show();

            } else if(output.equals("2")){

                Toast.makeText(getActivity(), "This test subject already exist...", Toast.LENGTH_LONG).show();

            }
        }


    }

}





