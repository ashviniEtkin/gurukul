package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_NewBatch extends ActionBarActivity {

    Button course,subject,launch,standard,faculty;
    ProgressDialog pg;
    ArrayList<String> crslist ;
    ArrayList<String> classlist ;
    ArrayList<String> facultyList;
    String crsnm,clsnm,subnm,Faculty;
    String [] STANDARD;
    String [] SUBJECT;
    String [] COURSE;
    String [] FACULTY;
    public String COURSE1,SUBJECT1,STANDARD1,FACULTY1;
    ArrayList<String> sublist ;
    EditText fees,time;
    String output;
    public static ActionBarActivity new_batch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__new_batch);
        course=(Button)findViewById(R.id.course);
        subject=(Button)findViewById(R.id.subject);
        standard=(Button)findViewById(R.id.standard);
        launch=(Button)findViewById(R.id.create);
        faculty=(Button)findViewById(R.id.faculty);
        fees=(EditText)findViewById(R.id.fees);
        time=(EditText)findViewById(R.id.time);
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);
        new_batch=this;

        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CoursesList().execute(getString(R.string.AdminIp)+"/GetCourseName");
            }
        });

        standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ClassList().execute(getString(R.string.AdminIp)+"/GetClass");
            }
        });

        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stan1,cour1;
                stan1=standard.getText().toString().trim();
                cour1=course.getText().toString().trim();
                new SubjectList().execute(getString(R.string.AdminIp)+"/GetSubject",stan1,cour1);
            }
        });

        faculty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FacultyList().execute(getString(R.string.AdminIp)+"/FacultyList");
            }
        });

        launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Standard,batchName,fees1,time1,course1,faculty1;
                course1=course.getText().toString().trim();
                Standard=standard.getText().toString().trim();
                batchName=subject.getText().toString().trim();
                fees1=fees.getText().toString().trim();
                time1=time.getText().toString().trim();
                faculty1=faculty.getText().toString().toLowerCase().trim();
                new LaunchBatch().execute(getString(R.string.AdminIp)+"/LaunchBatch",Standard,course1,batchName,fees1,time1,faculty1);

            }
        });


    }

    public  class CoursesList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONArray jarray = new JSONArray(data);
                    crslist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        crsnm = jrealobj.getString("course");

                        crslist.add(crsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectCoursesDialog();
            }
        }

    }

    private void showSelectCoursesDialog() {

        COURSE=new String[crslist.size()];
        COURSE=crslist.toArray(COURSE);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course....");
        ad1.setSingleChoiceItems(COURSE, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                COURSE1=COURSE[arg1].toString();
                course.setText(COURSE1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }

    public  class SubjectList extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... params)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("standard", params[1]));
            nameValuePairList.add(new BasicNameValuePair("coursename", params[2]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jarray = new JSONArray(data);
                    sublist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        subnm = jrealobj.getString("subject");

                        sublist.add(subnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();;
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectSubhectDialog();
            }
        }

    }

    private void showSelectSubhectDialog() {


        SUBJECT=new String[sublist.size()];
        SUBJECT=sublist.toArray(SUBJECT);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course....");
        ad1.setSingleChoiceItems(SUBJECT, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                SUBJECT1=SUBJECT[arg1].toString();
                subject.setText(SUBJECT1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }

    public  class ClassList extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONArray jarray = new JSONArray(data);

                    classlist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        clsnm = jrealobj.getString("standard");

                        classlist.add(clsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectClassDialog();
            }
        }

    }

    private void showSelectClassDialog() {

        STANDARD=new String[classlist.size()];
        STANDARD=classlist.toArray(STANDARD);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course....");
        ad1.setSingleChoiceItems(STANDARD, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                STANDARD1=STANDARD[arg1].toString();
                standard.setText(STANDARD1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }

    public  class FacultyList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONArray jarray = new JSONArray(data);
                    facultyList = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        Faculty = jrealobj.getString("fname")+"("+jrealobj.getString("femailid")+")";

                        facultyList.add(Faculty);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectFacultyDialog();
            }
        }

    }

    private void showSelectFacultyDialog() {

        FACULTY=new String[facultyList.size()];
        FACULTY=facultyList.toArray(FACULTY);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course....");
        ad1.setSingleChoiceItems(FACULTY, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                FACULTY1=FACULTY[arg1].toString();
                faculty.setText(FACULTY1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }


    public  class LaunchBatch extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("standard", params[1]));
            nameValuePairList.add(new BasicNameValuePair("course", params[2]));
            nameValuePairList.add(new BasicNameValuePair("batchName", params[3]));
            nameValuePairList.add(new BasicNameValuePair("fees", params[4]));
            nameValuePairList.add(new BasicNameValuePair("time", params[5]));
            nameValuePairList.add(new BasicNameValuePair("facultyAlloted", params[6]));

            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                output = data;

            }

            catch(ClientProtocolException e1)
            {
                e1.printStackTrace();
            }
            catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            catch (UnknownHostException e1)
            {
                output = "6";
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pg.dismiss();
            if(output.equals("1"))
            {
                Toast.makeText(getApplicationContext(), "Batch launched successfully...", Toast.LENGTH_LONG).show();
                A_NewBatch.new_batch.finish();

            }
            if(output.equals("0"))
            {
                Toast.makeText(getApplicationContext(), "Subject launched successfully...", Toast.LENGTH_LONG).show();
                A_NewBatch.new_batch.finish();

            }

            else
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__new_batch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
