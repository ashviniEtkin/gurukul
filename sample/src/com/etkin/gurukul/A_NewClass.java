package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_NewClass extends ActionBarActivity {

    Button create;
    EditText course,standard,subject;
    ProgressDialog pb;
    String output;
    public static ActionBarActivity newClass;
    String [] course1={"Regular","TestSeries"};
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__new_class);
        create=(Button)findViewById(R.id.create);
        course=(EditText)findViewById(R.id.course);
        standard=(EditText)findViewById(R.id.standard);
        subject=(EditText)findViewById(R.id.subject);
        pb=new ProgressDialog(A_NewClass.this);
        ctx=this;
        newClass=this;

        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(ctx);
                builder.setSingleChoiceItems(course1, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which==0)
                        {
                            course.setText("Regular");
                            dialog.dismiss();
                        }
                        else {
                            course.setText("TestSeries");
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Course,Standard,Subject;
                Course=course.getText().toString().trim();
                Standard=standard.getText().toString().trim();
                Subject=subject.getText().toString().trim();

                new CreateClass().execute(getString(R.string.AdminIp)+"/CreateClass", Standard, Course, Subject);

            }
        });

    }

    public  class CreateClass extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... params)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("standard", params[1]));
            nameValuePairList.add(new BasicNameValuePair("course", params[2]));
            nameValuePairList.add(new BasicNameValuePair("subject", params[3]));
            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                output = data;

            }

            catch(ClientProtocolException e1)
            {
                e1.printStackTrace();
            }
            catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            catch (UnknownHostException e1)
            {
                output = "6";
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            DismissDialog();
            if(output.equals("1"))
            {
                Toast.makeText(getApplicationContext(), "New Class Added successfully...", Toast.LENGTH_LONG).show();
                A_NewClass.newClass.finish();

            }
            else
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }


        }
    }

    public void showProgressDialog()
    {
        pb.setTitle("Please wait");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.show();
    }
    public void DismissDialog()
    {
        pb.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__new_class, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
