package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import network.LruBitmapCache;
import network.VolleySingleton;

/**
 * Created by Administrator on 4/4/2015.
 */
public class CounselorAdapter extends RecyclerView.Adapter<CounselorAdapter.CounselorViewHolder> {
    private ArrayList<Counselor_pojo> counselorData;
    public static Context con;
    RequestQueue queue;
    ImageLoader imgLoader;
    ProgressDialog pb;
    String Delete;

    public CounselorAdapter(){

    }

    public CounselorAdapter(Context context,ArrayList<Counselor_pojo> counselorData) {
        //this.cardData = cardData;
        this.counselorData=counselorData;
        con=context;
        pb = new ProgressDialog(con);
    }

    @Override
    public CounselorAdapter.CounselorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(parent.getContext())));
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.counselor_list_row, parent, false);
        return new CounselorViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final CounselorViewHolder holder, int i) {
        final Counselor_pojo counselor_pojo=counselorData.get(i);
        holder.nImageView.setImageUrl(con.getString(R.string.image)+counselor_pojo.getPhoto(),imgLoader);

        final String id=counselor_pojo.getId();
        holder.name.setText(counselor_pojo.getCounselor_name());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        con);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this counselor ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int i) {

                                new DeleteCounselor().execute(con.getString(R.string.AdminIp) + "/DeleteCounselor", id);


                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();


            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Counselor_List.class);
                intent.putExtra("id", counselor_pojo.getId());
                intent.putExtra("photo", counselor_pojo.getPhoto());
                intent.putExtra("name", counselor_pojo.getCounselor_name());
                intent.putExtra("joining", counselor_pojo.getCounselor_date_of_joining());
                intent.putExtra("salary", counselor_pojo.getCounselor_salary());
                intent.putExtra("email", counselor_pojo.getCounselor_email());
                intent.putExtra("address", counselor_pojo.getCounselor_address());
                intent.putExtra("contact", counselor_pojo.getCounselor_contact());
                intent.putExtra("password", counselor_pojo.getCounselor_login());
                holder.context.startActivity(intent);
                }
        });

        holder.itemView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return counselorData.size();
    }

   /* public void setDataAdaptor(ArrayList<Counselor_pojo> counselorData) {
        this.counselorData = counselorData;
    }*/


    public static class CounselorViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;

        protected NetworkImageView nImageView;
        protected TextView id,name;
        private Context context;
        Button delete;

        public CounselorViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            nImageView= (NetworkImageView) itemView.findViewById(R.id.photo);
            name=(TextView)itemView.findViewById(R.id.name);
            delete=(Button)itemView.findViewById(R.id.delete);

        }
    }

    public class DeleteCounselor extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                Delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (Delete.equals("6")){
                Toast.makeText(con, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (Delete.equals("1")) {

                Toast.makeText(con, "Counselor deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(con, Counselor_List.class);
                con.startActivity(intent1);
                Counselor_List.counselor_list.finish();
            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting counselor....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }
}
