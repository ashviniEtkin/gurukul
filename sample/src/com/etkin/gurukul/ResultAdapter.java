package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/11/2015.
 */
public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder> {


    private ArrayList<Result_Pojo> resultData;
    public static Context con;
    ProgressDialog pb;

    public ResultAdapter(){

    }

    public ResultAdapter(Context context,ArrayList<Result_Pojo> resultData) {
        //this.cardData = cardData;
        this.resultData=resultData;
        con=context;
        pb = new ProgressDialog(con);
    }

    @Override
    public ResultAdapter.ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.result_list_row, parent, false);
        return new ResultViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(ResultAdapter.ResultViewHolder holder, int i) {
        final Result_Pojo result_pojo=resultData.get(i);


        holder.name.setText(result_pojo.getsName());
        holder.subject.setText(result_pojo.getSubject());
        holder.marks.setText(result_pojo.getMarks());

        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return resultData.size();
    }

    public static class ResultViewHolder extends RecyclerView.ViewHolder {

        protected TextView name,subject,marks;
        private Context context;

        public ResultViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            name=(TextView)itemView.findViewById(R.id.name);
            subject=(TextView)itemView.findViewById(R.id.subject);
            marks=(TextView)itemView.findViewById(R.id.marks);

        }
    }
}
