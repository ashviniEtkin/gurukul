package com.etkin.gurukul.faculty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyRequestApprovalActivity extends Activity {



    String tit,nm,desc,data,mob,email;
    Button btn;
    EditText etmsg;
    ProgressDialog pg;
    //String s1="Mina",s2="Leave Application";
    SmsManager manager;
    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_request_approval);

        b=getIntent().getExtras();
        tit=b.getString("title");
        nm=b.getString("name");
        mob=b.getString("mobno");
        email=b.getString("email");

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("In Progress....");
        pg.setCancelable(false);

        String url = getResources().getString(R.string.reqDescURL);

        new RequestTask().execute(url,tit,nm);

        etmsg=(EditText)findViewById(R.id.reqmsg);
        btn=(Button)findViewById(R.id.sendFun);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( etmsg.length()==0) {
                    etmsg.setError("Message is required");
                    etmsg.setFocusable(true);
                    etmsg.setFocusableInTouchMode(true);
                }
                else {
                    String msg = etmsg.getText().toString();

                  /*  manager = SmsManager.getDefault();
                    manager.sendTextMessage(mob, null, msg, null, null);*/

                    String url1 = getResources().getString(R.string.AdminIp)+"/send_ResponseToStudent";
                    new SendMsgTask().execute(url1, email, msg);

                }


                }
           // }
        });
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "Please check your internet connection.", false);
        }




    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public class RequestTask extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("title", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("name",arg0[2]));

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("reqdesc");
                    // for (int i = 0; i < jarray.length() - 1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(0);


                    desc=jrealobj.getString("description");

                    //}
                    //adapter = new CustomAdapter(getApplicationContext(),list);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                TextView n=(TextView)findViewById(R.id.name);
                TextView t=(TextView)findViewById(R.id.reqtitle);
                TextView de=(TextView)findViewById(R.id.reqdesc);

                n.setText(nm.toString());
                t.setText(tit.toString());
                de.setText(desc.toString());

                //Toast.makeText(getBaseContext()," "+ nm, Toast.LENGTH_LONG).show();

            }
        }

    }

    public class SendMsgTask extends AsyncTask<String,Void,Boolean> {


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("email", params[1]));
                nameValuePairList.add(new BasicNameValuePair("msg", params[2]));

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            pg.dismiss();

            if (data.equals("1")) {

                Toast.makeText(getApplicationContext(), "Succesfully Done", Toast.LENGTH_LONG).show();
                etmsg.setText("");
                finish();

            } else {
                Toast.makeText(getApplicationContext(), "Oopzs..Sorry Not Done!!!", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void back(View v)
    {

        finish();
    }
    public void home(View v)
    {
       finish();
        FacultyStudentRequestListActivity.list.finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyStudentRequestListActivity.list.finish();
        FacultyMenuActivity.menu.finish();
    }
}
