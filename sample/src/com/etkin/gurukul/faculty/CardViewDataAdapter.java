package com.etkin.gurukul.faculty;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.etkin.gurukul.R;
import com.etkin.gurukul.counselor.InsertNotice;
import com.etkin.gurukul.counselor.NextInsertActivity;

import java.util.List;

public class CardViewDataAdapter extends
        RecyclerView.Adapter<CardViewDataAdapter.ViewHolder> {

    private List<AttendanceNow> stList;


    Context con;
    DbHandler dbHandler;

    public CardViewDataAdapter(List<AttendanceNow> students, Context context) {
        this.stList = students;
        dbHandler = new DbHandler(context);
        con = context;

    }

    // Create new views
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cardview_row, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        final int pos = position;
        viewHolder.tvName.setText(stList.get(position).getNAME());
        viewHolder.rollNo.setText(stList.get(position).getROLL());

        viewHolder.tvEmailId.setText(stList.get(position).getMOB_NO());
        viewHolder.chkSelected.setChecked(stList.get(position).isSelected());
        if (stList.get(position).getSTATUS().equals("1")) {
            viewHolder.chkSelected.setChecked(true);
        }


        viewHolder.chkSelected.setTag(stList.get(position));
      //  setCheckOne(viewHolder.chkSelected);

/*
        viewHolder.chkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {


                } else {

                    not_checked=0;
                    pref2 = con.getSharedPreferences("NOTCHECKED", Context.MODE_PRIVATE);
                    editor=pref2.edit();
                    editor.putInt("NOTCHECKED",not_checked);
                    editor.commit();

                }
            }
        });*/


        viewHolder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                CheckBox cb = (CheckBox) v;
                AttendanceNow contact = (AttendanceNow) cb.getTag();
                contact.setSelected(cb.isChecked());
                stList.get(pos).setSelected(cb.isChecked());

                if (cb.isChecked()) {
                 //  dbHandler.updateAttendance(contact.getSTUD_ID(), "1");
                } else {
                    //dbHandler.updateAttendance(contact.getSTUD_ID(), "0");
                }

/*	Toast.makeText(
                        v.getContext(),
						"Clicked on Checkbox: " + cb.getTexrt() + " is "
								+ cb.isChecked(), Toast.LENGTH_LONG).show();*/

            }

        });



     //   setCheckOne(viewHolder.chkSelected);




    }




    // Return the size arraylist
    @Override
    public int getItemCount() {
        return stList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public TextView tvEmailId, rollNo;
        public CheckBox chkSelected;

        public FacultyAttendancePojo singlestudent;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvName = (TextView) itemLayoutView.findViewById(R.id.tvName);
            rollNo = (TextView) itemLayoutView.findViewById(R.id.rollNo);
            tvEmailId = (TextView) itemLayoutView.findViewById(R.id.tvEmailId);
            chkSelected = (CheckBox) itemLayoutView
                    .findViewById(R.id.chkSelected);

        }

    }


    // method to access in activity after updating selection
    public List<AttendanceNow> getStudentist() {
        return stList;
    }

}
