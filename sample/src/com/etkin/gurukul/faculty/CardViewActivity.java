package com.etkin.gurukul.faculty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CardViewActivity extends ActionBarActivity {

    private Toolbar toolbar;
    public static String once_attendance = "ONE";
    ProgressDialog pb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<FacultyAttendancePojo> studentList;
    String selectItem1, selectItem2, selectItem3, selectItem;
    private Button btnSelection;
    String nm, list1, date, data;
    FacultyAttendancePojo model;
    String[] StudName;
    String[] StudMob;
    String[] StudStatus;
    String[] StudId;
    Context context;
    TextView total, start;


    ArrayList<AttendanceNow> attend;
    String STD, DATE, COURSE, SUBJECT, BATCH_TIME;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_attendance);
        savedInstanceState = getIntent().getExtras();
        selectItem = savedInstanceState.getString("course");
        selectItem1 = savedInstanceState.getString("std");
        selectItem2 = savedInstanceState.getString("sub");
        selectItem3 = savedInstanceState.getString("time");
        context = this;
        pb = new ProgressDialog(context);
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        date = String.format("%d/%d/%d", day, month + 1, year);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        total = (TextView) findViewById(R.id.total);
        btnSelection = (Button) findViewById(R.id.submit);
        start = (TextView) findViewById(R.id.t1);
        start.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QrActivity.class);
                startActivity(intent);
            }
        });


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String url1 = getResources().getString(R.string.studListURL);

        SharedPreferences sharedPreferences1 = getSharedPreferences(CardViewActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        String login = sharedPreferences1.getString("studData", "");
        SharedPreferences sharedPreferences2 = getSharedPreferences(QrActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        String qr = sharedPreferences2.getString("qrData", "");
      /*  if (qr.equals("1")) {

        } else if (login.equals("1")) {
            Intent intent = new Intent(getApplicationContext(), QrActivity.class);
            startActivity(intent);
        } else {

            DbHandler handler = new DbHandler(context);
            new ListAsynTask().execute(url1, selectItem, selectItem1, selectItem2, selectItem3);
        }*/


        DbHandler handler = new DbHandler(context);
        new ListAsynTask().execute(url1, selectItem, selectItem1, selectItem2, selectItem3);


        // create an Object for Adapter


        btnSelection.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


        /*        String data = "";
                String name="";
                List<FacultyAttendancePojo> stList = ((CardViewDataAdapter) mAdapter)
                        .getStudentist();
                StudName=new String[stList.size()];
                StudMob=new String[stList.size()];
                StudId=new String[stList.size()];
                StudStatus=new String[stList.size()];

                for (int i = 0; i < stList.size(); i++) {
                    FacultyAttendancePojo singleStudent = stList.get(i);
                    if (singleStudent.isSelected() == true) {

                        StudName[i]=singleStudent.getName().toString();
                        StudMob[i]=singleStudent.getMob().toString();
                        StudId[i]=singleStudent.getId().toString();

                        name=name+singleStudent.getName().toString();
                        data = data + singleStudent.getMob().toString();
                        StudStatus[i]="0";
						*//*
                         * Toast.makeText( CardViewActivity.this, " " +
						 * singleStudent.getName() + " " +
						 * singleStudent.getEmailId() + " " +
						 * singleStudent.isSelected(),
						 * Toast.LENGTH_SHORT).show();
						 *//*
                    }
                    else {
                        StudName[i] = singleStudent.getName().toString();
                        StudMob[i] = singleStudent.getMob().toString();
                        StudId[i] = singleStudent.getId().toString();
                        StudStatus[i] = "1";
                    }

                }*/

                /*Toast.makeText(CardViewActivity.this,
                        "Selected Students: \n" + name+" "+data, Toast.LENGTH_LONG)
                        .show();*/


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to submit ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {

                                String url = getResources().getString(R.string.saveAttendanceURL);

                                DbHandler db = new DbHandler(context);
                                attend = db.getAttendance(selectItem, selectItem1, selectItem2, selectItem3);
                                StudName = new String[attend.size()];
                                StudMob = new String[attend.size()];
                                StudId = new String[attend.size()];
                                StudStatus = new String[attend.size()];


                                for (int j = 0; j < attend.size(); j++) {

                                    AttendanceNow singleStudent = attend.get(j);

                                    StudName[j] = singleStudent.getNAME();
                                    StudMob[j] = singleStudent.getMOB_NO();
                                    StudId[j] = singleStudent.getSTUD_ID();
                                    StudStatus[j] = singleStudent.getSTATUS();
                                    STD = singleStudent.getSTD();
                                    DATE = singleStudent.getDATE();
                                    COURSE = singleStudent.getCOURSE();
                                    SUBJECT = singleStudent.getSUB();
                                    BATCH_TIME = singleStudent.getBATCH_TIME();

                                }

                                once_attendance = "TWO";
                                Log.e("once_attendance", once_attendance + "");
                                new Attendance().execute(url, STD, DATE, COURSE, SUBJECT, BATCH_TIME);

                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mRecyclerView != null) {
            //      getList();
        }
    }

    String chk = "";

    class ListAsynTask extends AsyncTask<String, Void, Boolean> {
        DbHandler db = new DbHandler(context);
        String curDate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... arg0) {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("course", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("std", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("time", arg0[4]));

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    if (data.equals("0")) {
                        chk = data;
                    } else {
                        JSONObject jobj = new JSONObject(data);

                        JSONArray jarray = jobj.getJSONArray("studentdata");
                        studentList = new ArrayList<FacultyAttendancePojo>();

                        for (int i = 0; i < jarray.length() - 1; i++) {
                            JSONObject jrealobj = jarray.getJSONObject(i);
                            model = new FacultyAttendancePojo();
                            model.setName(jrealobj.getString("name"));
                            model.setId(jrealobj.getString("id"));
                            model.setRollNo(jrealobj.getString("rollNo"));
                            model.setMob(jrealobj.getString("mob"));
                            curDate = jrealobj.getString("date");
                            db.addStudList(model, selectItem1, curDate, selectItem, selectItem2, selectItem3);
                            studentList.add(model);

                        }
                    }

                    Log.e("msg", data);
                    //adapter = new CustomAdapter(getApplicationContext(),list);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result) {

            db.deleteAttendance(curDate);
            dismissProgressDialog();
            if (result == false) {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            } else if (!chk.equals("0")) {


                SharedPreferences sharedPreferences = getSharedPreferences(CardViewActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("studData", "1");
                //editor.putString("admin",user);
                editor.commit();

            if(once_attendance.equals("ONE")) {
                getList();
            }else{
                Log.e("Submitted", "Submitedddddd");
                Toast.makeText(context, "Attendance Already Submited", Toast.LENGTH_LONG).show();
                //    finish();

            }

               // Log.e("Submitted", "Submitedddddd");
              //  Toast.makeText(context, "Attendance Already Submited", Toast.LENGTH_LONG).show();
                //    finish();

                //  }
            } else {
                onResume();
                chk = "";
                Toast.makeText(context, "A Submited", Toast.LENGTH_LONG).show();

            }
        }

    }

    private void getList() {
        DbHandler db = new DbHandler(context);
        mAdapter = new CardViewDataAdapter(db.getAttendance(selectItem, selectItem1, selectItem2, selectItem3), context);

        // set the adapter object to the Recyclerview
        mRecyclerView.setAdapter(mAdapter);

        List<AttendanceNow> stList1 = ((CardViewDataAdapter) mAdapter)
                .getStudentist();
        total.setText(stList1.size() + "");
    }

    public class Attendance extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        String date = "";

        @Override
        protected Boolean doInBackground(String... params) {


            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("std", params[1]));

                for (int i = 0; i < StudId.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("id", StudId[i]));
                }

                nameValuePairList.add(new BasicNameValuePair("date", params[2]));
                date = params[2];
                for (int i = 0; i < StudName.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("name", StudName[i]));
                }

                nameValuePairList.add(new BasicNameValuePair("course", params[3]));
                nameValuePairList.add(new BasicNameValuePair("sub", params[4]));
                nameValuePairList.add(new BasicNameValuePair("batchtime", params[5]));

                for (int i = 0; i < StudStatus.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("status", StudStatus[i]));
                }

                for (int i = 0; i < StudMob.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("mbno", StudMob[i]));
                }

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if (data.equals("1")) {
                SharedPreferences sharedPreferences1 = getSharedPreferences(CardViewActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                sharedPreferences1.edit().clear().commit();

                SharedPreferences sharedPreferences2 = getSharedPreferences(QrActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                sharedPreferences2.edit().clear().commit();

              /*  DbHandler handler=new DbHandler(context);
                handler.deleteAttendance();*/
                Toast.makeText(getApplicationContext(), "Now Attendance submitted", Toast.LENGTH_LONG).show();
                DbHandler db = new DbHandler(getApplicationContext());
                db.deleteAttendance(date);
                once_attendance = "TWO";

                finish();
            } else {
                Toast.makeText(getApplicationContext(), "oopzs", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void showProgressDialog() {

        pb.setMessage("Please wait....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            once_attendance = "TWO";
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void back(View v) {

        once_attendance = "ONE";
        finish();
    }

    public void home(View v) {
        once_attendance = "ONE";
        finish();
        FacultyAttendanceSpinner.spinner.finish();
    }

    public void logout(View v) {
        once_attendance = "ONE";
        Intent in = new Intent(getApplication(), MenuActivity.class);
        startActivity(in);
        finish();
        FacultyAttendanceSpinner.spinner.finish();
        FacultyMenuActivity.menu.finish();
        FacultyLoginActivity.login.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedPreferences1 = getSharedPreferences(CardViewActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();

        SharedPreferences sharedPreferences2 = getSharedPreferences(QrActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences2.edit().clear().commit();

       /* DbHandler handler=new DbHandler(context);
        handler.deleteAttendance();
        Toast.makeText(context,"Closed",Toast.LENGTH_SHORT).show();*/
    }
}
