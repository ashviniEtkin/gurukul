package com.etkin.gurukul.faculty;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyNoticeListActivity extends ActionBarActivity {

    RecyclerView recList,res;
    CardView cardView;
    View v;
    String title,date;
    List<FacultyNoticeListPojo> result1 = new ArrayList();
    public static ActionBarActivity notice;
    ProgressDialog pg;
    FacultyNoticeListAdapter ca;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_notice_list);
        notice=this;

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);

        recList = (RecyclerView) findViewById(R.id.cardList);
        LinearLayoutManager llm = new LinearLayoutManager(FacultyNoticeListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        // cardView.setCardBackgroundColor(getResources().getColor(R.color.headBack));
        String url = getResources().getString(R.string.noticeTitleURL);
        new NoticeList().execute(url);
      //  ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

      //  Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
     //   if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
     // } else {
            // Internet connection is not present
            // Ask user to connect to Internet
       //     showAlertDialog(this, "No Internet Connection",
         //           "Please check your internet connection.", false);
      //  }




    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public class NoticeList extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }


        @Override
        protected Boolean doInBackground(String... arg0)
        {

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("noticetitle");


                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        // date=jrealobj.getString("date").toString();
                        //title=jrealobj.getString("title").toString();
                        FacultyNoticeListPojo ci = new FacultyNoticeListPojo();
                        ci.setSrno(jrealobj.getString("srNo"));
                        ci.setDate(jrealobj.getString("date"));
                        ci.setTitle(jrealobj.getString("title"));



                        result1.add(ci);

                    }


                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "Check For Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                // Toast.makeText(getBaseContext(), title+date , Toast.LENGTH_LONG).show();
                ca = new FacultyNoticeListAdapter(FacultyNoticeListActivity.this,result1);

                recList.setAdapter(ca);

            }
        }

    }
    public void back(View v)
    {

        finish();
    }
    public void home(View v)
    {

        finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyMenuActivity.menu.finish();
        FacultyLoginActivity.login.finish();

    }
}



