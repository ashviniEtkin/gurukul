package com.etkin.gurukul.faculty;

public class FacultyTestReportPojo {

    String name;

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    String roll;

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    String marks;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email;

    public void setName(String name) {
        this.name = name;
    }


    public String getName(){
        return this.name;
    }


}