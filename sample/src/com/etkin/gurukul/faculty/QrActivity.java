package com.etkin.gurukul.faculty;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.etkin.gurukul.R;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.rajeshwar.qrlibrary.DecoderActivity;
import com.rajeshwar.qrlibrary.result.ResultHandler;
import com.rajeshwar.qrlibrary.result.ResultHandlerFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class QrActivity extends DecoderActivity {

    TextToSpeech t1;
    QrActivity mainActivity;

    private static final String TAG = QrActivity.class.getSimpleName();
    private static final Set<ResultMetadataType> DISPLAYABLE_METADATA_TYPES = EnumSet.of(ResultMetadataType.ISSUE_NUMBER, ResultMetadataType.SUGGESTED_PRICE,
            ResultMetadataType.ERROR_CORRECTION_LEVEL, ResultMetadataType.POSSIBLE_COUNTRY);

    private TextView statusView = null;
    private View resultView = null;
    private boolean inScanMode = false;
    int a;
    String id,name;
    TextView contentsTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        resultView = findViewById(com.rajeshwar.qrlibrary.R.id.result_view);
        statusView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.status_view);
        mainActivity=this;
        inScanMode = false;


        SharedPreferences sharedPreferences = getSharedPreferences(QrActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("qrData", "1");
        //editor.putString("admin",user);
        editor.commit();


        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                    t1.setSpeechRate(0.9f);
                }
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause()");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inScanMode)
                finish();
            else
                onResume();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void handleDecode(Result rawResult, Bitmap barcode) {
        drawResultPoints(barcode, rawResult);

        ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(this, rawResult);
        handleDecodeInternally(rawResult, resultHandler, barcode);
    }

    protected void showScanner() {
        inScanMode = true;
        resultView.setVisibility(View.GONE);
        statusView.setText(com.rajeshwar.qrlibrary.R.string.msg_default_status);
        statusView.setVisibility(View.VISIBLE);
        viewfinderView.setVisibility(View.VISIBLE);
    }

    protected void showResults() {
        inScanMode = false;
        statusView.setVisibility(View.GONE);
        viewfinderView.setVisibility(View.GONE);
        resultView.setVisibility(View.VISIBLE);
    }

    // Put up our own UI for how to handle the decodBarcodeFormated contents.
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    private void handleDecodeInternally(Result rawResult, ResultHandler resultHandler, Bitmap barcode) {
        onPause();
        showResults();

        ImageView barcodeImageView = (ImageView) findViewById(com.rajeshwar.qrlibrary.R.id.barcode_image_view);
        if (barcode == null) {
            barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), com.rajeshwar.qrlibrary.R.drawable.icon));
        } else {
           // barcodeImageView.setImageBitmap(barcode);
            barcodeImageView.setImageResource(R.drawable.symbol_ok);
        }

        TextView formatTextView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.format_text_view);
        formatTextView.setText(rawResult.getBarcodeFormat().toString());

        TextView typeTextView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.type_text_view);
        typeTextView.setText(resultHandler.getType().toString());

        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        String formattedTime = formatter.format(new Date(rawResult.getTimestamp()));
        TextView timeTextView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.time_text_view);
        timeTextView.setText(formattedTime);

        TextView metaTextView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.meta_text_view);
        View metaTextViewLabel = findViewById(com.rajeshwar.qrlibrary.R.id.meta_text_view_label);
        metaTextView.setVisibility(View.GONE);
        metaTextViewLabel.setVisibility(View.GONE);
        Map<ResultMetadataType, Object> metadata = rawResult.getResultMetadata();
        if (metadata != null) {
            StringBuilder metadataText = new StringBuilder(20);
            for (Map.Entry<ResultMetadataType, Object> entry : metadata.entrySet()) {
                if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
                    metadataText.append(entry.getValue()).append('\n');
                }
            }
            if (metadataText.length() > 0) {
                metadataText.setLength(metadataText.length() - 1);
                metaTextView.setText(metadataText);
                metaTextView.setVisibility(View.VISIBLE);
                metaTextViewLabel.setVisibility(View.VISIBLE);
            }
        }

        contentsTextView = (TextView) findViewById(com.rajeshwar.qrlibrary.R.id.contents_text_view);
        CharSequence displayContents = resultHandler.getDisplayContents();
        contentsTextView.setText(displayContents);
        // Crudely scale betweeen 22 and 32 -- bigger font for shorter text
        int scaledSize = Math.max(22, 32 - displayContents.length() / 4);
        contentsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);



        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "12");


        try {
            JSONArray jsonArray=new JSONArray(displayContents.toString());
            JSONObject jrealobj = jsonArray.getJSONObject(0);
            id=jrealobj.getString("id");
            name=jrealobj.getString("name");
            if (id.equals(""))
            {
                invalidData();
                mainActivity.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            invalidData();
            mainActivity.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
            return;

        }


        contentsTextView.setText(name);
        new AsyncTask(){
            DbHandler dbHandler=new DbHandler(mainActivity);
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    a=dbHandler.updateAttendance(id,"1");
                    if (a==1)
                    {

                        t1.speak("Thanks You " + name, TextToSpeech.QUEUE_FLUSH, null);
                    }else if (a==123){
                        t1.speak("Your Attendance already submitted" + name, TextToSpeech.QUEUE_FLUSH, null);
                    }else {
                        t1.speak("Sorry " + name +" You did not belong to this batch", TextToSpeech.QUEUE_FLUSH, null);
                    }
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);


                mainActivity.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_BACK));
            }
        }.execute(null, null, null);
      /* */

    }

    public void invalidData()
    {
        t1.speak("Invalid Data" + name, TextToSpeech.QUEUE_FLUSH, null);
        contentsTextView.setText("Invalid Data !");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
