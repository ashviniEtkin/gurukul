package com.etkin.gurukul.faculty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.ArrayList;
import java.util.List;

public class FacultyTestReportAdapter extends ArrayAdapter<FacultyTestReportPojo> {

    private final List<FacultyTestReportPojo> list;
    //private final Activity context;

    LayoutInflater li;
    int textViewResourceId;
    Context context;

    public FacultyTestReportAdapter(Context context, int textViewResourceId, ArrayList<FacultyTestReportPojo> list) {
        super(context,textViewResourceId,list);
        this.context = context;
        this.textViewResourceId=textViewResourceId;
        this.list = list;
        li=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        protected TextView text1;
        protected TextView testEmail;
        protected EditText et;
    }
    List<String> data=new ArrayList<String>();
    public View getView(final int position, View convertView, ViewGroup parent) {


       final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = li.inflate(textViewResourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.text1 = (TextView) convertView.findViewById(R.id.textView1);
            viewHolder.et = (EditText) convertView.findViewById(R.id.et_marks);
            viewHolder.testEmail=(TextView)convertView.findViewById(R.id.emailTest);

            convertView.setTag(viewHolder);
            data.add(position,viewHolder.et.getText().toString());


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            data.add(position,viewHolder.et.getText().toString());
        }

        viewHolder.text1.setText(list.get(position).getName());
        viewHolder.testEmail.setText(list.get(position).getEmail());
        viewHolder.et.setText(data.get(position).toString());

       /* viewHolder.et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String nm=viewHolder.et.getText().toString();
                Toast.makeText(context, "Clicked on " + position + " " + nm, Toast.LENGTH_LONG).show();
            }
        });*/
        return convertView;
    }
}


