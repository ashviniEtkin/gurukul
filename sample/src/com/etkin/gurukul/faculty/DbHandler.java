package com.etkin.gurukul.faculty;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/4/2015.
 */
public class DbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    // private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "student";
    private static final String TABLE_STUDENT = "student_detail";

    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //PRIMARY KEY

        String CREATE_COMPANY_TABLE = "CREATE TABLE " + TABLE_STUDENT + "(STUD_ID TEXT,STD TEXT,DATE TEXT,NAME TEXT,COURSE TEXT,SUB TEXT,BATCH_TIME TEXT,STATUS TEXT,MOB_NO TEXT,ROLL TEXT)";
        db.execSQL(CREATE_COMPANY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);
    }

    public void addStudList(FacultyAttendancePojo student, String std, String date, String course, String sub, String batchTime) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("STUD_ID", student.getId());
            values.put("STD", std);
            values.put("DATE", date);
            values.put("NAME", student.getName());
            values.put("COURSE", course);
            values.put("SUB", sub);
            values.put("BATCH_TIME", batchTime);
            values.put("STATUS", "0");// status =0
            values.put("MOB_NO", student.getMob());
            values.put("ROLL", student.getRollNo());
            db.insert(TABLE_STUDENT, null, values);
            db.close(); // Closing database connection
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean check(String id) {
        sqLiteDatabase = this.getReadableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT  DISTINCT STUD_ID,STD ,DATE ,NAME ,COURSE ,SUB ,BATCH_TIME ,STATUS ,MOB_NO ,ROLL FROM " + TABLE_STUDENT + " where STUD_ID='" + id + "'", null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                return true;
            } else {
                return false;
            }
        } else
            return false;
    }

    public int updateAttendance(String id, String status) {

        try {
            sqLiteDatabase = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("STATUS", status);
            int a;
            if (status.equals("0")) {
                a = sqLiteDatabase.update(TABLE_STUDENT, values, "STUD_ID='" + id + "'", null);
            } else {

                a = sqLiteDatabase.update(TABLE_STUDENT, values, "STUD_ID='" + id + "' and not STATUS='1'", null);
            //    sqLiteDatabase.execSQL("DELETE FROM " + TABLE_STUDENT + " where STUD_ID='" + id + "' and  STATUS='0'");
            }
            if (a > 0) {
                return 1;
            } else {
                if (check(id))
                    return 123;
                else
                    return 111;
            }
            //sqLiteDatabase.execSQL("UPDATE " + TABLE_STUDENT+" SET STATUS='1' where STUD_ID='"+id+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 3;
    }


    public ArrayList getAttendance(String course, String std, String sub, String batch) {
        ArrayList<AttendanceNow> attendanceNows = new ArrayList<>();

        sqLiteDatabase = this.getReadableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT DISTINCT STUD_ID,STD ,DATE ,NAME ,COURSE ,SUB ,BATCH_TIME ,STATUS ,MOB_NO ,ROLL  FROM " + TABLE_STUDENT + " where COURSE='" + course + "' and STD='" + std + "' and SUB='" + sub + "' and BATCH_TIME='" + batch + "' ", null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    AttendanceNow attendanceNow = new AttendanceNow();
                    attendanceNow.setSTUD_ID(cursor.getString(0));
                    attendanceNow.setSTD(cursor.getString(1));
                    attendanceNow.setDATE(cursor.getString(2));
                    attendanceNow.setNAME(cursor.getString(3));
                    attendanceNow.setCOURSE(cursor.getString(4));
                    attendanceNow.setSUB(cursor.getString(5));
                    attendanceNow.setBATCH_TIME(cursor.getString(6));
                    attendanceNow.setSTATUS(cursor.getString(7));
                    attendanceNow.setMOB_NO(cursor.getString(8));
                    attendanceNow.setROLL(cursor.getString(9));
                    attendanceNows.add(attendanceNow);
                }
                while (cursor.moveToNext());
            }
        }

        return attendanceNows;
    }


    public String deleteAttendance(String date) {
        sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM " + TABLE_STUDENT + " where not date='" + date + "' ");
        // sqLiteDatabase.execSQL("DELETE FROM " + TABLE_STUDENT+" where date='"+date+"' ");
        sqLiteDatabase.execSQL("vacuum");
        return "1";
    }

}
