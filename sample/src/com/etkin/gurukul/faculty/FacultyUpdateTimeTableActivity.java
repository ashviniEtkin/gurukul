package com.etkin.gurukul.faculty;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class FacultyUpdateTimeTableActivity extends Activity {

    String dt,ti,co,st,su,ty,data;
    EditText et1,et2;
    Button btn;
    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_update_time_table);

        b=getIntent().getExtras();
        dt=b.getString("date");
        ti=b.getString("time");
        co=b.getString("course");
        st=b.getString("std");
        su=b.getString("sub");
        ty=b.getString("type");

        //Toast.makeText(getBaseContext(),dt+ti+co+st+su+ty, Toast.LENGTH_LONG).show();

        et1=(EditText)findViewById(R.id.et_date);
        et2=(EditText)findViewById(R.id.et_time);

        et1.setText(dt.toString());
        et2.setText(ti.toString());

        btn=(Button)findViewById(R.id.updateFunction);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et1 = (EditText) findViewById(R.id.et_date);
                et2 = (EditText) findViewById(R.id.et_time);
                String newdt = et1.getText().toString();
                String t = et2.getText().toString();
                if( et2.length()==0) {
                    et2.setError("Time is required");
                    et2.setFocusable(true);
                    et2.setFocusableInTouchMode(true);
                }
                else {
                    String url = getResources().getString(R.string.getTimeTable);

                    new UpdateTable().execute(url, st, su, ty, t, newdt, co, dt);

                }
            }

        });

    }


    public void showDateDialog(View v)
    {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                selectedmonth = selectedmonth + 1;
                et1.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();
    }

    public class UpdateTable extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("testtype", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("testtime", arg0[4]));
            nameValuePairList.add(new BasicNameValuePair("testdate", arg0[5]));
            nameValuePairList.add(new BasicNameValuePair("course", arg0[6]));
            nameValuePairList.add(new BasicNameValuePair("olddate", arg0[7]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                Log.e("mq", data);

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub

            super.onPostExecute(result);

            if (!data.equals("0")) {
                Toast.makeText(getBaseContext(), "Update Successfully...", Toast.LENGTH_LONG).show();
                finish();

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Check For Internet Connection", Toast.LENGTH_LONG).show();
            }


        }
    }


    public void back(View v)
    {
        finish();
    }
    public void home(View v)
    {
        finish();
        FacultyTimeTableListActivity.activity.finish();
        FacultyTableListSpinner.table.finish();

    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyTimeTableListActivity.activity.finish();
        FacultyTableListSpinner.table.finish();
        FacultyMenuActivity.menu.finish();
    }
}
