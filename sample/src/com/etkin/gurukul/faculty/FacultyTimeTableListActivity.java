package com.etkin.gurukul.faculty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyTimeTableListActivity extends ActionBarActivity {

    RecyclerView recList,res;
    View v;
    String title,date,st1,su1,co1;
    List<FacultyTimeTablePojo> result1 = new ArrayList();
    public static ActionBarActivity activity;
    ProgressDialog pg;
    FacultyTimeTableAdapter ca;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_time_table_list);
        recList = (RecyclerView) findViewById(R.id.cardList);
        activity=this;
        LinearLayoutManager llm = new LinearLayoutManager(FacultyTimeTableListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading.........");
        pg.setCancelable(false);


        SharedPreferences prefs = getSharedPreferences(FacultyTableListSpinner.class.getSimpleName(),Context.MODE_PRIVATE);

        st1=prefs.getString("STAD","");
        su1=prefs.getString("SUBJ","");
        co1=prefs.getString("CRS","");



        String url = getResources().getString(R.string.getTableList);

        new TimeTable().execute(url,co1,st1,su1);

    }


    public class TimeTable extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {

           List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("course", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("std", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[3]));

            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);
                    Log.e("msg", data);
                    JSONArray jarray = jobj.getJSONArray("tabledata");


                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        FacultyTimeTablePojo ci = new FacultyTimeTablePojo();

                        ci.setDate(jrealobj.getString("date"));
                        ci.setCourse(jrealobj.getString("course"));
                        ci.setStd(jrealobj.getString("std"));
                        ci.setSub(jrealobj.getString("sub"));
                        ci.setTime(jrealobj.getString("time"));
                        ci.setType(jrealobj.getString("type"));
                        result1.add(ci);

                    }



                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();

            if(result==false)
            {

                Toast.makeText(getBaseContext(), "Check For Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                // Toast.makeText(getBaseContext(), title+date , Toast.LENGTH_LONG).show();
                ca = new FacultyTimeTableAdapter(FacultyTimeTableListActivity.this,result1);
                recList.setAdapter(ca);

            }
        }

    }
    public void back(View v)
    {

        finish();
    }
    public void home(View v)
    {
        finish();
        FacultyTableListSpinner.table.finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyTableListSpinner.table.finish();
        FacultyMenuActivity.menu.finish();
    }


}
