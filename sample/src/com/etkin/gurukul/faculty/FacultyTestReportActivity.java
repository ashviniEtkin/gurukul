package com.etkin.gurukul.faculty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class FacultyTestReportActivity extends Activity {
    String url;
    String std,sub,co,type,data,testsubject,testchapter;
    EditText d,marks,totalMarks;
    Button btn1;
    ArrayList<FacultyTestReportPojo> list;
    FacultyTestReportAdapter mAdapter;
    FacultyTestReportPojo model;
    String nm,mk,tmk,da,email;
    ListView lv;
    View v1;
    int i;

    FacultyTestReportPojo facultyTestReportPojo;
    Button show;
    ArrayList s;
    ArrayList chap;
    String[] TESTSUBJECT1;
    String[] TESTCHAPTER1;

    ListView listView;
    private String[] arrTemp;
    private List<FacultyTestReportPojo> listDataModels;
    ArrayList date;
    String []Date1;
    ProgressDialog pg;
    MyListAdapter myListAdapter;
    EditText testSubject,testChapter;


    String[] StudName;
    String[] StudMarks;
    String[] StudEmail;

    Context context;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_test_report);
        url = getResources().getString(R.string.saveReportURL);
        b=getIntent().getExtras();
        std=b.getString("std");
        sub=b.getString("sub");
        co=b.getString("course");
        type=b.getString("type");
        context=this;
        d=(EditText)findViewById(R.id.et_date);
        marks=(EditText)findViewById(R.id.et_marks);
        testSubject= (EditText) findViewById(R.id.testSubject);
        testChapter= (EditText) findViewById(R.id.testChapter);
     //   marks.setText("0");
        totalMarks= (EditText) findViewById(R.id.et_mk);
       // lv = (ListView)findViewById(R.id.listview);
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);
        btn1=(Button)findViewById(R.id.submit);
        show= (Button) findViewById(R.id.submit);

        String url1 = getResources().getString(R.string.getStudListURL);
        new ListAsynTask().execute(url1,co,std,sub);
        //new ListAsynTask().execute("http://192.168.1.2/Institute/FacultyModul.asmx/getStudentlist",co,std,sub);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "Please check your internet connection.", false);
        }

        listDataModels = new ArrayList();
        facultyTestReportPojo = new FacultyTestReportPojo();


        myListAdapter = new MyListAdapter();
        listView = (ListView) findViewById(R.id.listViewMain);

        testSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DisplayTestSubject().execute(getString(R.string.AdminIp) + "/Get_TestSubject");
            }
        });

        testChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DisplayTestChapter().execute(getString(R.string.AdminIp) + "/Get_TestChapter",testSubject.getText().toString().trim(),std);
            }
        });


        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String data = "";
                List<FacultyTestReportPojo> stList = myListAdapter.getStudentist();
                StudName=new String[stList.size()];
                StudMarks=new String[stList.size()];
                StudEmail=new String[stList.size()];

                for (int i = 0; i < stList.size(); i++) {
                    FacultyTestReportPojo singleStudent = stList.get(i);
                    data = data + "\n" + singleStudent.getMarks();

                    StudName[i]=singleStudent.getName().toString();
                    StudMarks[i]=singleStudent.getMarks().toString();
                    StudEmail[i]=singleStudent.getEmail().toString();
                   /* if (singleStudent.isSelected() == true) {

                        data = data + "\n" + singleStudent.getMarks().toString();
						*//*
						 * Toast.makeText( CardViewActivity.this, " " +
						 * singleStudent.getName() + " " +
						 * singleStudent.getEmailId() + " " +
						 * singleStudent.isSelected(),
						 * Toast.LENGTH_SHORT).show();
						 *//*
                    }*/

                }
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to submit ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int i) {
                                new Report().execute(getString(R.string.saveReportURL));
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();



            }
        });

    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showDateDialog(View v)
    {
        String url2 = getResources().getString(R.string.getDate);
        new DisplayCourseTime().execute(url2, std, sub, co, type);
    }


    public class DisplayCourseTime extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("co", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("type", arg0[4]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);


                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("data");
                    date=new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        //sub.add(jrealobj.getString("coursename"));
                        date.add(jrealobj.getString("date"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectDateDialog();
            }
        }

    }


    private void showSelectDateDialog() {
        //COURSE1=new String[course.size()];
        Date1=(String[])date.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Date...");
        ad1.setSingleChoiceItems(Date1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                da=Date1[array];

                d.setText(da);

                dialog.dismiss();

            }
        });
        ad1.show();
        date.clear();

    }



    public class DisplayTestSubject extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);


                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jsonArray=new JSONArray(data);

                    s=new ArrayList();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jrealobj = jsonArray.getJSONObject(i);
                        s.add(jrealobj.getString("subject"));


                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectTestSubject();

            }
        }

    }

    private void showSelectTestSubject() {

        TESTSUBJECT1=(String[])s.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Subject...");
        ad1.setSingleChoiceItems(TESTSUBJECT1, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                testsubject = TESTSUBJECT1[array];
                testSubject.setText(testsubject);
                dialog.dismiss();

            }
        });
        ad1.show();


    }

    public class DisplayTestChapter extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("subject", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("standard", arg0[2]));
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jsonArray=new JSONArray(data);
                    chap=new ArrayList();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jrealobj = jsonArray.getJSONObject(i);
                        chap.add(jrealobj.getString("chapter"));


                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectTestChapter();

            }
        }

    }

    private void showSelectTestChapter() {

        TESTCHAPTER1=(String[])chap.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Chapter...");
        ad1.setSingleChoiceItems(TESTCHAPTER1, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                testchapter = TESTCHAPTER1[array];
                testChapter.setText(testchapter);
                dialog.dismiss();

            }
        });
        ad1.show();


    }




    public class ListAsynTask extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("course", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("std", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[3]));
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                Log.e("m","err1");
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("studentdata");
                    list = new ArrayList<FacultyTestReportPojo>();
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        facultyTestReportPojo = new FacultyTestReportPojo();

                        facultyTestReportPojo.setName(jrealobj.getString("name"));
                        facultyTestReportPojo.setEmail(jrealobj.getString("email"));
                        facultyTestReportPojo.setRoll(jrealobj.getString("roll"));

                        listDataModels.add(facultyTestReportPojo);


                    }
                   // mAdapter = new TestReportAdapter(getApplicationContext(),R.layout.f_testreport_row,list);

                    Log.e("msg",data);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                if (listDataModels != null) {
                    arrTemp = new String[listDataModels.size()];
                } else {
                    arrTemp = new String[0];
                }
                listView.setAdapter(myListAdapter);
            }
        }

    }


    private class MyListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            /*if(arrText != null && arrText.length != 0){
                return arrText.length;
            }*/

            if (listDataModels != null && listDataModels.size() != 0) {
                return listDataModels.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            //  return arrText[position];
            return listDataModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            //ViewHolder holder = null;
            final ViewHolder holder;
            if (convertView == null) {

                holder = new ViewHolder();
                LayoutInflater inflater = FacultyTestReportActivity.this.getLayoutInflater();
                convertView = inflater.inflate(R.layout.list_row, null);
                holder.textView1 = (TextView) convertView.findViewById(R.id.textView1);
                holder.textView2 = (TextView) convertView.findViewById(R.id.textView2);
                holder.editText1 = (EditText) convertView.findViewById(R.id.editText1);
                convertView.setTag(holder);

            } else {

                holder = (ViewHolder) convertView.getTag();
            }

            holder.ref = position;

            holder.textView2.setText(listDataModels.get(position).getRoll());
            holder.textView1.setText(listDataModels.get(position).getName());
            holder.editText1.setText(arrTemp[position]);
            holder.editText1.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    arrTemp[holder.ref] = arg0.toString();
                    listDataModels.get(holder.ref).setMarks(arg0.toString());
                }
            });

            return convertView;
        }

        private class ViewHolder {
            TextView textView1,textView2;
            EditText editText1;
            int ref;
        }
        public List<FacultyTestReportPojo> getStudentist() {
            return listDataModels;
        }


    }






    public class Report extends AsyncTask<String,String,String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("std", std));

                for (int i = 0; i < StudName.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("name", StudName[i]));
                }

                nameValuePairList.add(new BasicNameValuePair("sub", sub));

                nameValuePairList.add(new BasicNameValuePair("type", testSubject.getText().toString().trim()));

                for (int i = 0; i < StudMarks.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("marks", StudMarks[i]));
                }

                nameValuePairList.add(new BasicNameValuePair("date", d.getText().toString()));

                nameValuePairList.add(new BasicNameValuePair("course", co));

                nameValuePairList.add(new BasicNameValuePair("total_mk", totalMarks.getText().toString()));

                for (int i = 0; i < StudEmail.length; i++) {

                    nameValuePairList.add(new BasicNameValuePair("email", StudEmail[i]));
                }

                nameValuePairList.add(new BasicNameValuePair("chapter", testChapter.getText().toString()));

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                data="0";
            } catch (UnknownHostException e) {
                e.printStackTrace();
                data="0";

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                data="0";
            } catch (Exception e) {
                e.printStackTrace();
                data="0";
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pg.dismiss();
            if (data.equals("1")) {

                Toast.makeText(getApplicationContext(), "Successfully Done", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Oops.. Sorry Not Done!!!", Toast.LENGTH_LONG).show();
            }
        }


    }
    public void back(View v)
    {

        finish();
    }
    public void home(View v)
    {

        finish();
        FacultyTestReportSpinner.test.finish();

    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        FacultyTestReportSpinner.test.finish();
        finish();
        FacultyMenuActivity.menu.finish();

    }
}