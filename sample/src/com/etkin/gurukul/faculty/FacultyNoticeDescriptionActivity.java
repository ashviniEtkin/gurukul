package com.etkin.gurukul.faculty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyNoticeDescriptionActivity extends ActionBarActivity {

    //String title,srno;
    String dt,ti,desc;
    String titl,sr;
    ProgressDialog pg;
    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_notice_description);
        b=getIntent().getExtras();
        titl=b.getString("title");
        sr=b.getString("sno");
       // title="Meeting";
        //srno="2";
        String url = getResources().getString(R.string.noticeDescURL);
        new noticeTask().execute(url,titl,sr);


        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);

       /* final RelativeLayout rl=(RelativeLayout)findViewById(R.id.no);
        final Animation anim= AnimationUtils.loadAnimation(this, R.anim.anim_notice);
        rl.startAnimation(anim);
*/
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "Please check your internet connection.", false);
        }




    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public class noticeTask extends AsyncTask<String, Void, Boolean>
    {

     /*   @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
*/
        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("title", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("srno",arg0[2]));

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("notice");
                    // for (int i = 0; i < jarray.length() - 1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(0);

                    dt=jrealobj.getString("date");
                    ti=jrealobj.getString("title");
                    desc=jrealobj.getString("desc");

                    //}
                    //adapter = new CustomAdapter(getApplicationContext(),list);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
           // pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "Check For Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                TextView d=(TextView)findViewById(R.id.date);
                TextView t=(TextView)findViewById(R.id.title);
                TextView de=(TextView)findViewById(R.id.desc);

                d.setText(dt.toString());
                t.setText(ti.toString());
                de.setText(desc.toString());

                //Toast.makeText(getBaseContext()," "+ nm, Toast.LENGTH_LONG).show();

            }
        }

    }
    public void back(View v)
    {

        finish();
    }
    public void home(View v)
    {
        Intent in=new Intent(getApplication(),FacultyMenuActivity.class);
        startActivity(in);
        finish();
        FacultyNoticeListActivity.notice.finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyNoticeListActivity.notice.finish();
        FacultyMenuActivity.menu.finish();
    }
}
