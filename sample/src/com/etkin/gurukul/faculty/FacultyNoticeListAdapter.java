package com.etkin.gurukul.faculty;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.List;

/**
 * Created by Admins on 27/03/2015.
 */
class FacultyNoticeListAdapter extends RecyclerView.Adapter<com.etkin.gurukul.faculty.FacultyNoticeListAdapter.ContactViewHolder> {
    private List<FacultyNoticeListPojo> contactList;
    Context context;
    View itemView;
    public FacultyNoticeListAdapter(Context context, List<FacultyNoticeListPojo> contactList) {
        this.contactList = contactList;
        this.context=context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.f_noticelist_row, viewGroup, false);




        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {

        // PersonalInfo ci = contactList.get(i);
        //contactViewHolder.vNewsHeading.setText(ci.name);
        //contactViewHolder.vNewsContent.setText(ci.surname);
        contactViewHolder.ttl.setText(contactList.get(i).getTitle());
        contactViewHolder.dt.setText(contactList.get(i).getDate());
        contactViewHolder.sno.setText(contactList.get(i).getSrno());
        contactViewHolder.layout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                // on click action here
                //-- use context to start the new Activity
                FacultyNoticeListPojo p=contactList.get(i);

                // Toast.makeText(context, "Clicked on "+i, Toast.LENGTH_LONG).show();
                String t=contactViewHolder.ttl.getText().toString();
                String s=contactViewHolder.sno.getText().toString();
                Intent in = new Intent(context,FacultyNoticeDescriptionActivity.class);
                in.putExtra("title",t);
                in.putExtra("sno",s);

                context.startActivity(in);
                FacultyNoticeListActivity.notice.finish();
            }
        });

        //contactViewHolder.vTitle.setText(ci.name + " " + ci.surname);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;
        protected TextView ttl;
        protected TextView dt;
        protected TextView sno;
        protected RelativeLayout layout;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ttl =  (TextView)itemView.findViewById(R.id.title);
            dt = (TextView) itemView.findViewById(R.id.date);
            sno = (TextView) itemView.findViewById(R.id.srno);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);
        }
    }

}


