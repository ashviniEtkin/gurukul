package com.etkin.gurukul.faculty;

/**
 * Created by Admins on 27/03/2015.
 */
public class FacultyNoticeListPojo {

    public String title;
    public String date;
    public String srno;

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }





}

