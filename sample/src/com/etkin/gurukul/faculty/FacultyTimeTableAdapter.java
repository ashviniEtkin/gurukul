package com.etkin.gurukul.faculty;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admins on 27/03/2015.
 */
class FacultyTimeTableAdapter extends RecyclerView.Adapter<com.etkin.gurukul.faculty.FacultyTimeTableAdapter.ContactViewHolder> {
    private List<FacultyTimeTablePojo> contactList;
    Context context;
    String data;

    View itemView;
    public FacultyTimeTableAdapter(Context context, List<FacultyTimeTablePojo> contactList) {
        this.contactList = contactList;
        this.context=context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.f_time_table_row, viewGroup, false);




        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {

        contactViewHolder.date.setText(contactList.get(i).getDate());
        contactViewHolder.course.setText(contactList.get(i).getCourse());
        contactViewHolder.std.setText(contactList.get(i).getStd());
        contactViewHolder.sub.setText(contactList.get(i).getSub());
        contactViewHolder.time.setText(contactList.get(i).getTime());
        contactViewHolder.type.setText(contactList.get(i).getType());
        contactViewHolder.btn_update.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String d = contactViewHolder.date.getText().toString();
                String t = contactViewHolder.time.getText().toString();
                String c = contactViewHolder.course.getText().toString();
                String st = contactViewHolder.std.getText().toString();
                String su = contactViewHolder.sub.getText().toString();
                String ty = contactViewHolder.type.getText().toString();
                Intent in = new Intent(context, FacultyUpdateTimeTableActivity.class);
                in.putExtra("date", d);
                in.putExtra("time", t);
                in.putExtra("course", c);
                in.putExtra("std", st);
                in.putExtra("sub", su);
                in.putExtra("type", ty);
                context.startActivity(in);
            }
        });
        contactViewHolder.btn_delete.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String d=contactViewHolder.date.getText().toString();
                String t=contactViewHolder.time.getText().toString();
                String c=contactViewHolder.course.getText().toString();
                String st=contactViewHolder.std.getText().toString();
                String su=contactViewHolder.sub.getText().toString();
                String ty=contactViewHolder.type.getText().toString();

                Resources res=context.getResources();
                String url = res.getString(R.string.deleteTable);
                new DeleteTable().execute(url, st, su, ty, t,d,c);
               // notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;
        public TextView date;
        public TextView course;
        public TextView std;
        public TextView sub;
        public TextView time;
        public TextView type;

        public Button btn_update;
        public Button btn_delete;

        public ContactViewHolder(View itemView) {
            super(itemView);
           date=  (TextView)itemView.findViewById(R.id.tdate);
           course = (TextView) itemView.findViewById(R.id.tcourse);
           std = (TextView) itemView.findViewById(R.id.tstd);
           sub = (TextView) itemView.findViewById(R.id.tsub);
           time= (TextView) itemView.findViewById(R.id.ttime);
           type= (TextView) itemView.findViewById(R.id.ttype);
            btn_update=(Button)itemView.findViewById(R.id.update);
            btn_delete=(Button)itemView.findViewById(R.id.delete);
        }
    }
    public class DeleteTable extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("testtype", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("testtime", arg0[4]));
            nameValuePairList.add(new BasicNameValuePair("testdate", arg0[5]));
            nameValuePairList.add(new BasicNameValuePair("course", arg0[6]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);


            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (!data.equals("0")) {
                //objects.remove(position);
                //notifyDataSetChanged();

                Toast.makeText(context, "Deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent=new Intent(context,FacultyTimeTableListActivity.class);
                context.startActivity(intent);
                FacultyTimeTableListActivity.activity.finish();

                // Intent in=new Intent(context,AdminOption.class);
                //startActivity(in);

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(context, "Deletion Failed", Toast.LENGTH_LONG).show();
            }


        }
    }

}