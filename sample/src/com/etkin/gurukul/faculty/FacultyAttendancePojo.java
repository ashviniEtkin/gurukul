package com.etkin.gurukul.faculty;

public class FacultyAttendancePojo {

    String name;
    private boolean selected;
    String mob;
    String id;

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    String rollNo;

    public void setName(String name) {
        this.name = name;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getName(){
        return this.name;
    }
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    public String getId(){
        return this.id;
    }
    public String getMob(){
        return this.mob;
    }

}