package com.etkin.gurukul.faculty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class FacultySendRequestActivity extends Activity {

    private int day, month, year;
    Button btn;
    String data,nm;
    EditText et_title,et_desc;
    TextView date;
    ProgressDialog pg;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_send_request);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("In Progress....");
        pg.setCancelable(false);

        b=getIntent().getExtras();
        nm=b.getString("name");

        date=(TextView)findViewById(R.id.date);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        date.setText(String.format("%d/%d/%d", day, month + 1,year ));

        et_title=(EditText)findViewById(R.id.req_title);
        et_desc=(EditText)findViewById(R.id.req_desc);

        btn=(Button)findViewById(R.id.send_req);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_title=(EditText)findViewById(R.id.req_title);
                et_desc=(EditText)findViewById(R.id.req_desc);
                String title=et_title.getText().toString();
                String desc=et_desc.getText().toString();
                String date1=date.getText().toString();
                //String nm="Mina";
                if( et_title.length()==0) {
                    et_title.setError("Title is required");
                    et_title.setFocusable(true);
                    et_title.setFocusableInTouchMode(true);
                }
                else if(  et_desc.length()==0) {
                    et_desc.setError("Write Some Data");
                    et_desc.setFocusable(true);
                    et_desc.setFocusableInTouchMode(true);
                }
                else if(date.length()==0) {
                    date.setError("Date is required");
                    date.setFocusable(true);
                    date.setFocusableInTouchMode(true);
                }
                else {
                    String url = getResources().getString(R.string.sendReqURL);
                    new requestAsyn().execute(url, nm, date1, title, desc);
                    // new requestAsyn().execute("http://192.168.1.2/Institute/FacultyModul.asmx/sendRequest",nm,date1,title,desc);

                }
            }
        });
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection","Please check your internet connection.", false);
        }




    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public class requestAsyn extends AsyncTask<String, Void, String>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("sendernm", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("reqdate", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("reqtitle", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("reqdesc", arg0[4]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                Log.e("msg", data);

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            pg.dismiss();

            if (data.equals("1")) {
                Toast.makeText(getBaseContext(), "Successfully Done", Toast.LENGTH_LONG).show();
                et_title.setText("");
                et_desc.setText("");
                finish();
                //Intent in=new Intent(getApplication(),AttendanceActivity.class);
                //startActivity(in);

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Oopzs..Sorry Not Done!!!", Toast.LENGTH_LONG).show();
            }


        }
    }

    public void back(View v)
    {
        finish();
    }
    public void home(View v)
    {
       finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyMenuActivity.menu.finish();
    }
}
