package com.etkin.gurukul.faculty;

/**
 * Created by Administrator on 11/5/2015.
 */
public class AttendanceNow {

    public String getSTUD_ID() {
        return STUD_ID;
    }

    public void setSTUD_ID(String STUD_ID) {
        this.STUD_ID = STUD_ID;
    }

    public String getSTD() {
        return STD;
    }

    public void setSTD(String STD) {
        this.STD = STD;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getCOURSE() {
        return COURSE;
    }

    public void setCOURSE(String COURSE) {
        this.COURSE = COURSE;
    }

    public String getSUB() {
        return SUB;
    }

    public void setSUB(String SUB) {
        this.SUB = SUB;
    }

    public String getBATCH_TIME() {
        return BATCH_TIME;
    }

    public void setBATCH_TIME(String BATCH_TIME) {
        this.BATCH_TIME = BATCH_TIME;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getMOB_NO() {
        return MOB_NO;
    }

    public void setMOB_NO(String MOB_NO) {
        this.MOB_NO = MOB_NO;
    }

    public String STUD_ID;
    public String STD;
    public String DATE;
    public String NAME;
    public String COURSE;
    public String SUB;
    public String BATCH_TIME;
    public String STATUS;
    public String MOB_NO;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {

        this.selected = selected;
    }

    private boolean selected;

    public String getROLL() {
        return ROLL;
    }

    public void setROLL(String ROLL) {
        this.ROLL = ROLL;
    }

    public String ROLL;
}
