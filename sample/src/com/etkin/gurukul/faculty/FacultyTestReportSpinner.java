package com.etkin.gurukul.faculty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyTestReportSpinner extends ActionBarActivity {
    Spinner sp1,type;
    String selectItem1,selectItem2,selectItem3,selectItem4,selectItem,email;
    ArrayList std;
    ArrayList sub;
    //ArrayList time=new ArrayList();
    ArrayList course;
    String msg="data",cors,st,su,ty;
    Button btn,btn_course,btn_std,btn_sub,btn_type;
    String []COURSE1;
    String []STD1;
    String []SUB1;
    String [] testtype1;
    ProgressDialog pg;
    public static ActionBarActivity test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_test_report_spinner);
        email=getIntent().getExtras().getString("name");
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading.........");
        pg.setCancelable(false);
        test=this;


        Resources res=getResources();
        testtype1=res.getStringArray(R.array.testtype);


        btn=(Button)findViewById(R.id.nextFun);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(getApplicationContext(),FacultyTestReportActivity.class);
                in.putExtra("std",selectItem1);
                in.putExtra("sub",selectItem2);
                in.putExtra("course",selectItem);
                in.putExtra("type",selectItem4);
                startActivity(in);
               }
        });

        btn_course=(Button)findViewById(R.id.stud_course);
        btn_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = getResources().getString(R.string.getCourseURL1);
                new DisplayCourse().execute(url);
            }
        });

        btn_std=(Button)findViewById(R.id.stud_std);
        btn_std.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url1 = getResources().getString(R.string.getStdURL);
                new DisplayAsynTask().execute(url1,selectItem);

            }
        });
        btn_sub=(Button)findViewById(R.id.stud_sub);
        btn_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url2 = getResources().getString(R.string.stdDetailsURL);
                new DisplayTask().execute(url2,selectItem1);
            }
        });
        btn_type=(Button)findViewById(R.id.test_type);
        btn_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showSelectTypeDialog();
            }
        });

        //blink();
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "Please check your internet connection.", false);
        }




    }

    private void showSelectTypeDialog() {
        //COURSE1=new String[course.size()];


        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Type...");
        ad1.setSingleChoiceItems(testtype1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                ty=testtype1[array];

                btn_type.setText(ty);
                selectItem4=btn_type.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();

    }
    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public class DisplayCourse extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("Email", email));
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));

                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray1 = jobj.getJSONArray("course");
                    course=new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray1.length() - 1; i++) {
                        JSONObject jrealobj = jarray1.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        course.add(jrealobj.getString("course"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectCoursesDialog();

            }
        }

    }
    public class DisplayAsynTask extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("coursename", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray1 = jobj.getJSONArray("standard");
                    std=new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray1.length() - 1; i++) {
                        JSONObject jrealobj = jarray1.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        std.add(jrealobj.getString("standard"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                      showSelectStdDialog();
            }
           // blink();
        }

    }


    public class DisplayTask extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("stddata");
                    sub=new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        sub.add(jrealobj.getString("subject"));
                        // time.add(jrealobj.getString("coursetiming"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectSubDialog();
            }
        }

    }


    private void showSelectCoursesDialog() {
        //COURSE1=new String[course.size()];

        COURSE1=(String[])course.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course...");
        ad1.setSingleChoiceItems(COURSE1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                cors=COURSE1[array];

                btn_course.setText(cors);
                selectItem=btn_course.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        course.clear();
    }


    private void showSelectStdDialog() {
        //COURSE1=new String[course.size()];
        STD1=(String[])std.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard...");
        ad1.setSingleChoiceItems(STD1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                st=STD1[array];

                btn_std.setText(st);
                selectItem1=btn_std.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        std.clear();
        //sub.clear();
        //time.clear();
    }

    private void showSelectSubDialog() {
        //COURSE1=new String[course.size()];
        SUB1=(String[])sub.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Subject...");
        ad1.setSingleChoiceItems(SUB1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                su=SUB1[array];

                btn_sub.setText(su);
                selectItem2=btn_sub.getText().toString();
                dialog.dismiss();

            }
        });
        ad1.show();
        //std.clear();
        sub.clear();
        //time.clear();
    }

    public void back(View v)
    {
        finish();
    }
    public void home(View v)
    {

        finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyMenuActivity.menu.finish();
    }

}
