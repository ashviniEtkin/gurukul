package com.etkin.gurukul.faculty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class FacultyTestTimeTableActivity extends Activity {

    String selectItem1,selectItem2,selectItem3,selectItem,email;
    ArrayList std;
    ArrayList sub;
    ArrayList course;
    EditText time,date,syllabus,msg;
    String ttime,tdate,tsy,tmsg;
    Spinner sp1;
    String data,cors,su,st,ty,testsubject;
    ArrayList s;
    Button btn;
    EditText btn_course,btn_std,btn_sub,btn_type;
    private int day, month, year;
    private final int DATE_DIALOG = 1;
    ProgressDialog pg;
    String []COURSE1;
    String []TESTSUBJECT1;
    String []STD1;
    String []SUB1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_test_time_table);
        email=getIntent().getExtras().getString("name");
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is Loading.......");
        pg.setCancelable(false);


        time=(EditText)findViewById(R.id.et_time);
        date=(EditText)findViewById(R.id.et_date);
        syllabus=(EditText)findViewById(R.id.et_syllabus);
        msg=(EditText)findViewById(R.id.et_msg);

        btn_course=(EditText)findViewById(R.id.course);
        btn_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = getResources().getString(R.string.getCourseURL1);

                new DisplayCourse().execute(url);
            }
        });

        btn_std=(EditText)findViewById(R.id.standard);
        btn_std.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url1 = getResources().getString(R.string.getStdURL);

                new DisplayAsynTask().execute(url1,selectItem);

            }
        });

        btn_sub=(EditText)findViewById(R.id.subject);
        btn_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url2 = getResources().getString(R.string.stdDetailsURL);
                new DisplayTask().execute(url2,selectItem1);
            }
        });

        btn_type=(EditText)findViewById(R.id.testtype);
        btn_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DisplayTestSubject().execute(getString(R.string.AdminIp) + "/Get_TestSubject");
            }
        });

        btn=(Button)findViewById(R.id.createFunction);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                time=(EditText)findViewById(R.id.et_time);
                date=(EditText)findViewById(R.id.et_date);
                syllabus=(EditText)findViewById(R.id.et_syllabus);
                msg=(EditText)findViewById(R.id.et_msg);
                ttime=time.getText().toString();
                tdate=date.getText().toString();
                tsy=syllabus.getText().toString();
                tmsg=msg.getText().toString();
                if( time.length()==0) {
                    time.setError("Time is required");
                    time.setFocusable(true);
                    time.setFocusableInTouchMode(true);
                }
                else if(date.length()==0) {
                    date.setError("Date is required");
                    date.setFocusable(true);
                    date.setFocusableInTouchMode(true);
                }
                else if( syllabus.length()==0) {
                    syllabus.setError("Syllabus is required");
                    syllabus.setFocusable(true);
                    syllabus.setFocusableInTouchMode(true);
                }
                else if( msg.length()==0) {
                    msg.setError("Message is required");
                    msg.setFocusable(true);
                    msg.setFocusableInTouchMode(true);
                }
                else {
                    String url3 = getResources().getString(R.string.timeTableURL);

                    new createAsyn().execute(url3, selectItem1, selectItem2, selectItem3, ttime, tdate, selectItem, tsy, tmsg);
                }

            }
        });


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

        Boolean isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            //	Toast.makeText(getApplicationContext(), "Please wait.....\nData is loading", Toast.LENGTH_LONG).show();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "Please check your internet connection.", false);
        }




    }


    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

/*
    public void showDateDialog(View v)
    {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub
                    *//*      Your code   to get date and time    *//*
                selectedmonth = selectedmonth + 1;
                date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();
    }*/



    public class DisplayCourse extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("Email", email));
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));

                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray1 = jobj.getJSONArray("course");
                    course =new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray1.length() - 1; i++) {
                        JSONObject jrealobj = jarray1.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        course.add(jrealobj.getString("course"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectCoursesDialog();

            }
        }

    }


    public class DisplayTestSubject extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);


                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jsonArray=new JSONArray(data);

                    s =new ArrayList();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jrealobj = jsonArray.getJSONObject(i);
                      s.add(jrealobj.getString("subject"));


                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectTestSubject();

            }
        }

    }

    private void showSelectTestSubject() {

            TESTSUBJECT1=(String[])s.toArray(new String[0]);
            final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
            ad1.setTitle("Select Test Subject...");
            ad1.setSingleChoiceItems(TESTSUBJECT1, -1, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int array) {
                    testsubject = TESTSUBJECT1[array];

                    btn_type.setText(testsubject);
                    selectItem3 = btn_type.getText().toString();

                    dialog.dismiss();

                }
            });
            ad1.show();


    }

    public class DisplayAsynTask extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("coursename", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray1 = jobj.getJSONArray("standard");
                    std =new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray1.length() - 1; i++) {
                        JSONObject jrealobj = jarray1.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        std.add(jrealobj.getString("standard"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                    showSelectStdDialog();

            }
        }

    }

    public class DisplayTask extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("stddata");
                    sub =new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        sub.add(jrealobj.getString("subject"));


                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectSubDialog();
            }
        }

    }
    public class createAsyn extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("testtype", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("testtime", arg0[4]));
            nameValuePairList.add(new BasicNameValuePair("testdate", arg0[5]));
            nameValuePairList.add(new BasicNameValuePair("course", arg0[6]));
            nameValuePairList.add(new BasicNameValuePair("syllabus", arg0[7]));
            nameValuePairList.add(new BasicNameValuePair("msg", arg0[8]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                Log.e("msg",data);

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (data.equals("1")) {
                Toast.makeText(getBaseContext(), "Successfully Done", Toast.LENGTH_LONG).show();
                time.setText("");
                date.setText("");
                syllabus.setText("");
                msg.setText("");
                finish();
                //Intent in=new Intent(getApplication(),AttendanceActivity.class);
                //startActivity(in);

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Oopzs..Sorry Not Done!!!", Toast.LENGTH_LONG).show();
            }


        }
    }

    private void showSelectCoursesDialog() {
        //COURSE1=new String[course.size()];

        COURSE1=(String[])course.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course...");
        ad1.setSingleChoiceItems(COURSE1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                cors=COURSE1[array];

                btn_course.setText(cors);
                selectItem=btn_course.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        course.clear();
    }


    private void showSelectStdDialog() {
        //COURSE1=new String[course.size()];
        STD1=(String[])std.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard...");
        ad1.setSingleChoiceItems(STD1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                st=STD1[array];

                btn_std.setText(st);
                selectItem1=btn_std.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        std.clear();
        //sub.clear();
        //time.clear();
    }

    private void showSelectSubDialog() {
        //COURSE1=new String[course.size()];
        SUB1=(String[])sub.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Subject...");
        ad1.setSingleChoiceItems(SUB1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                su=SUB1[array];

                btn_sub.setText(su);
                selectItem2=btn_sub.getText().toString();
                dialog.dismiss();

            }
        });
        ad1.show();
        //std.clear();
        sub.clear();
        //time.clear();
    }

    public void back(View v)
    {
         finish();
    }
    public void home(View v)
    {
        finish();
    }

    public void logout(View v)
    {
        Intent in=new Intent(getApplication(),MenuActivity.class);
        startActivity(in);
        finish();
        FacultyMenuActivity.menu.finish();
    }
}
