package com.etkin.gurukul.faculty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import com.etkin.gurukul.R;

/**
 * Created by SONY on 28/03/2015.
 */
public class FacultyMenuActivity extends ActionBarActivity {

    String nm;
    public  static ActionBarActivity menu;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.f_menu);
        b = getIntent().getExtras();
        nm = b.getString("name");
        menu=this;

        ImageButton atten_btn = (ImageButton) findViewById(R.id.atten);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.whythistwo);
        atten_btn.startAnimation(anim);
        atten_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getApplication(), FacultyAttendanceSpinner.class);
                in.putExtra("name", nm);
                startActivity(in);

            }
        });

        ImageButton notice_btn = (ImageButton) findViewById(R.id.notice);
        Animation anim1 = AnimationUtils.loadAnimation(this, R.anim.why);
        notice_btn.startAnimation(anim1);
        notice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(), FacultyNoticeListActivity.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });

        ImageButton time_btn = (ImageButton) findViewById(R.id.time);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.whythistwo);
        time_btn.startAnimation(anim2);
        time_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(), FacultyTestTimeTableActivity.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });

        ImageButton report_btn = (ImageButton) findViewById(R.id.report);
        Animation anim3 = AnimationUtils.loadAnimation(this, R.anim.why);
        report_btn.startAnimation(anim3);
        report_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(), FacultyTestReportSpinner.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });
        ImageButton request_btn = (ImageButton)findViewById(R.id.send_req);
        Animation anim4 = AnimationUtils.loadAnimation(this, R.anim.whythistwo);
        request_btn.startAnimation(anim4);
        request_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(), FacultySendRequestActivity.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });



        ImageButton req_btn = (ImageButton) findViewById(R.id.studRequest);
        Animation anim5 = AnimationUtils.loadAnimation(this, R.anim.why);
        req_btn.startAnimation(anim5);
        req_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(),FacultyStudentRequestListActivity.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });


        ImageButton show_btn = (ImageButton) findViewById(R.id.showtable);
        Animation anim6 = AnimationUtils.loadAnimation(this, R.anim.whythistwo);
        show_btn.startAnimation(anim6);
        show_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplication(), FacultyTableListSpinner.class);
                in.putExtra("name", nm);
                startActivity(in);
            }
        });

    }}

