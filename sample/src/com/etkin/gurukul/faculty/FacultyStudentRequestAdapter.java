package com.etkin.gurukul.faculty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admins on 27/03/2015.
 */
class FacultyStudentRequestAdapter extends RecyclerView.Adapter<com.etkin.gurukul.faculty.FacultyStudentRequestAdapter.ContactViewHolder> {
    private List<FacultyStudentRequestPojo> contactList;
    Context context;
    View itemView;
    String Delete1;
    ProgressDialog pb;

    public FacultyStudentRequestAdapter(Context context, List<FacultyStudentRequestPojo> contactList) {
        this.contactList = contactList;
        this.context=context;
        pb = new ProgressDialog(context);
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.f_studentreq_row, viewGroup, false);




        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {


        contactViewHolder.nm.setText(contactList.get(i).getName());
        contactViewHolder.em.setText(contactList.get(i).getEmail());
        contactViewHolder.co.setText(contactList.get(i).getCourse());
        contactViewHolder.st.setText(contactList.get(i).getStd());
        contactViewHolder.su.setText(contactList.get(i).getSub());
        contactViewHolder.dt.setText(contactList.get(i).getDate());
        contactViewHolder.mb.setText(contactList.get(i).getMob());
        contactViewHolder.tt.setText(contactList.get(i).getTitle());

        contactViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this request ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int i) {

                                new DeleteRequest().execute(context.getString(R.string.AdminIp) + "/DeleteFacultyRequest", contactList.get(i).getId());


                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();

            }
        });

        contactViewHolder.layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // on click action here
                //-- use context to start the new Activity

                 //Toast.makeText(context, "Clicked on "+i, Toast.LENGTH_LONG).show();
                String n = contactViewHolder.nm.getText().toString();
                String t = contactViewHolder.tt.getText().toString();
                String m=contactViewHolder.mb.getText().toString();
                Intent in = new Intent(context,FacultyRequestApprovalActivity.class);
                in.putExtra("title", t);
                in.putExtra("name", n);
                in.putExtra("mobno", m);
                in.putExtra("email", contactViewHolder.em.getText().toString().trim());
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView nm;
        protected TextView em;
        protected TextView co;
        protected TextView st;
        protected TextView su;
        protected TextView dt;
        protected TextView mb;
        protected TextView tt;
        protected Button delete;
        protected RelativeLayout layout;


        public ContactViewHolder(View itemView) {
            super(itemView);
            nm =  (TextView)itemView.findViewById(R.id.nm);
            em =  (TextView)itemView.findViewById(R.id.email);
            co = (TextView) itemView.findViewById(R.id.course);
            st = (TextView) itemView.findViewById(R.id.std);
            su = (TextView) itemView.findViewById(R.id.sub);
            dt = (TextView) itemView.findViewById(R.id.date);
            mb = (TextView) itemView.findViewById(R.id.mob);
            tt = (TextView) itemView.findViewById(R.id.title);
            layout = (RelativeLayout) itemView.findViewById(R.id.ll1);
            delete=(Button)itemView.findViewById(R.id.delete);


        }
    }


    public class DeleteRequest extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                Delete1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Delete1 = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (Delete1.equals("6")){
                Toast.makeText(context, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (Delete1.equals("1")) {

                Toast.makeText(context, "Notice deleted successfully...", Toast.LENGTH_LONG).show();

                FacultyStudentRequestListActivity.list.finish();



            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting request....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

}


