package com.etkin.gurukul.faculty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class FacultyLoginActivity extends ActionBarActivity {

    //String std[];
    ArrayList std=new ArrayList();
    ArrayList sub=new ArrayList();
    String data;
    EditText user, pass;
    String selectItem1,selectItem2;
    ProgressDialog pg;
    public static ActionBarActivity login;
    ConnectionDetector cd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_login);
        login=this;
        cd=new ConnectionDetector(getApplicationContext());

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Log In....");
        pg.setCancelable(false);




    }

    public void showAlertDialog(Context context, String title, String message, Boolean
            status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void loginfunction(View v)
    {
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            user = (EditText) findViewById(R.id.uid);
            pass = (EditText) findViewById(R.id.pass);
            String u=user.getText().toString();
            String p=pass.getText().toString();
            if( user.length()==0) {
                user.setError("UserName is required");
                user.setFocusable(true);
                user.setFocusableInTouchMode(true);
            }
            else if( pass.length()==0) {
                pass.setError("Password is required");
                pass.setFocusable(true);
                pass.setFocusableInTouchMode(true);
            }
            else {
                //Toast.makeText(getBaseContext(),""+u+p+selectItem1+selectItem2, Toast.LENGTH_LONG).show();
                String url = getResources().getString(R.string.loginURL);
                new LoginAsynTask().execute(url, u, p);
            }

        }
        else {

            showAlertDialog(this, "No Internet Connection","Please check your internet connection.", false);
        }


    }


    public class LoginAsynTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("loginid", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("pass", arg0[2]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                Log.e("mq", data);

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            pg.dismiss();
            super.onPostExecute(result);

            if (data.equals("1")) {
                //Toast.makeText(getBaseContext(), "login successfully...", Toast.LENGTH_LONG).show();
                String u=user.getText().toString();
                Intent in=new Intent(getApplication(),FacultyMenuActivity.class);
                in.putExtra("name",u);
                finish();
                startActivity(in);
                user.setText("");
                pass.setText("");

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Check UserId & Password", Toast.LENGTH_LONG).show();
            }


        }
    }


}