package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_Create_Notice extends ActionBarActivity {

    Button create;
    TextView date,noticeFor,noticeTitle,noticeDescription;
    String Date,NoticeFor,NoticeTitle,NoticeDescription,insert;
    ProgressDialog pb;
    public static ActionBarActivity createNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__create__notice);
        date=(TextView)findViewById(R.id.date);
        noticeFor=(TextView)findViewById(R.id.noticeFor);
        noticeTitle=(TextView)findViewById(R.id.noticeTitle);
        noticeDescription=(TextView)findViewById(R.id.noticeDescription);
        pb=new ProgressDialog(A_Create_Notice.this);
        create=(Button)findViewById(R.id.create);
        createNotice=this;



        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date=date.getText().toString().trim();
                NoticeFor=noticeFor.getText().toString().trim();
                NoticeTitle=noticeTitle.getText().toString().trim();
                NoticeDescription=noticeDescription.toString().trim();

                new CreateNotice().execute(getString(R.string.AdminIp)+"/CreateNotice", Date, NoticeFor, NoticeTitle, NoticeDescription);

            }
        });


    }

    public class CreateNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("noticeFor", params[2]));
                nameValuePairList.add(new BasicNameValuePair("noticeTitle", params[3]));
                nameValuePairList.add(new BasicNameValuePair("description", params[4]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                insert = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                insert = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (insert.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (insert.equals("1")) {

                Toast.makeText(getApplicationContext(), "Notice created  successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(A_Create_Notice.this, A_Notice_List.class);
                startActivity(intent1);
                A_Notice_List.notice_list.finish();
                Sub_Notice_List.sub_notice_list.finish();
                A_Create_Notice.createNotice.finish();
            }
        }
    }

    public void showProgressDialog() {

        pb.setMessage("Creating notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__create__notice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
