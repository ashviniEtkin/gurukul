package com.etkin.gurukul;

/**
 * Created by Administrator on 4/4/2015.
 */
public class Counselor_pojo {

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String photo;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCounselor_name() {
        return counselor_name;
    }

    public void setCounselor_name(String counselor_name) {
        this.counselor_name = counselor_name;
    }

    public String getCounselor_login() {
        return counselor_login;
    }

    public void setCounselor_login(String counselor_login) {
        this.counselor_login = counselor_login;
    }

    public String getCounselor_date_of_joining() {
        return counselor_date_of_joining;
    }

    public void setCounselor_date_of_joining(String counselor_date_of_joining) {
        this.counselor_date_of_joining = counselor_date_of_joining;
    }

    public String getCounselor_salary() {
        return counselor_salary;
    }

    public void setCounselor_salary(String counselor_salary) {
        this.counselor_salary = counselor_salary;
    }

    public String getCounselor_email() {
        return counselor_email;
    }

    public void setCounselor_email(String counselor_email) {
        this.counselor_email = counselor_email;
    }

    public String getCounselor_contact() {
        return counselor_contact;
    }

    public void setCounselor_contact(String counselor_contact) {
        this.counselor_contact = counselor_contact;
    }

    public String getCounselor_address() {
        return counselor_address;
    }

    public void setCounselor_address(String counselor_address) {
        this.counselor_address = counselor_address;
    }

    public String id;
    public String counselor_name;
    public String counselor_login;
    public String counselor_date_of_joining;
    public String counselor_salary;
    public String counselor_email;
    public String counselor_contact;
    public String counselor_address;
}
