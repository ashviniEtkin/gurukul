package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/8/2015.
 */
public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestViewHolder> {

    private ArrayList<Request_Pojo> requestData;
    public static Context con;
    ProgressDialog pb;



    public RequestAdapter(){

    }

    public RequestAdapter(Context context,ArrayList<Request_Pojo> requestData) {
        //this.cardData = cardData;
        this.requestData=requestData;
        con=context;
        pb = new ProgressDialog(con);
    }


    @Override
    public RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.request_list_row, parent, false);
        return new RequestViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final RequestViewHolder holder, int i) {

        final Request_Pojo request_pojo=requestData.get(i);

        final String id=request_pojo.getId();
        holder.date.setText(request_pojo.getDate());
        holder.sender.setText(request_pojo.getSender_name());
        holder.title.setText(request_pojo.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Request_List.class);
                intent.putExtra("by", request_pojo.getSender_name());
                intent.putExtra("title", request_pojo.getTitle());
                intent.putExtra("description", request_pojo.getDescription());
                intent.putExtra("id", request_pojo.getId());
                holder.context.startActivity(intent);
            }
        });
        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return requestData.size();
    }

    public static class RequestViewHolder extends RecyclerView.ViewHolder {

        protected TextView date,sender,title;
        private Context context;

        public RequestViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            date=(TextView)itemView.findViewById(R.id.date);
            sender=(TextView)itemView.findViewById(R.id.requestBy1);
            title=(TextView)itemView.findViewById(R.id.requestBy2);

        }
    }
}
