package com.etkin.gurukul;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;



public class A_Attendance extends Fragment {
    RippleView Attendance;
    private int Color;
    ConnectionDetector cd;

    public A_Attendance() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_a__attendance, container, false);
        cd=new ConnectionDetector(getActivity());
        Attendance=(RippleView)root.findViewById(R.id.attendance);

        Attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(), "You seem to be offline !", Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_Attendance_Page.class);
                    startActivity(intent);
                }
            }
        });

        return root;
    }
}


