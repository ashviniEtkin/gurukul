package com.etkin.gurukul;

import android.app.Activity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


public class Vision extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vision);
        try {


            final TextView tv1 = (TextView) findViewById(R.id.tv_vision1);
            final Animation anim1 = AnimationUtils.loadAnimation(this, R.anim.animtext1);
            tv1.startAnimation(anim1);

            final TextView tv2 = (TextView) findViewById(R.id.tv_vision2);
            final Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.animtext2);
            tv2.startAnimation(anim2);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        }


}
