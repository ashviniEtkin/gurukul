package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.etkin.gurukul.menu.MenuActivity;
import com.etkin.gurukul.student.Result;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Attendance_List extends ActionBarActivity {
    String studId,classes,course;
    String check="hello";

    protected RecyclerView mRecyclerView;
    protected AttendanceAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Context context;
    ArrayList<Attendance_Pojo> mDatasetHome = new ArrayList<Attendance_Pojo>();
    ProgressDialog pb;
    Toolbar toolbar;
    public static ActionBarActivity attendance;
    ConnectionDetector cd;
    SharedPreferences prefs,prefs1;
    String std,crs,sub;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance__list);
        Intent intent=getIntent();
        check=intent.getStringExtra("student");

        if (check==null)
        {
            studId = intent.getStringExtra("studId");
            classes = intent.getStringExtra("classes");
            course = intent.getStringExtra("courses");
        }
        else {
            prefs = getSharedPreferences(Result.class.getSimpleName(), Context.MODE_PRIVATE);
            prefs1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
            studId = prefs1.getString("email", "");
            course = prefs.getString("CRS", "");
            classes = prefs.getString("STD", "");

        }
        context = this;
        pb=new ProgressDialog(context);
        cd=new ConnectionDetector(getApplicationContext());
        toolbar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Absent Days");
        Boolean isInternet = cd.isConnectingToInternet();
        mRecyclerView = (RecyclerView) findViewById(R.id.attendance_view);
        mLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        new AttendanceList().execute(getString(R.string.AdminIp) + "/Get_Stud_Attendance", classes, course,studId);
    }

    public class AttendanceList extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("classes", params[1]));
                nameValuePairList.add(new BasicNameValuePair("coursename", params[2]));
                nameValuePairList.add(new BasicNameValuePair("studId", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                // JSONObject jobj = new JSONObject(data);
                // JSONArray jArray = jobj.getJSONArray("information");
                JSONArray jsonArray=new JSONArray(data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    Attendance_Pojo list_pojo = new Attendance_Pojo();
                    list_pojo.setDate(jrealobj.getString("date"));

                    mDatasetHome.add(list_pojo);
                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            mAdapter=new AttendanceAdapter(Attendance_List.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);
        }
    }



    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attendance__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
