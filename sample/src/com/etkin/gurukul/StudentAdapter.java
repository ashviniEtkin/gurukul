package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import network.LruBitmapCache;
import network.VolleySingleton;

/**
 * Created by Administrator on 4/11/2015.
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder>{

    private ArrayList<Student_Pojo> studentData;
    public static Context con;
    RequestQueue queue;
    ImageLoader imgLoader;
    ProgressDialog pb;
    String Delete;

    public StudentAdapter(){

    }

    public StudentAdapter(Context context,ArrayList<Student_Pojo> studentData) {
        //this.cardData = cardData;
        this.studentData=studentData;
        con=context;
        pb = new ProgressDialog(con);
    }


    @Override
    public StudentAdapter.StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(parent.getContext())));
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.student_list_row, parent, false);
        return new StudentViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final StudentAdapter.StudentViewHolder holder, int i) {
        final Student_Pojo student_pojo=studentData.get(i);
        holder.nImageView.setImageUrl("http://etkininfotech.com/image/"+student_pojo.getPhoto(),imgLoader);

        final String id=student_pojo.getStudId();
        holder.name.setText(student_pojo.getStudName());
        holder.rollNo.setText(student_pojo.getRollNo());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Student_List.class);
                intent.putExtra("id", student_pojo.getStudId());
                intent.putExtra("photo", student_pojo.getPhoto());
                intent.putExtra("name", student_pojo.getStudName());
                intent.putExtra("email", student_pojo.getEmail());
                intent.putExtra("classes", student_pojo.getSclass());
                intent.putExtra("feesPaid", student_pojo.getFeesPaid());
                intent.putExtra("contact", student_pojo.getContact());
                intent.putExtra("address", student_pojo.getAddress());
                intent.putExtra("totalFees", student_pojo.getTotalFees());
                holder.context.startActivity(intent);
            }
        });
        holder.itemView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return studentData.size();
    }

    public static class StudentViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;

        protected NetworkImageView nImageView;
        protected TextView name,rollNo;

        private Context context;

        public StudentViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            nImageView= (NetworkImageView) itemView.findViewById(R.id.photo);
            name=(TextView)itemView.findViewById(R.id.name);
            rollNo=(TextView)itemView.findViewById(R.id.rollNo);

        }
    }
}
