package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_Result_Page extends ActionBarActivity {

    Button examType,classes,Subject,Show,Course;
    ArrayList<String> crslist ;
    ArrayList<String> examList ;
    ArrayList<String> subjectList ;
    ProgressDialog pg;
    String clsnm,examtype,subject;
    String [] CLASS;
    String [] EXAMTYPE;
    String [] SUBJECT;
    String CLASS1,EXAMTYPE1,SUBJECT1;
    String crsnm;
    String [] STANDARD;
    public String GENDER,CATEGORY,STANDARD1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__result__page);
        examType=(Button)findViewById(R.id.examType);
        classes=(Button)findViewById(R.id.Class);
        Course= (Button) findViewById(R.id.Course);
        Subject=(Button)findViewById(R.id.Subject);
        Show=(Button)findViewById(R.id.show1);
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);

        Course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CoursesList().execute(getString(R.string.AdminIp) + "/GetCourseName");
            }
        });

        classes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ClassList().execute(getString(R.string.AdminIp) + "/GetClass");
            }
        });

        examType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String class1 = classes.getText().toString().trim();
                String course1 = Course.getText().toString().trim();
                new ExamType().execute(getString(R.string.AdminIp) + "/GetResultinfo", class1, course1);
            }
        });

        Subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DisplayTestSubject().execute(getString(R.string.AdminIp) + "/Get_TestSubject");
            }
        });

        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Result_Student_List.class);
                intent.putExtra("course", Course.getText().toString().trim());
                intent.putExtra("classes", classes.getText().toString().trim());
                intent.putExtra("examType", examType.getText().toString().trim());
                intent.putExtra("testSubject", Subject.getText().toString().trim());
            /*    intent.putExtra("subject", Subject.getText().toString().trim());*/
                startActivity(intent);
            }
        });
    }

    public  class CoursesList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONArray jarray = new JSONArray(data);
                    crslist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        crsnm = jrealobj.getString("course");

                        crslist.add(crsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectCoursesDialog();
            }
        }

    }

    private void showSelectCoursesDialog() {

        STANDARD=new String[crslist.size()];
        STANDARD=crslist.toArray(STANDARD);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard....");
        ad1.setSingleChoiceItems(STANDARD, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                STANDARD1 = STANDARD[arg1].toString();
                Course.setText(STANDARD1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }




    public  class ClassList extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONArray jarray = new JSONArray(data);

                    crslist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        clsnm = jrealobj.getString("standard");

                        crslist.add(clsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectClassDialog();
            }
        }

    }
    private void showSelectClassDialog() {

        CLASS=new String[crslist.size()];
        CLASS=crslist.toArray(CLASS);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard....");
        ad1.setSingleChoiceItems(CLASS, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                CLASS1=CLASS[arg1].toString();
                classes.setText(CLASS1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }

    public  class ExamType extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("classes", params[1]));
                nameValuePairList.add(new BasicNameValuePair("course", params[2]));
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONArray jarray = new JSONArray(data);

                    examList = new ArrayList<String>();

                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        examtype = jrealobj.getString("examType");
                       // subject = jrealobj.getString("subject");

                        examList.add(examtype);
                       // subjectList.add(subject);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showExamDialog();
            }
        }

    }

    private void showExamDialog() {

        EXAMTYPE=new String[examList.size()];
        EXAMTYPE=examList.toArray(EXAMTYPE);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard....");
        ad1.setSingleChoiceItems(EXAMTYPE, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                EXAMTYPE1=EXAMTYPE[arg1].toString();
                examType.setText(EXAMTYPE1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }


    public class DisplayTestSubject extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);


                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jsonArray=new JSONArray(data);
                    subjectList = new ArrayList<String>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jrealobj = jsonArray.getJSONObject(i);
                        subjectList.add(jrealobj.getString("subject"));


                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSubjectDialog();

            }
        }

    }



    private void showSubjectDialog() {

        SUBJECT=new String[subjectList.size()];
        SUBJECT=subjectList.toArray(SUBJECT);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Subject....");
        ad1.setSingleChoiceItems(SUBJECT, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                SUBJECT1=SUBJECT[arg1].toString();
                Subject.setText(SUBJECT1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__result__page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
