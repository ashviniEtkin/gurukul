package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import network.LruBitmapCache;
import network.VolleySingleton;


public class Sub_Counselor_List extends ActionBarActivity {

    NetworkImageView nImageView;
    String Image,name,joining,salary,email,address,contact,id,password;
    RequestQueue queue;
    ImageLoader imgLoader;
    EditText Name,Joining,Salary,Email,Address,Contact,Password;
    Button Delete,Update;
    String delete,update;
    ProgressDialog pb,pb1;
    Context context;

    private static ActionBarActivity sub_counselor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__counselor__list);
        pb=new ProgressDialog(Sub_Counselor_List.this);
        pb1=new ProgressDialog(Sub_Counselor_List.this);
        sub_counselor=this;
        context=this;
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(getApplicationContext())));
        nImageView= (NetworkImageView)findViewById(R.id.photo);
        Name=(EditText)findViewById(R.id.Name);
        Joining=(EditText)findViewById(R.id.Joining);
        Salary=(EditText)findViewById(R.id.Salary);
        Email=(EditText)findViewById(R.id.Email);
        Password=(EditText)findViewById(R.id.Password);
        Address=(EditText)findViewById(R.id.Address);
        Contact=(EditText)findViewById(R.id.Contact);
        Delete=(Button)findViewById(R.id.delete);
        Update=(Button)findViewById(R.id.update);

        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        Image=intent.getStringExtra("photo");
        name=intent.getStringExtra("name");
        joining=intent.getStringExtra("joining");
        salary=intent.getStringExtra("salary");
        email=intent.getStringExtra("email");
        address=intent.getStringExtra("address");
        contact=intent.getStringExtra("contact");
        password=intent.getStringExtra("password");
        nImageView.setImageUrl(getString(R.string.image)+Image,imgLoader);
        Name.setText(name);
        Joining.setText(joining);
        Salary.setText(salary);
        Email.setText(email);
        Password.setText(password);
        Address.setText(address);
        Contact.setText(contact);

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this counselor ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {
                                new DeleteCounselor().execute(getString(R.string.AdminIp) + "/DeleteCounselor", id);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name1,joining1,email1,salary1,contact1,address1;
                name1=Name.getText().toString().trim();
                joining1=Joining.getText().toString().trim();
                email1=Email.getText().toString().trim();
                salary1=Salary.getText().toString().trim();
                contact1=Contact.getText().toString().trim();
                address1=Address.getText().toString().trim();

                new UpdateCounselor().execute(getString(R.string.AdminIp)+"/UpdateCounselor",name1,joining1,email1,salary1,contact1,address1,id);

            }
        });

    }

    public class DeleteCounselor extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (delete.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (delete.equals("1")) {

                Toast.makeText(getApplicationContext(), "Counselor deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Counselor_List.this, Counselor_List.class);
                startActivity(intent1);
                Counselor_List.counselor_list.finish();
                Sub_Counselor_List.sub_counselor.finish();
            }
        }
    }

    public class UpdateCounselor extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog1();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("name", params[1]));
                nameValuePairList.add(new BasicNameValuePair("date", params[2]));
                nameValuePairList.add(new BasicNameValuePair("email", params[3]));
                nameValuePairList.add(new BasicNameValuePair("salary", params[4]));
                nameValuePairList.add(new BasicNameValuePair("contact", params[5]));
                nameValuePairList.add(new BasicNameValuePair("address", params[6]));
                nameValuePairList.add(new BasicNameValuePair("id", params[7]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                update = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog1();

            if (update.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (update.equals("1")) {

                Toast.makeText(getApplicationContext(), "Counselor updated successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Counselor_List.this, Counselor_List.class);
                startActivity(intent1);
                Counselor_List.counselor_list.finish();
                Sub_Counselor_List.sub_counselor.finish();
            }
        }
    }

    public void showProgressDialog() {

        pb.setMessage("Deleting counselor....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

    public void showProgressDialog1() {

        pb1.setMessage("Updating counselor....");
        pb1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb1.setIndeterminate(true);
        pb1.show();
    }


    public void dismissProgressDialog1() {

        pb1.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__counselor__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
