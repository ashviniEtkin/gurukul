package com.etkin.gurukul;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.etkin.gurukul.counselor.C_LoginActivity;
import com.etkin.gurukul.counselor.C_MainActivity;
import com.etkin.gurukul.menu.MenuActivity;
import com.etkin.gurukul.student.p_mainActivity;


public class WelcomeActivity extends ActionBarActivity {

    private ImageView img,img2,home,img3;
    Animation myRotation,myRotation2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        img=(ImageView)findViewById(R.id.ImageView01);
      /*  img2=(ImageView)findViewById(R.id.ImageView02);
        img3=(ImageView)findViewById(R.id.ImageView03);
        home=(ImageView)findViewById(R.id.ImageView04);*/
        int i=0;
    /*    myRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator);
        myRotation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator1);
        Animation anim2= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.top_anim);
*/
   /*     img3.startAnimation(anim2);
        home.startAnimation(anim2);*/

        ScaleAnimation scale
                = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        AnimationSet set = new AnimationSet(true);

        set.addAnimation(scale);
        set.addAnimation(alpha);
        set.setDuration(5000);
        img.startAnimation(set);
        i= call();
        if (i==1)
        {
            call();
        }


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences2 = getSharedPreferences(C_LoginActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences3 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                String login=sharedPreferences1.getString("login","");
                String login1=sharedPreferences2.getString("login1","");

                String login2=sharedPreferences2.getString("email","");
                if (login.equals("true")) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }else  if (login1.equals("true")) {
                    Intent intent = new Intent(getApplicationContext(), C_MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                else  if (!login2.equals("")) {
                    Intent intent = new Intent(getApplicationContext(), p_mainActivity.class);
                    startActivity(intent);
                    finish();
                }

                else {
                    Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
    public int call()
    {
        //img.startAnimation(myRotation);
      //  img2.startAnimation(myRotation2);
        return 1;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
