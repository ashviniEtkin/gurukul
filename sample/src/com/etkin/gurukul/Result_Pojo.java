package com.etkin.gurukul;

/**
 * Created by Administrator on 4/11/2015.
 */
public class Result_Pojo {

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String sName;
    public String subject;
    public String marks;

}
