package com.etkin.gurukul;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class Sub_Report_List extends ActionBarActivity {

    TextView type,date,description;
    String Type,Date,Description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__report__list);
        type=(TextView)findViewById(R.id.type);
        date=(TextView)findViewById(R.id.date);
        description=(TextView)findViewById(R.id.reportDescription);

        Intent intent=getIntent();
        Type=intent.getStringExtra("title");
        Date=intent.getStringExtra("date");
        Description=intent.getStringExtra("description");

        type.setText(Type);
        date.setText(Date);
        description.setText(Description);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__report__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
