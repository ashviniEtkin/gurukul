package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/9/2015.
 */
public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeViewHolder> {

    private ArrayList<Notice_Pojo> noticeData;
    public static Context con;
    ProgressDialog pb;

    public NoticeAdapter(){

    }

    public NoticeAdapter(Context context,ArrayList<Notice_Pojo> noticeData) {
        //this.cardData = cardData;
        this.noticeData=noticeData;
        con=context;
        pb = new ProgressDialog(con);
    }

    @Override
    public NoticeAdapter.NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.notice_list_row, parent, false);
        return new NoticeViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final NoticeAdapter.NoticeViewHolder holder, int i) {
        final Notice_Pojo notice_pojo=noticeData.get(i);

        final String id=notice_pojo.getSrNo();
        holder.date.setText(notice_pojo.getDate());
        holder.title.setText(notice_pojo.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Notice_List.class);
                intent.putExtra("srNo", notice_pojo.getSrNo());
                intent.putExtra("date", notice_pojo.getDate());
                intent.putExtra("noticeFor", notice_pojo.getNoticeFor());
                intent.putExtra("title", notice_pojo.getTitle());
                intent.putExtra("description", notice_pojo.getDescription());
                holder.context.startActivity(intent);
            }
        });
        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return noticeData.size();
    }

    public static class NoticeViewHolder extends RecyclerView.ViewHolder {

        protected TextView date,title;
        private Context context;

        public NoticeViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            date=(TextView)itemView.findViewById(R.id.date);
            title=(TextView)itemView.findViewById(R.id.noticeTitle);

        }
    }
}
