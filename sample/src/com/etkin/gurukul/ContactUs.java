package com.etkin.gurukul;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class ContactUs extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        try {



            final TextView tv3 = (TextView) findViewById(R.id.tv_contactnm3);
            final Animation anim3 = AnimationUtils.loadAnimation(this, R.anim.animtext3);
            tv3.startAnimation(anim3);

            final ImageView iv4 = (ImageView) findViewById(R.id.iv_contactno);
            final Animation ianim4 = AnimationUtils.loadAnimation(this, R.anim.anim_contactus);
            iv4.startAnimation(ianim4);

            final TextView tv4 = (TextView) findViewById(R.id.tv_contactnm4);
            final Animation anim4 = AnimationUtils.loadAnimation(this, R.anim.anim_contactus);
            tv4.startAnimation(anim4);

            tv4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+tv4.getText().toString().trim()));
                    startActivity(callIntent);
                }
            });

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        }


}
