package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import network.LruBitmapCache;
import network.VolleySingleton;


public class Sub_Student_List extends ActionBarActivity {

    EditText Name,Classes,Fees,CoursePackage,Salary,Email,Contact,Address;
    String name,classes,feesPaid,subject,totalFees,email,contact,address,id,Image,date;
    NetworkImageView nImageView;
    RequestQueue queue;
    ImageLoader imgLoader;
    Button Delete,Update;
    String delete,update;
    ProgressDialog pb,pb1;
    private static ActionBarActivity sub_student_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__student__list);
        pb=new ProgressDialog(Sub_Student_List.this);
        pb1=new ProgressDialog(Sub_Student_List.this);

        Name=(EditText)findViewById(R.id.studName);
        Classes=(EditText)findViewById(R.id.classes);
        Fees=(EditText)findViewById(R.id.feesPaid);
        Contact=(EditText)findViewById(R.id.Contact);
        Address=(EditText)findViewById(R.id.Address);
        CoursePackage=(EditText)findViewById(R.id.coursePackgae);
        nImageView=(NetworkImageView)findViewById(R.id.photo);
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(getApplicationContext())));
        Delete=(Button)findViewById(R.id.delete);
        Update=(Button)findViewById(R.id.update);
        sub_student_list=this;

        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        Image=intent.getStringExtra("photo");
        name=intent.getStringExtra("name");
        email=intent.getStringExtra("email");
        classes=intent.getStringExtra("classes");
        feesPaid=intent.getStringExtra("feesPaid");
        contact=intent.getStringExtra("contact");
        address=intent.getStringExtra("address");
        totalFees=intent.getStringExtra("totalFees");

        nImageView.setImageUrl(getString(R.string.image)+Image,imgLoader);
        Name.setText(name);
        Classes.setText(classes);
        Fees.setText(feesPaid);
        Contact.setText(contact);
        Address.setText(address);
        CoursePackage.setText(totalFees);

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteStudent().execute(getString(R.string.AdminIp)+"/DeleteStudent",id);
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name1,classes1,fees1,contact1,address1;
                name1=Name.getText().toString().trim();
                classes1=Classes.getText().toString().trim();
                fees1=Fees.getText().toString().trim();
                contact1=Contact.getText().toString().trim();
                address1=Address.getText().toString().trim();


                new UpdateStudent().execute(getString(R.string.AdminIp)+"/UpdateStudent",name1,classes1,fees1,contact1,address1,email,id);

            }
        });
    }

    public class DeleteStudent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (delete.equals("6")) {
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            } else if (delete.equals("1")) {

                Toast.makeText(getApplicationContext(), "Student deleted successfully...", Toast.LENGTH_LONG).show();
                A_Student_List.student_list.finish();
                Sub_Student_List.sub_student_list.finish();
                Intent intent1 = new Intent(Sub_Student_List.this, A_Student_Page.class);

                startActivity(intent1);


            }
        }
    }

    public class UpdateStudent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog1();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("name", params[1]));
                nameValuePairList.add(new BasicNameValuePair("classes", params[2]));
                nameValuePairList.add(new BasicNameValuePair("feesPaid", params[3]));
                nameValuePairList.add(new BasicNameValuePair("contactno", params[4]));
                nameValuePairList.add(new BasicNameValuePair("address", params[5]));
                nameValuePairList.add(new BasicNameValuePair("email", params[6]));
                nameValuePairList.add(new BasicNameValuePair("id", params[7]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                update = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog1();

            if (update.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (update.equals("1")) {

                Toast.makeText(getApplicationContext(), "Student updated successfully...", Toast.LENGTH_LONG).show();
                A_Student_List.student_list.finish();
                Sub_Student_List.sub_student_list.finish();
                Intent intent1 = new Intent(Sub_Student_List.this, A_Student_Page.class);
                startActivity(intent1);

            }
        }
    }


    public void showProgressDialog() {

        pb.setMessage("Deleting student....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

    public void showProgressDialog1() {

        pb1.setMessage("Updating student....");
        pb1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb1.setIndeterminate(true);
        pb1.show();
    }


    public void dismissProgressDialog1() {

        pb1.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__student__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
