package com.etkin.gurukul.student;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class StudentRequest extends ActionBarActivity {

     int year, month, day;
    String date, Register1;
    final CharSequence[] items = {"Admin","Faculty"};
    ArrayList<String> facultyList;
    EditText text1, text4,text5,text6,text7,text8,text9,text2;
    TextView text3;
    Button btn1, btn2;
    ProgressDialog pg;
    String [] FACULTY;
    AlertDialog alertDialog;
    String data,Faculty,FACULTY1;
    String Name,Receiver,Title,Course,Standard,Subject,Batchtime,Description,Receiveby;
     Toolbar toolbar;
    ConnectionDetector cd;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";

    SharedPreferences prefs;
    String registrationId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_request);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);
        text1 = (EditText) findViewById(R.id.r1);
        text2 = (EditText) findViewById(R.id.r2);
        text3 = (TextView) findViewById(R.id.r3);
        text4 = (EditText) findViewById(R.id.r4);

        text9=(EditText)findViewById(R.id.r9);
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        date = String.format("%d-%d-%d", day, month + 1, year);
        btn2=(Button)findViewById(R.id.cancel);
        prefs = getSharedPreferences(MenuActivity.class.getSimpleName(),Context.MODE_PRIVATE);
        registrationId = prefs.getString("email", "");
        text2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(StudentRequest.this);
                builder.setTitle("Select Receiver...");

                builder.setSingleChoiceItems(items,1,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                       text2.setText(items[i]);
                       // Toast.makeText(getApplicationContext(), items[i], Toast.LENGTH_SHORT).show();

                        //  dialogInterface.dismiss();
                        alertDialog.dismiss();
                    }
                });
                alertDialog = builder.create();

                alertDialog.show();
            }
        });

        text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FacultyList().execute(getString(R.string.AdminIp)+"/FacultyList");
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), p_mainActivity.class);

                startActivity(in);
                overridePendingTransition( R.anim.page_in_left, R.anim.page_out_left);
            }
        });


        btn1 = (Button) findViewById(R.id.submit);
        btn1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else  if(text1.length() == 0)
                    text1.setError( "Sender Name is required!" );
                else if(text2.length()==0)
                    text2.setError( "Receiver Type is required!" );
                else if(text3.length()==0)
                    text3.setError( "Receiver Name is required!" );
                else if(text4.length()==0)
                    text4.setError( "Request Topic is required!" );
                else if(text9.length()==0)
                    text9.setError( "Description is required!" );
                else
                {

                    Name = text1.getText().toString().trim();
                    Receiveby = text2.getText().toString().trim();
                    Receiver = text3.getText().toString().trim();
                    Title = text4.getText().toString().trim();

                    Description = text9.getText().toString().trim();


                    new Request().execute(getString(R.string.AdminIp)+"/studrequest", date, Name, Receiveby, Receiver, Title, Description,registrationId);

                } }
        });

    }


    public  class FacultyList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONArray jarray = new JSONArray(data);
                    facultyList = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        Faculty = jrealobj.getString("fname");

                        facultyList.add(Faculty);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectFacultyDialog();
            }
        }

    }

    private void showSelectFacultyDialog() {

        FACULTY=new String[facultyList.size()];
        FACULTY=facultyList.toArray(FACULTY);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Faculty....");
        ad1.setSingleChoiceItems(FACULTY, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                FACULTY1=FACULTY[arg1].toString();
                text3.setText(FACULTY1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        ad1.show();
    }



    public class Request extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("senderName", params[2]));
                nameValuePairList.add(new BasicNameValuePair("recievedBy", params[3]));
                nameValuePairList.add(new BasicNameValuePair("recieverName", params[4]));
                nameValuePairList.add(new BasicNameValuePair("title", params[5]));

                nameValuePairList.add(new BasicNameValuePair("description", params[6]));
                nameValuePairList.add(new BasicNameValuePair("email", params[7]));



                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                String abc = "0";
                //  Register1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Register1 = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return true;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);



            if (data.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (data.equals("1")) {


                Toast.makeText(getApplicationContext(), "Request send...", Toast.LENGTH_LONG).show();

                Intent intent1 = new Intent(getApplicationContext(), p_mainActivity.class);
                startActivity(intent1);

            } else if (data.equals("2")) {
                Toast.makeText(getApplication(), "You have already send....", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);
        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }

    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(StudentRequest.this, StudentRequest.class);  //your class
        startActivity(i);
        finish();

    }

}


