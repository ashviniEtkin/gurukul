package com.etkin.gurukul.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by Administrator on 4/10/2015.
 */
public class viewpaperAdapter extends RecyclerView.Adapter<viewpaperAdapter.InboxViewHolder> {

    private ProgressDialog pd;
    private ArrayList<viewpaperpojo> myDatas;
    Context context;
    String jobFileName;
    public void setDataAdaptor(ArrayList<viewpaperpojo> mDatasetHome) {
        myDatas=mDatasetHome;

    }

    public viewpaperAdapter(Context context,ArrayList<viewpaperpojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.viewpaper_row, parent, false);
        this.context = context;

        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final viewpaperpojo myData=myDatas.get(position);
        holder.title.setText(myData.getTitle());
        holder.fname.setText(myData.getFilename());
        holder.date.setText(myData.getDate());
        holder.desc.setText(myData.getDesc());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                jobFileName=myData.getTitle();
                String jobFile2=myData.getFilename();
                new DownloadFile().execute("http://192.168.1.114/student/pdf/" + jobFile2+".pdf",jobFile2+".pdf");

            }


        });


    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        protected TextView date, title,desc,fname;

        private Context context;

        public InboxViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            pd=new ProgressDialog(context);

            title = (TextView) itemView.findViewById(R.id.pdf_title);
            fname = (TextView) itemView.findViewById(R.id.pdf_fname);

            date = (TextView) itemView.findViewById(R.id.pdf_date);
            desc= (TextView) itemView.findViewById(R.id.pdf_desc);



        }



    }
    public class DownloadFile extends AsyncTask<String,String,Boolean> {

        private  final int  MEGABYTE = 1024 * 1024;
        protected void onPreExecute()

        {
            super.onPreExecute();
            showProgress();
        }
        @Override
        protected Boolean doInBackground(String... params) {

            int count;
            String fileUrl = params[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = params[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "Institute_Pdf");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try {
                if(!pdfFile.exists()) {
                    pdfFile.createNewFile();
                }
                else
                {
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
//            FileDownloader.downloadFile(fileUrl, pdfFile);
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                //urlConnection.setRequestMethod("GET");
                //urlConnection.setDoOutput(true);
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                long total  = 0;
                while((count = inputStream.read(buffer))!= -1 ){
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/totalSize));
                    fileOutputStream.write(buffer, 0, count);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;


        }

        @Override
        protected void onPostExecute(Boolean s) {
            // super.onPostExecute(s);
            dismissProgress();
            if (s == false) {
                Toast.makeText(context, "Oops!File is already exist...", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "File successfully downloaded.", Toast.LENGTH_SHORT).show();
            }
        }
        protected  void onProgressUpdate(String... values)
        {
            super.onProgressUpdate(values);
            pd.setProgress(Integer.parseInt(values[0]));
        }
    }
    private void showProgress() {
        pd.setTitle("Downloading File");
        pd.setMessage("Downloading file...");
        pd.setIndeterminate(false);
        pd.setMax(100);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }
    private void dismissProgress() {
        pd.dismiss();
    }}

