package com.etkin.gurukul.student;

/**
 * Created by Administrator on 4/1/2015.
 */
public class NoticePojo {
    public String date;
    public String noticefor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String id;

    public String getNoticedesc() {
        return noticedesc;
    }

    public void setNoticedesc(String noticedesc) {
        this.noticedesc = noticedesc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNoticefor() {
        return noticefor;
    }

    public void setNoticefor(String noticefor) {
        this.noticefor = noticefor;
    }

    public String getNoticetitle() {
        return noticetitle;
    }

    public void setNoticetitle(String noticetitle) {
        this.noticetitle = noticetitle;
    }

    public String noticedesc;
    public String noticetitle;
}
