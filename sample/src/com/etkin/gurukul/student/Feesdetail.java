package com.etkin.gurukul.student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Feesdetail extends ActionBarActivity {
    ArrayList<String> result=new ArrayList<String>();
    TextView text1,text2,text3,text4,text5,text6;
    protected RecyclerView mRecyclerView;
    protected FeesdetailAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Feesdetailpojo> mDatasetHome = new ArrayList<Feesdetailpojo>();
    Context context;
    ImageView imageView;
    Toolbar toolbar;
    SharedPreferences settings;
    public static final String REG_ID="email";
    public static final String PWD="pwd";


SharedPreferences prefs;
    ConnectionDetector cd;
    String registrationId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feesdetail);

        text1=(TextView)findViewById(R.id.f1);
        text2=(TextView)findViewById(R.id.f2);
        text3=(TextView)findViewById(R.id.f3);
        text4=(TextView)findViewById(R.id.f4);
        text5=(TextView)findViewById(R.id.f5);
        text6=(TextView)findViewById(R.id.f6);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        imageView=(ImageView)findViewById(R.id.img);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        imageView.startAnimation(myFadeInAnimation);
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        context = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.notice_view);
        //initData();
        prefs = getSharedPreferences(MenuActivity.class.getSimpleName(),Context.MODE_PRIVATE);
        registrationId = prefs.getString("email", "");
        mLayoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
if(!isInternet){
    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
}
else
{
        new Results().execute(getString(R.string.AdminIp)+"/Feesdetail",registrationId);
    }}
    public class Results extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {

                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("semailid",params[1]));


                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);

              post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("Data");
                for (int i = 0; i < jarray.length()-1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    Feesdetailpojo company2 = new Feesdetailpojo();
                    company2.setSname(jrealobj.getString("sname"));
                    // company2.setTesttype(jrealobj.getString("testtype"));
                    company2.setScourse(jrealobj.getString("scourse"));
                    company2.setSub(jrealobj.getString("subject"));
                    company2.setSfeespaid(jrealobj.getString("sfeespaid"));
                    company2.setSremainfees(jrealobj.getString("sremainigfees"));
                    company2.setNextinst(jrealobj.getString("snextinstallmentdate"));


                    // branch.add(company.getsub().toString());
                    //year.add(company2.getCourse().toString());
                    mDatasetHome.add(company2);
                }
            } catch (ClientProtocolException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            }catch (NullPointerException e2)
            {

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mAdapter=new FeesdetailAdapter(Feesdetail.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);

        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(Feesdetail.this, Feesdetail.class);  //your class
        startActivity(i);

        finish();

    }



}
