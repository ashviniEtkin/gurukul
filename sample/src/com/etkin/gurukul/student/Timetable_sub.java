package com.etkin.gurukul.student;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.etkin.gurukul.R;


public class Timetable_sub extends Activity {
    TextView text1,text2,text4,text5,text6,text7,text8;
    ImageView imageView;
    String Date,Time,Course,Subject,Standard,Testtype,Syllabus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable_sub);
        text1 = (TextView) findViewById(R.id.headdata);
        text2 = (TextView) findViewById(R.id.date);
        //text3 = (TextView) findViewById(R.id.notice);
        text4 = (TextView) findViewById(R.id.subjectdata);
        text5 = (TextView) findViewById(R.id.time);
        text6 = (TextView) findViewById(R.id.marks);
        text7 = (TextView) findViewById(R.id.units);
        text8 = (TextView) findViewById(R.id.passing_marks);



        setDetail();
        text1.setText(Date);
        text2.setText(Course);
        // text3.setText(Message);
        text4.setText(Standard);
        text5.setText(Subject);
        text6.setText(Time);
        text7.setText(Syllabus);
        text8.setText(Testtype);

        imageView=(ImageView)findViewById(R.id.img);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        imageView.startAnimation(myFadeInAnimation);

    }
    private void setDetail() {

        Intent intent=getIntent();
        Date =intent.getStringExtra("testdate");
        Course = intent.getStringExtra("course");
        // Message =intent.getStringExtra("message");
        Standard =intent.getStringExtra("standard");
        Subject = intent.getStringExtra("subject");
        Time =intent.getStringExtra("testtime");
        Syllabus=intent.getStringExtra("syllabus");
        Testtype =intent.getStringExtra("testtype");




    }


}
