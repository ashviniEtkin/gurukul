package com.etkin.gurukul.student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.etkin.gurukul.Attendance_List;
import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

public class p_mainActivity extends ActionBarActivity {

    Toolbar toolbar;
    SharedPreferences settings;
    SharedPreferences prefs;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_main);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Student Home");
        ImageView iv1 = (ImageView) findViewById(R.id.a);
        Animation anim1 = AnimationUtils.loadAnimation(this, R.anim.animapartment);
        iv1.startAnimation(anim1);

        ImageView iv2 = (ImageView) findViewById(R.id.b);
        Animation anim4 = AnimationUtils.loadAnimation(this, R.anim.animlayout);
        iv2.startAnimation(anim4);

        ImageView iv3 = (ImageView) findViewById(R.id.c);
        Animation anim5 = AnimationUtils.loadAnimation(this, R.anim.animarea);
        iv3.startAnimation(anim5);

        ImageView iv4 = (ImageView) findViewById(R.id.d);
        Animation anim3 = AnimationUtils.loadAnimation(this, R.anim.animflat);
        iv4.startAnimation(anim3);

        ImageView iv5 = (ImageView) findViewById(R.id.e);
        Animation anim6 = AnimationUtils.loadAnimation(this, R.anim.animarea);
        iv5.startAnimation(anim6);


        ImageView iv6 = (ImageView) findViewById(R.id.f);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.animflat);
        iv6.startAnimation(anim2);

        ImageView iv7 = (ImageView) findViewById(R.id.g);
        Animation anim7 = AnimationUtils.loadAnimation(this, R.anim.animcurrentsite);
        iv7.startAnimation(anim7);


        ImageView iv8 = (ImageView) findViewById(R.id.h);
        Animation anim8 = AnimationUtils.loadAnimation(this, R.anim.animenquiry);
        iv8.startAnimation(anim8);


    }

    public void inboxfun(View v)
    {
        Intent in= new Intent(this,StudentInbox.class);
        startActivity(in);
    }

    public void facultyfun(View v)
    {
        Intent in= new Intent(this,Attendance_List.class);
        in.putExtra("student","yes");
        startActivity(in);
    }

    public void feesfun(View v)
    {
    /*    Intent in= new Intent(this,Feesdetail.class);
        startActivity(in);*/
    }

    public void paperfun(View v)
    {
        Intent in= new Intent(this,Viewpaper.class);
        startActivity(in);
    }

    public void ttfun(View v)
    {
        Intent in= new Intent(this,Timetable.class);
        startActivity(in);
    }

    public void resultfun(View v)
    {
        Intent in= new Intent(this,StudentFilter.class);
        startActivity(in);
    }

    public void feedbackfun(View v)
    {
        Intent in= new Intent(this,Feedback.class);
        startActivity(in);
    }

    public void requestfun(View v)
    {
        Intent in= new Intent(this,StudentRequest.class);
        startActivity(in);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_p_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.action_home)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);

        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }


    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(p_mainActivity.this, p_mainActivity.class);  //your class
        startActivity(i);

        finish();

    }

}
