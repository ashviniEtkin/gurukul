package com.etkin.gurukul.student;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.etkin.gurukul.R;


public class NoticeSub extends Activity {
    TextView text1,text2,text3;
    String Notice,Date,Head;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_sub);


        text1 = (TextView) findViewById(R.id.date);
        text2 = (TextView) findViewById(R.id.heading);
        text3 = (TextView) findViewById(R.id.notice);
        setDetail();
        text1.setText(Date);
        text2.setText(Head);
        text3.setText(Notice);


    }


    private void setDetail() {

        Intent intent=getIntent();
        Date =intent.getStringExtra("noticedate");
        Head = intent.getStringExtra("noticetitle");
        Notice =intent.getStringExtra("noticedesc");



    }

}






