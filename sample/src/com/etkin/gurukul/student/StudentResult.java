package com.etkin.gurukul.student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class StudentResult extends ActionBarActivity {
    ArrayList<String> result=new ArrayList<String>();
    TextView text1,text2,text3,text4,text5,text6;
    protected RecyclerView mRecyclerView;
    protected Studresultadapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Studresultpojo> mDatasetHome = new ArrayList<Studresultpojo>();
    Context context;
    String s1,s2,s3;
    Intent in;
    Toolbar toolbar;

    String STD,SUB,CRS;
    String std,crs,sub,email;
    ImageView imageView;
    ConnectionDetector cd;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";
    String check="",subject;

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_result);
        text1=(TextView)findViewById(R.id.testtype);
        text2=(TextView)findViewById(R.id.course);

        text4=(TextView)findViewById(R.id.date);
        text5=(TextView)findViewById(R.id.marks);
        text6=(TextView)findViewById(R.id.std);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
       /* imageView=(ImageView)findViewById(R.id.img);*/

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
       /* imageView.startAnimation(myFadeInAnimation);*/
       /* savedInstanceState=getIntent().getExtras();
        in=getIntent();
        s1=savedInstanceState.getString("course");
        s2=savedInstanceState.getString("standard");
        s3=savedInstanceState.getString("subject");*/

        context = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.notice_view);
        //initData();

        check=getIntent().getStringExtra("admin");

        if (check==null)
        {
            prefs = getSharedPreferences(Result.class.getSimpleName(), Context.MODE_PRIVATE);
            crs = prefs.getString("CRS", "");
            std = prefs.getString("STD", "");
            sub = prefs.getString("SUB", "");
            prefs = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
            email=prefs.getString("email", "");
            subject=getIntent().getStringExtra("subject");

        }
        else {
            prefs = getSharedPreferences(Result.class.getSimpleName(), Context.MODE_PRIVATE);
            crs=getIntent().getStringExtra("courses");
            std=getIntent().getStringExtra("classes");
            sub = prefs.getString("SUB", "");
            email=getIntent().getStringExtra("studId");
            subject=getIntent().getStringExtra("testSubject");
        }


        mLayoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }
        else{


        new Results().execute(getString(R.string.AdminIp)+"/saveTestReport",crs,std,sub,email,subject);

    }
    }
    public class Results extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {


                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);

                nameValuePairs.add(new BasicNameValuePair("course",params[1]));
                nameValuePairs.add(new BasicNameValuePair("standard",params[2]));
                nameValuePairs.add(new BasicNameValuePair("subject",params[3]));
                nameValuePairs.add(new BasicNameValuePair("email",params[4]));
                nameValuePairs.add(new BasicNameValuePair("testSubject",params[5]));


                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("report");
                for (int i = 0; i < jarray.length()-1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    Studresultpojo company2 = new Studresultpojo();
                    company2.setDate(jrealobj.getString("date"));
                    company2.setSubjectname(jrealobj.getString("chapter"));
                    company2.setTotalmarks(jrealobj.getString("totalmarks"));
                    company2.setMarks(jrealobj.getString("marks"));


                    // branch.add(company.getsub().toString());
                    //year.add(company2.getCourse().toString());
                     mDatasetHome.add(company2);
                }
            } catch (ClientProtocolException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            }catch (NullPointerException e2)
            {

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mAdapter=new Studresultadapter(StudentResult.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);
        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }

    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(StudentResult.this, StudentResult.class);  //your class
        startActivity(i);
        finish();

    }

}
