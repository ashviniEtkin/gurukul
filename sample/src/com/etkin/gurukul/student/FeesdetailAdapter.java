package com.etkin.gurukul.student;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 4/7/2015.
 */
public class FeesdetailAdapter  extends RecyclerView.Adapter<FeesdetailAdapter.InboxViewHolder> {
    private ArrayList<Feesdetailpojo> myDatas;
    Context context;
    public void setDataAdaptor(ArrayList<Feesdetailpojo> mDatasetHome) {
        myDatas=mDatasetHome;

    }

    public FeesdetailAdapter(Context context,ArrayList<Feesdetailpojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
    }


    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.feesdetail_row, parent, false);
        this.context = context;

        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final Feesdetailpojo myData=myDatas.get(position);
        holder.sname.setText(myData.getSname());
        holder.scourse.setText(myData.getScourse());
        holder.sub.setText(myData.getSub());
        holder.paid.setText(myData.getSfeespaid());
        holder.remain.setText(myData.getSremainfees());
        holder.nextinst.setText(myData.getNextinst());

    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView sname,scourse,sub,paid,remain,nextinst;

        private Context context;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context=context;



           sname= (TextView) itemView.findViewById(R.id.f1);
            scourse = (TextView) itemView.findViewById(R.id.f2);
            sub = (TextView) itemView.findViewById(R.id.f3);
            paid = (TextView) itemView.findViewById(R.id.f4);
            remain = (TextView) itemView.findViewById(R.id.f5);
            nextinst = (TextView) itemView.findViewById(R.id.f6);


        }

    }

}

