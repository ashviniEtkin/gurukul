package com.etkin.gurukul.student;

/**
 * Created by Administrator on 4/7/2015.
 */
public class Feesdetailpojo {
    String sname;
    String scourse;
    String sub;

    public String getSfeespaid() {
        return sfeespaid;
    }

    public void setSfeespaid(String sfeespaid) {
        this.sfeespaid = sfeespaid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getScourse() {
        return scourse;
    }

    public void setScourse(String scourse) {
        this.scourse = scourse;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getNextinst() {
        return nextinst;
    }

    public void setNextinst(String nextinst) {
        this.nextinst = nextinst;
    }

    public String getSremainfees() {
        return sremainfees;
    }

    public void setSremainfees(String sremainfees) {
        this.sremainfees = sremainfees;
    }

    String sfeespaid;
    String nextinst;
    String sremainfees;

}
