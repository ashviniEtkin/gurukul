package com.etkin.gurukul.student;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends Activity {
Button btn1,save,btnCancel;
    EditText text1,text2;
    Context context;
    String STUDENTS_USER_ID,STUDENTS_PASSWORD,Register;
    String registrationId,registrationpwd;
    int PRIVATE_MODE=0;
    SharedPreferences settings;
    public static final String REG_ID="email";
    public static final String PWD="pwd";
    SharedPreferences prefs;
    ConnectionDetector cd;
    @Override
    protected void onSaveInstanceState(Bundle outState) {
       // outState.putSerializable("d", modelItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            final Dialog dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_login_dialog);
            dialog.show();

        }
        super.onRestoreInstanceState(savedInstanceState);
    }
    public final boolean isValidEmail(CharSequence Email) {
        if (Email.length() <= 0) {
            text1.requestFocus();
            text1.setError("FIELD CANNOT BE EMPTY");

            return false;
        } else if (Email == null) {
            return false;
        } else {
            if(!Patterns.EMAIL_ADDRESS.matcher(Email).matches())
            {
                text1.requestFocus();
                text1.setError("Email Id Is not proper");
            }
            return Patterns.EMAIL_ADDRESS.matcher(Email).matches();
        }
    }

    public final boolean Is_Valid_Password(String Password) {
        if (text2.length() <= 0) {
            text2.requestFocus();
            text2.setError("FIELD CANNOT BE EMPTY");
            return false;


        }
return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context=this;
        prefs = getSharedPreferences(LoginActivity.class.getSimpleName(),Context.MODE_PRIVATE);
        registrationId = prefs.getString(REG_ID, "");
        registrationpwd = prefs.getString(PWD, "");

        if(!(registrationId.isEmpty() && registrationpwd.isEmpty()))
        {
            Intent in=new Intent(getApplicationContext(),p_mainActivity.class);
            startActivity(in);


        }

        btn1 = (Button) findViewById(R.id.log);
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }

                final Dialog dialog = new Dialog(LoginActivity.this
                );
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.activity_login_dialog);
                //  dialog.setTitle("Student Login");
               dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setCancelable(true);
                save = (Button) dialog.findViewById(R.id.login);
                btnCancel = (Button) dialog.findViewById(R.id.cancel);
                dialog.show();

                save.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if(!isInternet)
                        {
                            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                        text1 = (EditText)dialog.findViewById(R.id.user_id);
                        text2 = (EditText)dialog.findViewById(R.id.password);
                        STUDENTS_USER_ID = text1.getText().toString().trim();
                        STUDENTS_PASSWORD = text2.getText().toString().trim();
                        new Students_login().execute(getString(R.string.AdminIp)+"/Stud_Login", STUDENTS_USER_ID, STUDENTS_PASSWORD);
                       check();}
                    }
                });


                btnCancel.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });



    }
    public class Students_login extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                    nameValuePairList.add(new BasicNameValuePair("semailid", params[1]));
                    nameValuePairList.add(new BasicNameValuePair("spassword", params[2]));
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost post = new HttpPost(params[0]);
                    post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                    HttpResponse response = httpClient.execute(post);
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    Register = data;
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    Register = "2";

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

                if (Register.equals("1")) {

                    prefs = getSharedPreferences(LoginActivity.class.getSimpleName(),Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(REG_ID,"text1");
                    editor.putString(PWD,"text2");
                    editor.commit();
                    Intent in=new Intent(getApplicationContext(),Result.class);
                    startActivity(in);
                    overridePendingTransition( R.anim.page_in_left, R.anim.page_out_left);

                   finish();

                }
                else if (Register.equals("2")) {
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                }

                else if (Register.equals("0")){
                    Toast.makeText(getApplicationContext(), "Please enter valid user id & password...", Toast.LENGTH_SHORT).show();
                }
            }
        }


    private void check()
    {
        Boolean email_result = isValidEmail(STUDENTS_USER_ID);

        Boolean password_result = Is_Valid_Password(STUDENTS_PASSWORD);
        if (email_result == false) {
            Toast.makeText(getApplicationContext(), "Enter valid Email id...", Toast.LENGTH_SHORT).show();
        } else if (password_result == false) {
            Toast.makeText(getApplicationContext(), "Enter valid password..", Toast.LENGTH_SHORT).show();
        }


    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }



}




