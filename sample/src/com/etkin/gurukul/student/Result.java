package com.etkin.gurukul.student;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Result extends ActionBarActivity {
    ArrayList<String> course=new ArrayList<String>();
    ArrayList<String>year =new ArrayList<String>();
    ArrayList<String>subjectda=new ArrayList<String>();
    String selectItem1,selectItem2,selectItem3;
    TextView text1,text2,text3;
    String a1,a2,a3;
    Button btn1,btn2;
    Toolbar toolbar;
    SharedPreferences prefs;
    String crs,std,sub;
public static ActionBarActivity result;
    ImageView imageView;
    ConnectionDetector cd;
    ProgressDialog pb;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        context = this;
        pb=new ProgressDialog(context);
        text1=(TextView)findViewById(R.id.course);
        text2=(TextView)findViewById(R.id.standard);
        text3=(TextView)findViewById(R.id.subject);
        result=this;
        btn1=(Button)findViewById(R.id.submit);
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        btn2=(Button)findViewById(R.id.cancel);
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }
        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else {
                    if(text1!=null && text2!=null && text3!=null)
                    {
                        Intent in = new Intent(getApplicationContext(), p_mainActivity.class);
                        // in.putExtra("course",selectItem1);
                        // in.putExtra("standard",selectItem2);
                        // in.putExtra("subject",selectItem3);

                        startActivity(in);
                        overridePendingTransition(R.anim.page_in_left, R.anim.page_out_left);
                    }}
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else {

                    Intent in = new Intent(getApplicationContext(), MenuActivity.class);
                    // in.putExtra("course",selectItem1);
                    // in.putExtra("standard",selectItem2);
                    // in.putExtra("subject",selectItem3);

                    startActivity(in);
                    overridePendingTransition(R.anim.page_in_left, R.anim.page_out_left);
                }
            }
        });
       /* prefs = getSharedPreferences(Result.class.getSimpleName(),Context.MODE_PRIVATE);
       crs= prefs.getString(CRS, "");
       std = prefs.getString(STD, "");
       sub = prefs.getString(SUB, "");*/

        btn1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else
                if(text1.length() == 0) {
                    text1.setError("Select course");
                    Toast.makeText(getApplicationContext(), "Please select course", Toast.LENGTH_SHORT).show();
                }
                else if(text2.length()==0){
                    text2.setError( "Select standard" );
                    Toast.makeText(getApplicationContext(), "Please select standard", Toast.LENGTH_SHORT).show();}
                else if(text3.length()==0){
                    text3.setError( "Select subject" );
                    Toast.makeText(getApplicationContext(), "Please select subject", Toast.LENGTH_SHORT).show();}
                else
                {
                    prefs = getSharedPreferences(Result.class.getSimpleName(),Context.MODE_PRIVATE);


                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("CRS",text1.getText().toString());
                editor.putString("STD",text2.getText().toString());
                editor.putString("SUB",text3.getText().toString());
                editor.commit();


                Intent in = new Intent(getApplicationContext(), p_mainActivity.class);

                startActivity(in);

            }}
        });

        text1.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else {

                course.clear();
                    text2.setText("");
                    text3.setText("");
                new showcourse().execute(getString(R.string.AdminIp)+"/course");

               // Courses();
            }}
        });
        text2.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else {

                year.clear();

                    text3.setText("");
                new years().execute(getString(R.string.AdminIp)+"/standard",a1);



            }}
        });

        text3.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
// TODO Auto-generated method stub
                if(!isInternet)
                {
                    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
                }
                else {
                subjectda.clear();

                new showsub().execute(getString(R.string.AdminIp)+"/subject", a1, a2);

            }}
        });

    }
    public class showcourse extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);
                // post.setEntity(new UrlEncodedFormEntity(nameValuePairList));


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("coursedetail");
                for (int i = 0; i < jarray.length()-1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    subjectpojo company1 = new subjectpojo();
                    company1.setCourse(jrealobj.getString("course"));
                    // branch.add(company.getsub().toString());
                    course.add(company1.getCourse().toString());
                }
            } catch (ClientProtocolException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            }catch (NullPointerException e2)
            {

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            Courses();
        }

    }
    String []abc;
    public void Courses()
    {
        abc=(String[])course.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course...");
        ad1.setSingleChoiceItems(abc,0,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                a1=abc[array];
                selectItem1=a1;
                text1.setText(a1);
                dialog.dismiss();

            }
        });
        ad1.show();



    }
    public class years extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            showProgressDialog();


        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {


                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("coursesName",params[1]));

                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("standard");
                for (int i = 0; i < jarray.length()-1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    subjectpojo company2 = new subjectpojo();
                    company2.setCourse(jrealobj.getString("year"));
                    // branch.add(company.getsub().toString());
                    year.add(company2.getCourse().toString());

                }
            } catch (ClientProtocolException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            }catch (NullPointerException e2)
            {

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            classyear();
        }
    }
    String []pqr;
    public void classyear()
    {
        pqr=(String[])year.toArray(new String[0]);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Year...");


        ad1.setSingleChoiceItems(pqr,0,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {

                a2=pqr[array];
                selectItem2=a2;
                text2.setText(a2);

                dialog.dismiss();

            }
        });
        ad1.show();
}
    public class showsub extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {

                List<NameValuePair>nameValuePairs=new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("coursesName",params[1]));
                nameValuePairs.add(new BasicNameValuePair("standard",params[2]));


                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);
                JSONArray jarray = jobj.getJSONArray("subjectdata");
                for (int i = 0; i < jarray.length()-1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    subjectpojo company = new subjectpojo();
                    company.setCourse(jrealobj.getString("sub"));
                    subjectda.add(company.getCourse().toString());


                }
            } catch (ClientProtocolException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            }catch (NullPointerException e2)
            {

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            showSelectDialog();
        }

    }
    String []zzz;

    public void showSelectDialog()
    {
        zzz=(String[])subjectda.toArray(new String[0]);
        final AlertDialog.Builder ad3 = new AlertDialog.Builder(this);
        ad3.setTitle("Select Subject...");
        ad3.setSingleChoiceItems(zzz,0,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                a3=zzz[which];
                selectItem3=a3;
                text3.setText(a3);
                dialog.dismiss();

            }
        });
        ad3.show();

    }

    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }

}



