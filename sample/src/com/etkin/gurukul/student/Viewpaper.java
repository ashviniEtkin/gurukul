package com.etkin.gurukul.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Viewpaper extends ActionBarActivity {


    protected RecyclerView mRecyclerView;
    ImageView imageView;
    protected viewpaperAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<viewpaperpojo> mDatasetHome = new ArrayList<viewpaperpojo>();
    Context context;
    TextView title, date;
    //ConnectionDetector cd;
    String Register;
    Toolbar toolbar;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID = "email";
    public static final String PROPERTY_PWD = "pwd";


    //        public static Viewpaper viewpaper;
    public Viewpaper viewpaper;
    String jobFileName;
    private ProgressDialog pd;
    Intent in;
    String s1, s2, s3;
    String crs, std, sub;
    String CRS, STD, SUB;
    SharedPreferences prefs;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpaper);
        context = this;
        viewpaper = this;
        //  savedInstanceState=getIntent().getExtras();
        //   in=getIntent();
        cd = new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        // s2=savedInstanceState.getString("standard");
        prefs = getSharedPreferences(Result.class.getSimpleName(), Context.MODE_PRIVATE);
        std = prefs.getString("STD", "");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.download_view);
        //initData();

        imageView = (ImageView) findViewById(R.id.img);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        imageView.startAnimation(myFadeInAnimation);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (!isInternet) {
            Toast.makeText(getApplicationContext(), "Please check ur internet connection", Toast.LENGTH_LONG).show();
        } else {
            new DownloadFile().execute(getString(R.string.AdminIp) + "/information", std);

        }
    }


    private class DownloadFile extends AsyncTask<String, String, Boolean> {


        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);


                nameValuePairs.add(new BasicNameValuePair("standard", params[1]));

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);
                JSONArray jArray = jobj.getJSONArray("Download");
                for (int i = 0; i < jArray.length() - 1; i++) {
                    JSONObject jrealobj = jArray.getJSONObject(i);
                    viewpaperpojo company = new viewpaperpojo();
                    company.setTitle(jrealobj.getString("title"));
                    company.setFilename(jrealobj.getString("filename"));
                    company.setDate(jrealobj.getString("date"));

                    company.setDesc(jrealobj.getString("desc"));
                    //company.setNoticedesc(jrealobj.getString("noticedesc"));
                    mDatasetHome.add(company);
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                Register = "6";
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   showProgress();
        }

     /*
        }
*/

        @Override
        protected void onPostExecute(Boolean s) {
            //super.onPostExecute(s);

            super.onPostExecute(s);
            mAdapter = new viewpaperAdapter(Viewpaper.this, mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);


        }

        //

        //}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {


            Logout();

        } else if (id == R.id.refresh_settings) {
            onRestart();
        } else if (id == R.id.home_settings) {
            Intent in = new Intent(this, p_mainActivity.class);
            startActivity(in);
        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if (Result.result != null) {
            Result.result.finish();
        }
        finish();
    }

    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(Viewpaper.this, Viewpaper.class);  //your class
        startActivity(i);
        finish();

    }


}






