package com.etkin.gurukul.student;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 4/1/2015.
 */
public class NoticeAdapter  extends RecyclerView.Adapter<NoticeAdapter.InboxViewHolder> {
    private ArrayList<NoticePojo> myDatas;
    Context context;
    ProgressDialog pb;
    String Delete1;

    public void setDataAdaptor(ArrayList<NoticePojo> mDatasetHome) {
        myDatas=mDatasetHome;
    }

    StudentInbox stud;
    public NoticeAdapter(StudentInbox context,ArrayList<NoticePojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
        stud=context;
        pb = new ProgressDialog(context);
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.notice_row, parent, false);
        this.context = context;
        ImageView img=(ImageView)itemView.findViewById(R.id.img);
        RotateAnimation rotate
                = new RotateAnimation(0.0f, 1080.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        ScaleAnimation scale
                = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        AnimationSet set = new AnimationSet(true);
        set.addAnimation(rotate);
        set.addAnimation(scale);
        set.addAnimation(alpha);
        set.setDuration(5000);
        View newspaper1 = (ImageView) itemView.findViewById(R.id.btn);
        newspaper1.startAnimation(set);



        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final NoticePojo myData=myDatas.get(position);
        holder.date.setText(myData.getDate());
        holder.noticetype.setText(myData.getNoticetitle());
        holder.noticedesc.setText(myData.getNoticedesc());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this notice ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int i) {

                                new DeleteNotice().execute(context.getString(R.string.AdminIp) + "/DeleteNotice1", myData.getId(), "student");


                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            NoticePojo data=myDatas.get(position);

            public void onClick(View view) {
              //  stud.overridePendingTransition(R.anim.page_in_left, R.anim.page_out_left);
                Intent intent = new Intent(holder.context, NoticeSub.class);

                intent.putExtra("noticedate", data.getDate());
                intent.putExtra("noticetitle", data.getNoticetitle());
                intent.putExtra("noticedesc", data.getNoticedesc());


                holder.context.startActivity(intent);


            }


        });

    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView date,noticetype,noticedesc;
        ImageView imageView;
        private Context context;
        ImageView img;
        protected Button delete;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context=context;


            imageView=(ImageView)itemView.findViewById(R.id.img);
            date = (TextView) itemView.findViewById(R.id.date);
            noticetype = (TextView) itemView.findViewById(R.id.notice_text);
            noticedesc = (TextView) itemView.findViewById(R.id.notice);
            img=(ImageView)itemView.findViewById(R.id.btn);
            delete=(Button)itemView.findViewById(R.id.delete);

        }

    }

    public class DeleteNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                nameValuePairList.add(new BasicNameValuePair("type", params[2]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                Delete1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Delete1 = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (Delete1.equals("6")){
                Toast.makeText(context, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (Delete1.equals("1")) {

                Toast.makeText(context, "Notice deleted successfully...", Toast.LENGTH_LONG).show();

                StudentInbox.notice.finish();



            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }


}
