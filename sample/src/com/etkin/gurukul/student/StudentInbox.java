package com.etkin.gurukul.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class StudentInbox extends ActionBarActivity {
    protected RecyclerView mRecyclerView;
    protected NoticeAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<NoticePojo> mDatasetHome = new ArrayList<NoticePojo>();
    Context context;
    private Toolbar toolbar;
    public static ActionBarActivity notice;
    ProgressDialog pb;
    String registrationId;
    String REG_ID,PWD;
    String Register;
    ImageView imageView;
    ConnectionDetector cd;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_inbox);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        context = this;
        notice=this;
        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        imageView=(ImageView)findViewById(R.id.img);
        pb=new ProgressDialog(context);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        imageView.startAnimation(myFadeInAnimation);
        mRecyclerView = (RecyclerView) findViewById(R.id.notice_view);
        //initData();
        mLayoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        prefs = getSharedPreferences(MenuActivity.class.getSimpleName(),Context.MODE_PRIVATE);
        registrationId = prefs.getString("email", "");
        if(!isInternet)
{
    Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
}
else{
        new Notice().execute(getString(R.string.AdminIp)+"/NoticeBoard",registrationId);
    }}

    public class Notice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);

                nameValuePairs.add(new BasicNameValuePair("email",params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
               post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);
                JSONArray jArray = jobj.getJSONArray("notice");
                for (int i = 0; i < jArray.length() - 1; i++) {
                    JSONObject jrealobj = jArray.getJSONObject(i);
                    NoticePojo company = new NoticePojo();
                    company.setDate(jrealobj.getString("noticedate"));
                    company.setNoticetitle(jrealobj.getString("noticetitle"));
                    company.setNoticedesc(jrealobj.getString("noticedesc"));
                    company.setId(jrealobj.getString("id"));
                    mDatasetHome.add(company);
                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                Register = "6";
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();

            mAdapter=new NoticeAdapter(StudentInbox.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);


        }
    }

    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Toast.makeText(getApplicationContext(),"Welcome to Home",Toast.LENGTH_SHORT).show();
          Intent in =new Intent(this,p_mainActivity.class);
           startActivity(in);

        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {


        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();

        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }

    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(StudentInbox.this, StudentInbox.class);  //your class
        startActivity(i);
        finish();

    }




}















