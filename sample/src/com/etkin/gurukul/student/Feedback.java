package com.etkin.gurukul.student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Feedback extends ActionBarActivity {

    int year, month, day;
    String date, Register1;
    EditText text1, text2, text3, text4;
    Button btn1, btn2;
    String data;
    ImageView imageView;
    String Feedbackby, Feedbackon, Feedbackfor, Feedback;
    SharedPreferences settings;
    public static final String REG_ID="email";
    public static final String PWD="pwd";
String CRS,STDS,SUB;
    SharedPreferences prefs;
Toolbar toolbar;
ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        cd=new ConnectionDetector(getApplicationContext());
        final Boolean isInternet = cd.isConnectingToInternet();
        text2 = (EditText) findViewById(R.id.feedbackb);
        text3 = (EditText) findViewById(R.id.feedbackc);
        text4 = (EditText) findViewById(R.id.feedbackd);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        date = String.format("%d-%d-%d", day, month + 1, year);
        btn2=(Button)findViewById(R.id.cancel);
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }

        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent in = new Intent(getApplicationContext(), p_mainActivity.class);
                // in.putExtra("course",selectItem1);
                // in.putExtra("standard",selectItem2);
                // in.putExtra("subject",selectItem3);

                startActivity(in);

            }
        });
        btn1 = (Button) findViewById(R.id.submit);
        btn1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Feedbackon = text2.getText().toString().trim();
                Feedbackfor = text3.getText().toString().trim();
                Feedback = text4.getText().toString().trim();
                if(!isInternet){
                    Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
                }else  if(text2.length() == 0)
                    text2.setError( "Sender Name is required!" );
                else if(text3.length()==0)
                    text3.setError( "Feedback Topic is required!" );
                else if(text4.length()==0)
                    text4.setError( "Description is required!" );
                else
                {
              new Feedbacks().execute(getString(R.string.AdminIp)+"/Feedback", date, Feedbackon, Feedbackfor, Feedback);

            }}
        });

    }


    public class Feedbacks extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("feedbackBy", params[2]));
                nameValuePairList.add(new BasicNameValuePair("title", params[3]));
                nameValuePairList.add(new BasicNameValuePair("description", params[4]));


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                String abc = "0";
              //  Register1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Register1 = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return true;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);



           /* if (data.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }*/
            if(data.equals("6"))
            {
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (data.equals("1")) {

                Toast.makeText(getApplication(), "Feedback send....", Toast.LENGTH_LONG).show();

            }
            else if (data.equals("2")) {
                Toast.makeText(getApplication(), "You have already send....", Toast.LENGTH_LONG).show();
            }


            else {
                    Toast.makeText(getApplicationContext(), "Feedback send...", Toast.LENGTH_LONG).show();
                    Intent in2 = new Intent(getApplicationContext(), p_mainActivity.class);
                    startActivity(in2);

                }



            }


        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);

        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();


        if(Result.result!=null){
            Result.result.finish();}
        finish();
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(com.etkin.gurukul.student.Feedback.this, com.etkin.gurukul.student.Feedback.class);  //your class
        startActivity(i);

        finish();

    }




}

