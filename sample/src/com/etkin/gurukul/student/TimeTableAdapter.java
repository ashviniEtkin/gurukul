package com.etkin.gurukul.student;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 4/2/2015.
 */
public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.InboxViewHolder> {
private ArrayList<TimeTablePojo> myDatas;
        Context context;
public void setDataAdaptor(ArrayList<TimeTablePojo> mDatasetHome) {
        myDatas=mDatasetHome;

        }
    public TimeTableAdapter(Context context,ArrayList<TimeTablePojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.timetable_row, parent, false);

        RotateAnimation rotate
                = new RotateAnimation(0.0f, 1080.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        ScaleAnimation scale
                = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        AnimationSet set = new AnimationSet(true);
        set.addAnimation(rotate);
        set.addAnimation(scale);
        set.addAnimation(alpha);
        set.setDuration(5000);
        View newspaper1 = (ImageView) itemView.findViewById(R.id.btn);
        newspaper1.startAnimation(set);


        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder,final int position) {
        final TimeTablePojo myData=myDatas.get(position);
        holder.tvMsg.setText(myData.getTestdate());
        holder.heading.setText(myData.getSyllabus());
       // holder.message.setText(myData.getMessage());



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            TimeTablePojo data=myDatas.get(position);

            public void onClick(View view) {
                Intent intent = new Intent(holder.context, Timetable_sub.class);
                intent.putExtra("testdate", data.getTestdate());
                intent.putExtra("course", myData.getCourse());
               // intent.putExtra("message", data.getStandard());
               intent.putExtra("standard",myData.getStandard());
               intent.putExtra("subject",myData.getSubject());
               intent.putExtra("testtime",myData.getTesttime());
               intent.putExtra("syllabus",myData.getSyllabus());
                intent.putExtra("testtype",myData.getTesttype());


                holder.context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView tvMsg,heading;
        ImageView imageView;
        private Context context;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context=context;
            tvMsg = (TextView) itemView.findViewById(R.id.date);
            imageView=(ImageView)itemView.findViewById(R.id.btn);
            heading = (TextView) itemView.findViewById(R.id.notice);


        }

    }
}

