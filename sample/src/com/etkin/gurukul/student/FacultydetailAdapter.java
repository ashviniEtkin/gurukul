package com.etkin.gurukul.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etkin.gurukul.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 4/6/2015.
 */
public class FacultydetailAdapter extends RecyclerView.Adapter<FacultydetailAdapter.InboxViewHolder> {
    private ArrayList<Facultydetailpojo> myDatas;
    Context context;

    RequestQueue queue;
    ImageLoader imgLoader;
    ProgressDialog pb;




    public void setDataAdaptor(ArrayList<Facultydetailpojo> mDatasetHome) {
        myDatas=mDatasetHome;

    }

    public FacultydetailAdapter(Context context,ArrayList<Facultydetailpojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
       pb = new ProgressDialog(context);
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        queue= MySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(parent.getContext())));
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.facultydetail_row, parent, false);
        this.context = context;

        return new InboxViewHolder(itemView, parent.getContext());

    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final Facultydetailpojo myData=myDatas.get(position);
        holder.networkImageView.setImageUrl("http://etkininfotech.com/image/"+myData.getPhotos(),imgLoader);
        holder.names.setText(myData.getName());
        holder.emails.setText(myData.getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            Facultydetailpojo data=myDatas.get(position);

            public void onClick(View view) {

                Intent intent = new Intent(holder.context, Facultydetailsub.class);
                intent.putExtra("name", data.getName());
               // intent.putExtra("photo", data.getName());
                intent.putExtra("email", data.getEmail());
                intent.putExtra("qualification", data.getQualification());
                intent.putExtra("class", data.getCourses());
                intent.putExtra("course", data.getClasses());
                intent.putExtra("subject", data.getSubject());
                intent.putExtra("contactno", data.getContactno());

                holder.context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView names,emails;
        protected NetworkImageView networkImageView;
        private Context context;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context=context;



            networkImageView=(NetworkImageView)itemView.findViewById(R.id.photo);
            names = (TextView) itemView.findViewById(R.id.name);
            emails = (TextView) itemView.findViewById(R.id.email);


        }

    }

}
