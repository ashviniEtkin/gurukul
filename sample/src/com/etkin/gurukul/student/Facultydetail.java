package com.etkin.gurukul.student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Facultydetail extends ActionBarActivity {
    protected RecyclerView mRecyclerView;
    protected FacultydetailAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Facultydetailpojo> mDatasetHome = new ArrayList<Facultydetailpojo>();
    Context context;
     public static Activity abc;
    String Register;
    Toolbar toolbar;
    String CRS,STDS,SUB;
    String std,STD;
    ConnectionDetector cd;
    ImageView imageView;
    int PRIVATE_MODE=0;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultydetail);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        context = this;
        abc=this;
        imageView=(ImageView)findViewById(R.id.img);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        imageView.startAnimation(myFadeInAnimation);

        cd=new ConnectionDetector(getApplicationContext());
        Boolean isInternet = cd.isConnectingToInternet();
        mRecyclerView = (RecyclerView) findViewById(R.id.notice_view);
        //initData();
        prefs = getSharedPreferences(Result.class.getSimpleName(),Context.MODE_PRIVATE);

        std = prefs.getString("STD", "");
        mLayoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(!isInternet)
        {
            Toast.makeText(getApplicationContext(),"Please check ur internet connection",Toast.LENGTH_LONG).show();
        }
        else {
            new Faculty().execute(getString(R.string.AdminIp)+"/GetStaffdetail", std);
        }
    }
    public class Faculty extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);

                nameValuePairs.add(new BasicNameValuePair("standard",params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
               post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);
                JSONArray jArray = jobj.getJSONArray("staffdata");
                for (int i = 0; i < jArray.length() - 1; i++) {
                    JSONObject jrealobj = jArray.getJSONObject(i);
                    Facultydetailpojo company = new Facultydetailpojo();
                    company.setName(jrealobj.getString("name"));
                    company.setPhotos(jrealobj.getString("photo"));

                    company.setEmail(jrealobj.getString("email"));
                    company.setQualification(jrealobj.getString("qualification"));
                    company.setClasses(jrealobj.getString("std"));
                    company.setCourses(jrealobj.getString("course"));
                    company.setSubject(jrealobj.getString("sub"));
                    company.setContactno(jrealobj.getString("contact_no"));
                    mDatasetHome.add(company);
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                Register = "6";
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);


            mAdapter = new FacultydetailAdapter(Facultydetail.this, mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);


        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_settings) {



            Logout();

        }
        else if (id==R.id.refresh_settings)
        {
            onRestart();
        }

        else if (id==R.id.home_settings)
        {
            Intent in =new Intent(this,p_mainActivity.class);
            startActivity(in);

        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Successfully logged out.... ", Toast.LENGTH_LONG).show();


        if(Result.result!=null){
        Result.result.finish();}
        finish();
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }
    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(Facultydetail.this, Facultydetail.class);  //your class
        startActivity(i);

        finish();

    }




}
