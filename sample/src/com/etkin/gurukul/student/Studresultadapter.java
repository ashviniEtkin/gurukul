package com.etkin.gurukul.student;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 4/4/2015.
 */
public class Studresultadapter extends RecyclerView.Adapter<Studresultadapter.InboxViewHolder> {
private ArrayList<Studresultpojo> myDatas;
        Context context;
public void setDataAdaptor(ArrayList<Studresultpojo> mDatasetHome) {
        myDatas=mDatasetHome;

        }

       public Studresultadapter(Context context,ArrayList<Studresultpojo> myDatas)
        {
        this.myDatas=myDatas;
        this.context=context;
        }


@Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.studentresult_row, parent, false);
        this.context = context;

        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final Studresultpojo myData=myDatas.get(position);

        holder.date.setText(myData.getDate());
        holder.syllabus.setText(myData.getSubjectname());
        holder.totalMark.setText(myData.getTotalmarks());
        holder.obtainMark.setText(myData.getMarks());


         }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView date,syllabus,totalMark,obtainMark;

        private Context context;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context=context;



            date = (TextView) itemView.findViewById(R.id.date);
            syllabus = (TextView) itemView.findViewById(R.id.syllabus);
            totalMark = (TextView) itemView.findViewById(R.id.totalMark);
            obtainMark = (TextView) itemView.findViewById(R.id.obtainMark);



        }

    }

}
