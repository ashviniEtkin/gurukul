package com.etkin.gurukul.student;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.RippleView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class StudentFilter extends ActionBarActivity {

    Button testSubject;
    RippleView show;
    ProgressDialog pg;
    ArrayList s;
    String []TESTSUBJECT1;
    String testsubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_filter);
        testSubject= (Button) findViewById(R.id.testSubject);
        show= (RippleView) findViewById(R.id.show);
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is Loading.......");
        pg.setCancelable(false);


        testSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testSubject.clearComposingText();
                new DisplayTestSubject().execute(getString(R.string.AdminIp) + "/Get_TestSubject");
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(getApplicationContext(),StudentResult.class);
                in.putExtra("subject",testSubject.getText().toString().trim());
                startActivity(in);
            }
        });
    }

    public class DisplayTestSubject extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }
        @Override
        protected Boolean doInBackground(String... arg0)
        {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);


                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONArray jsonArray=new JSONArray(data);
                    s=new ArrayList();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jrealobj = jsonArray.getJSONObject(i);
                        s.add(jrealobj.getString("subject"));


                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectTestSubject();

            }
        }

    }

    private void showSelectTestSubject() {

        TESTSUBJECT1=(String[])s.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Subject...");
        ad1.setSingleChoiceItems(TESTSUBJECT1, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                testsubject = TESTSUBJECT1[array];
                testSubject.setText("");
                testSubject.setText(testsubject);


                dialog.dismiss();

            }
        });
        ad1.show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
