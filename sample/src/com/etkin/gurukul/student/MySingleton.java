package com.etkin.gurukul.student;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Administrator on 4/7/2015.
 */
public class MySingleton {
    private static MySingleton sInstance=null;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;
    private MySingleton(){
        mRequestQueue= Volley.newRequestQueue(Facultydetail.abc);
        mImageLoader=new ImageLoader(mRequestQueue,new ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> cache=new LruCache((int)(Runtime.getRuntime().maxMemory()/1024)/8);

            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }


            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }
    public static MySingleton getInstance(){
        if(sInstance==null)
        {
            sInstance=new MySingleton();
        }
        return sInstance;
    }
    public RequestQueue getRequestQueue(){
        return mRequestQueue;
    }
    public ImageLoader getImageLoader(){
        return mImageLoader;
    }
}
