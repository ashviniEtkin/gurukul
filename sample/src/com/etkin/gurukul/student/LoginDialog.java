package com.etkin.gurukul.student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.etkin.gurukul.R;


public class LoginDialog extends Activity {
    String registrationId,registrationpwd;
    String registrationId_admin,registrationpwd_admin;
    int PRIVATE_MODE=0;
    SharedPreferences settings;
    public static final String PROPERTY_REG_ID="email";
    public static final String PROPERTY_PWD="pwd";
    public static final String REG_ID="email";
    public static final String PWD="pwd";
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_dialog);

        prefs = getSharedPreferences(LoginActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        registrationId = prefs.getString(PROPERTY_REG_ID, "");
        registrationpwd = prefs.getString(PROPERTY_PWD, "");

        if(!(registrationId.isEmpty() && registrationpwd.isEmpty()))
        {
            Intent in=new Intent(getApplicationContext(),p_mainActivity.class);
            startActivity(in);
            overridePendingTransition( R.anim.page_in_left, R.anim.page_out_left);

        }

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }



}
