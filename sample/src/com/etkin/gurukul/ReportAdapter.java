package com.etkin.gurukul;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/8/2015.
 */
public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {

    private ArrayList<Report_Pojo> reportData;
    public static Context con;

    public ReportAdapter(Context context,ArrayList<Report_Pojo> reportData) {
        //this.cardData = cardData;
        this.reportData=reportData;
        con=context;

    }


    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.report_list_row, parent, false);
        return new ReportViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ReportViewHolder holder, int i) {
        final Report_Pojo report_pojo=reportData.get(i);


        holder.date.setText(report_pojo.getDate());
        holder.title.setText(report_pojo.getType());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context,Sub_Report_List.class);
                intent.putExtra("date", report_pojo.getDate());
                intent.putExtra("title", report_pojo.getType());
                intent.putExtra("description", report_pojo.getDescription());
                holder.context.startActivity(intent);

            }
        });
        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return reportData.size();
    }

    public static class ReportViewHolder extends RecyclerView.ViewHolder {

        protected TextView date,title;
        private Context context;

        public ReportViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            date=(TextView)itemView.findViewById(R.id.date);
            title=(TextView)itemView.findViewById(R.id.reportType);

        }
    }
}
