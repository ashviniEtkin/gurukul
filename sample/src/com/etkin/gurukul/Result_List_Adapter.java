package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etkin.gurukul.student.StudentResult;

import java.util.ArrayList;

/**
 * Created by Administrator on 7/27/2015.
 */
public class Result_List_Adapter extends RecyclerView.Adapter<Result_List_Adapter.ListViewHolder> {
    private ArrayList<Attendance_List_Pojo> resultData;
    public static Context con;
    ProgressDialog pb;
    String classes,courses,Subject;

    public Result_List_Adapter(){

    }

    public Result_List_Adapter(Context context,ArrayList<Attendance_List_Pojo> resultData,String classes,String courses,String Subject) {
        //this.cardData = cardData;
        this.resultData=resultData;
        con=context;
        pb = new ProgressDialog(con);
        this.classes=classes;
        this.courses=courses;
        this.Subject=Subject;
    }

    @Override
    public Result_List_Adapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.attendance_student_list_row, parent, false);
        return new ListViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int i) {
        final Attendance_List_Pojo list_pojo=resultData.get(i);


        holder.name.setText(list_pojo.getStudName());
        holder.roll.setText(list_pojo.getRollNo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(con,StudentResult.class);
                intent.putExtra("studId",list_pojo.getStudId());
                intent.putExtra("classes",classes);
                intent.putExtra("courses",courses);
                intent.putExtra("testSubject",Subject);
                intent.putExtra("admin","yes");
                con.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return resultData.size();
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {

        protected TextView name,roll;
        private Context context;

        public ListViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            name=(TextView)itemView.findViewById(R.id.stud_name);
            roll=(TextView)itemView.findViewById(R.id.stud_roll);


        }
    }
}
