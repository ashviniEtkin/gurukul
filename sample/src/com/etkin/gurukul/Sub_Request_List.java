package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Sub_Request_List extends ActionBarActivity {

    TextView by,title,description;
    Button Verify,Decline;
    String By,Title,Description,id,verify,decline;
    ProgressDialog pb,pb1;
    private static ActionBarActivity sub_request_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__request__list);
        by=(TextView)findViewById(R.id.requestBy);
        title=(TextView)findViewById(R.id.requestTitle);
        description=(TextView)findViewById(R.id.requestDescription);
        Verify=(Button)findViewById(R.id.Verify);
        Decline=(Button)findViewById(R.id.Decline);
        pb=new ProgressDialog(Sub_Request_List.this);
        pb1=new ProgressDialog(Sub_Request_List.this);
        sub_request_list=this;
        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        By=intent.getStringExtra("by");
        Title=intent.getStringExtra("title");
        Description=intent.getStringExtra("description");

        by.setText(By);
        title.setText(Title);
        description.setText(Description);

        Verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VerifyRequest().execute(getString(R.string.AdminIp)+"/VerifyRequest",id);
            }
        });

        Decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeclineRequest().execute(getString(R.string.AdminIp)+"/DeclineRequest",id);
            }
        });


    }


    public class VerifyRequest extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                verify = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                verify = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (verify.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (verify.equals("1")) {

                Toast.makeText(getApplicationContext(), "Request verified successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Request_List.this, A_Request_List.class);
                startActivity(intent1);
                A_Request_List.request_list.finish();
                Sub_Request_List.sub_request_list.finish();

            }
        }
    }

    public class DeclineRequest extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog1();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                decline = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                decline = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog1();

            if (decline.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (decline.equals("1")) {

                Toast.makeText(getApplicationContext(), "Request declined successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Request_List.this, A_Request_List.class);
                startActivity(intent1);
                A_Request_List.request_list.finish();
                Sub_Request_List.sub_request_list.finish();

            }
        }
    }

    public void showProgressDialog() {

        pb.setMessage("Verifying request....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }
    public void showProgressDialog1() {

        pb1.setMessage("Declining request....");
        pb1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb1.setIndeterminate(true);
        pb1.show();
    }


    public void dismissProgressDialog1() {

        pb1.dismiss();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__request__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
