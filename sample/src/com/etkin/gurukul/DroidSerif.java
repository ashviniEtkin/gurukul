package com.etkin.gurukul;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Administrator on 3/19/2015.
 */
public class DroidSerif extends TextView {
    private static Typeface droidtype;

    public DroidSerif(Context context) {
        super(context);
        init();
    }

    public DroidSerif(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DroidSerif(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init() {
        if (!isInEditMode())
        {
            if (droidtype == null) {
                droidtype = Typeface.createFromAsset(getContext().getAssets(), "font/DroidSerif-Italic.ttf");
            }
            setTypeface(droidtype);
        }
}}
