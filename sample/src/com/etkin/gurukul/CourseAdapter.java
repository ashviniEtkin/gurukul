package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/14/2015.
 */
public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.CourseViewHolder>{

    private ArrayList<Course_Pojo> courseData;
    public static Context con;
    ProgressDialog pb;
    String verify;

    public CourseAdapter(Context context,ArrayList<Course_Pojo> courseData) {
        //this.cardData = cardData;
        this.courseData=courseData;
        con=context;
        pb = new ProgressDialog(con);

    }

    @Override
    public CourseAdapter.CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.course_list_row, parent, false);
        return new CourseViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(CourseAdapter.CourseViewHolder holder, int i) {
        final Course_Pojo course_pojo=courseData.get(i);


        holder.course.setText(course_pojo.getCourse());
        holder.standard.setText(course_pojo.getStandard());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteCourse().execute(con.getString(R.string.AdminIp)+"/DeleteCourse", course_pojo.getStandard(), course_pojo.getCourse());
            }
        });

        holder.itemView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return courseData.size();
    }

    public static class CourseViewHolder extends RecyclerView.ViewHolder {

        protected TextView course,standard;
        protected Button delete;
        private Context context;

        public CourseViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            course=(TextView)itemView.findViewById(R.id.courses);
            standard=(TextView)itemView.findViewById(R.id.standard);
            delete=(Button)itemView.findViewById(R.id.delete);

        }
    }

    public class DeleteCourse extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("standard", params[1]));
                nameValuePairList.add(new BasicNameValuePair("course", params[2]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                verify = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                verify = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (verify.equals("6")){
                Toast.makeText(con, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (verify.equals("1")) {

                Toast.makeText(con, "Course deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(con, A_Course_List.class);
                con.startActivity(intent1);
            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting course....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

}
