package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_Result_List extends ActionBarActivity {

    protected RecyclerView mRecyclerView;
    protected ResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Context context;
    ArrayList<Result_Pojo> mDatasetHome = new ArrayList<Result_Pojo>();
    ProgressDialog pb;
    Toolbar toolbar;
    public static ActionBarActivity result_list;
    ConnectionDetector cd;
    String classes,examType,subject,course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__result__list);
        toolbar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Student List");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;

        result_list=this;
        pb=new ProgressDialog(context);
        cd=new ConnectionDetector(getApplicationContext());
        Boolean isInternet = cd.isConnectingToInternet();
        mRecyclerView = (RecyclerView) findViewById(R.id.result_view);
        mLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent=getIntent();
        classes=intent.getStringExtra("classes");
        examType=intent.getStringExtra("examType");
      //  subject=intent.getStringExtra("subject");
        course=intent.getStringExtra("course");

        new ResultList().execute(getString(R.string.AdminIp)+"/GetResult",classes,examType,course);

    }

    public class ResultList extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("classes", params[1]));
                nameValuePairList.add(new BasicNameValuePair("examType", params[2]));
              //  nameValuePairList.add(new BasicNameValuePair("subject", params[3]));
                nameValuePairList.add(new BasicNameValuePair("course", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                // JSONObject jobj = new JSONObject(data);
                // JSONArray jArray = jobj.getJSONArray("information");
                JSONArray jsonArray=new JSONArray(data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    Result_Pojo result_pojo = new Result_Pojo();

                    result_pojo.setsName(jrealobj.getString("studName"));
                    result_pojo.setMarks(jrealobj.getString("marks"));
                    result_pojo.setSubject(jrealobj.getString("subject"));

                    mDatasetHome.add(result_pojo);
                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            mAdapter=new ResultAdapter(A_Result_List.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);
        }
    }



    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__result__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
