package com.etkin.gurukul;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.balysv.materialripple.MaterialRippleLayout;
import com.etkin.gurukul.menu.MenuActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static com.astuetz.PagerSlidingTabStrip.CustomTabProvider;

public class QuickContactFragment extends DialogFragment {

    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private ContactPagerAdapter adapter;
    Button logout, changePassword, submit, cancel;
    EditText current, nwPassword, confirmPassword;
    ProgressDialog pb;
    String res;

    public static QuickContactFragment newInstance() {
        QuickContactFragment quickContactFragment = new QuickContactFragment();
        return quickContactFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getDialog() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }

        View root = inflater.inflate(com.etkin.gurukul.R.layout.fragment_quick_contact, container, false);

        tabs = (PagerSlidingTabStrip) root.findViewById(com.etkin.gurukul.R.id.tabs);
        pager = (ViewPager) root.findViewById(R.id.pager);
        logout = (Button) root.findViewById(R.id.logout);
        changePassword = (Button) root.findViewById(R.id.password);
        adapter = new ContactPagerAdapter(getActivity());
        pb=new ProgressDialog(getActivity());
        pager.setAdapter(adapter);

        tabs.setViewPager(pager);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Logout();
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.change_password);
                dialog.setCancelable(false);

                current = (EditText) dialog.findViewById(R.id.current);
                nwPassword = (EditText) dialog.findViewById(R.id.newPassword);
                confirmPassword = (EditText) dialog.findViewById(R.id.confirm);

                submit = (Button) dialog.findViewById(R.id.submit);
                cancel = (Button) dialog.findViewById(R.id.cancel);

                dialog.show();

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getActivity());

                        // set title
                        alertDialogBuilder.setTitle("Warning !");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Do you really want to change your password ?")
                                .setCancelable(false)
                                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int i) {

                                        SharedPreferences sharedPreferences2 = getActivity().getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                                        String login2=sharedPreferences2.getString("admin", "");



                                        if (nwPassword.getText().toString().trim().equals(confirmPassword.getText().toString().trim()))
                                        {
                                            new Change_Password().execute(getString(R.string.AdminIp) + "/ChangePassword", login2, nwPassword.getText().toString().trim(),current.getText().toString().trim());

                                        }
                                        else {
                                            Toast.makeText(getActivity(),"Password and Confirm password are not same",Toast.LENGTH_LONG).show();
                                        }

                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        });

                        alertDialogBuilder.show();

                    }
                });

            }
        });

        return root;
    }

    private void Logout() {
        SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent = new Intent(getActivity(), MenuActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart() {
        super.onStart();

        // change dialog width
        if (getDialog() != null) {

            int fullWidth = getDialog().getWindow().getAttributes().width;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                fullWidth = size.x;
            } else {
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                fullWidth = display.getWidth();
            }

            final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                    .getDisplayMetrics());

            int w = fullWidth - padding;
            int h = getDialog().getWindow().getAttributes().height;

            getDialog().getWindow().setLayout(w, h);
        }
    }

    public static class ContactPagerAdapter extends PagerAdapter implements CustomTabProvider {

        private final int[] ICONS = {R.drawable.ic_launcher_gplus, R.drawable.ic_launcher_gmail,
                R.drawable.ic_launcher_gmaps, R.drawable.ic_launcher_chrome};
        private final Context mContext;

        public ContactPagerAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return ICONS.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TextView textview = (TextView) LayoutInflater.from(mContext).inflate(R.layout.fragment_quickcontact, container, false);
            // textview.setText("PAGE "+position);
            container.addView(textview);
            return textview;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            container.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View v, Object o) {
            return v == o;
        }

        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            MaterialRippleLayout materialRippleLayout = (MaterialRippleLayout) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, parent, false);
            ((ImageView) materialRippleLayout.findViewById(R.id.image)).setImageResource(ICONS[position]);
            return materialRippleLayout;
        }
    }


    public class Change_Password extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("email", params[1]));
                nameValuePairList.add(new BasicNameValuePair("password", params[2]));
                nameValuePairList.add(new BasicNameValuePair("current", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                res=data;


            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();

            if(res.equals("0"))
            {
                Toast.makeText(getActivity(),"Currrent password does not match with your user id !",Toast.LENGTH_LONG).show();
            }
            else if (res.equals("1")){

                Toast.makeText(getActivity(), "Password has been changed successfully !", Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(getActivity(),"Something went wrong !",Toast.LENGTH_LONG).show();
                }


            }

    }



    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }

}
