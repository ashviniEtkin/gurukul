package com.etkin.gurukul;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class A_Counselor_Register extends ActionBarActivity {

    Bitmap facultyImageBitmap = null;
    String output, facultyImageString;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private String imagepath = null;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    private Uri fileUri; // file url to store image/video
    // to fetch courses

    String MobilePattern = "[0-9]{10}";
    ConnectionDetector cd;

    ImageView imgPreview,cam,gal;

    Button fdojbtn;
    String fdoj = null;
    String f_name,f_emailid,f_pass,f_qual,f_sal,f_cno,f_address;
    EditText f_nameet,f_emailidet,f_passet,f_qualet,f_salet,f_cnoet,f_addresset;
    ProgressDialog pg;
    public static ActionBarActivity counselor_register;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__counselor__register);
        counselor_register=this;
        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);
        cd=new ConnectionDetector(getApplicationContext());
        imgPreview = (ImageView) findViewById(R.id.fcameraimg);

        cam = (ImageView) findViewById(R.id.fcamerabtn);
        cam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                captureImage();
            }
        });
        gal = (ImageView) findViewById(R.id.fgallerybtn);
        gal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectImage();
            }
        });

        final DatePickerDialog dateDlg = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                fdoj= strDate.toString();
                fdojbtn.setText(fdoj);

            }}, 2011,0, 1);
        fdojbtn = (Button) findViewById(R.id.fdojb);
        fdojbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dateDlg.show();
            }
        });

        f_nameet= (EditText) findViewById(R.id.fnm);
        f_emailidet= (EditText) findViewById(R.id.femailin);
        f_passet= (EditText) findViewById(R.id.fpass);
        f_qualet= (EditText) findViewById(R.id.fqua);
        f_salet= (EditText) findViewById(R.id.fsal);
        f_cnoet= (EditText) findViewById(R.id.fcno);
        f_addresset= (EditText) findViewById(R.id.fadd);


        Button f_reg = (Button) findViewById(R.id.submitbtn);
        f_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                f_cno= f_cnoet.getText().toString().trim();
                final String eid1  = f_emailidet.getText().toString().trim();
                if(f_nameet.length()==0)
                    f_nameet.setError("You haven't specified the name");
                else if (!isValidEmail(eid1))
                    f_emailidet.setError("Invalid Email Format(eg: abc@gmail.com)");
                else if(f_passet.length()== 0)
                    f_passet.setError("You haven't specified the password");
                else if (f_qualet.length()==0)
                    f_qualet.setError("You haven't specified the Qualification");
                else  if(fdoj==null) {
                    fdojbtn.setError("You haven't selected date of joing");
                    fdojbtn.setText("Tap Date Of Joining");
                }
                else if(f_salet.length()==0)
                    f_salet.setError("You haven't specified the salary");
                else if(f_cnoet.length()==0)
                    f_cnoet.setError("You haven't specified Contact Number");
                else if (!f_cno.matches(MobilePattern))
                    f_cnoet.setError(" Contact Number must be 10 digit");
                else if(f_addresset.length()==0)
                    f_addresset.setError("You haven't specified Address");

                else
                {
                    f_name = f_nameet.getText().toString().trim();
                    if (facultyImageBitmap!=null) {
                        facultyImageString = encodeToBase64(facultyImageBitmap);
                    }else{
                        facultyImageString=getString(R.string.blankImage);
                    }
                    f_emailid= f_emailidet.getText().toString().trim();
                    f_pass= f_passet.getText().toString().trim();
                    f_qual= f_qualet.getText().toString().trim();
                    f_sal= f_salet.getText().toString().trim();
                    f_cno= f_cnoet.getText().toString().trim();
                    f_address= f_addresset.getText().toString().trim();
                    Boolean isInternet = cd.isConnectingToInternet();
                    if (!isInternet)
                    {
                        Toast.makeText(getApplicationContext(),"You seem to be offline !",Toast.LENGTH_SHORT).show();
                    }else {

                        new CounselorRegister().execute(getString(R.string.AdminIp)+"/Counselor_Register", f_name, f_name, facultyImageString, f_emailid, f_pass, fdoj, f_qual, f_sal, f_cno, f_address);
                    }
                }

            }
        });
    }

    public  class CounselorRegister extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("Cname1", params[1]));
            nameValuePairList.add(new BasicNameValuePair("Cphoto1", params[2]));
            nameValuePairList.add(new BasicNameValuePair("Cphotocode", params[3]));
            nameValuePairList.add(new BasicNameValuePair("Ceid1", params[4]));
            nameValuePairList.add(new BasicNameValuePair("Cpass1", params[5]));
            nameValuePairList.add(new BasicNameValuePair("Cdoj1", params[6]));
            nameValuePairList.add(new BasicNameValuePair("Cqual1", params[7]));
            nameValuePairList.add(new BasicNameValuePair("Csal1", params[8]));
            nameValuePairList.add(new BasicNameValuePair("Ccno1", params[9]));
            nameValuePairList.add(new BasicNameValuePair("Cadd1", params[10]));

            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                output = data;

            }

            catch(ClientProtocolException e1)
            {
                e1.printStackTrace();
            }
            catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            catch (UnknownHostException e1)
            {
                output = "6";
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pg.dismiss();
            if(output.equals("1"))
            {
                Toast.makeText(getApplicationContext(),"Counselor registered successfully...",Toast.LENGTH_LONG).show();
                A_Counselor_Register.counselor_register.finish();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Network Problem",Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                try {

                    // hide video preview
                    imgPreview.setVisibility(View.VISIBLE);
                    // bimatp factory
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger images
                    options.inSampleSize = 8;
                    final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                    facultyImageBitmap = bitmap;
                    imgPreview.setImageBitmap(bitmap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                imagepath = getPath(selectedImageUri);
                Bitmap bitmap = BitmapFactory.decodeFile(imagepath);
                facultyImageBitmap = bitmap;
                imgPreview.setImageBitmap(bitmap);
//            messageText.setText("Uploading file path:" + imagepath);
//                imagedata = encodeToBase64(facultyImageBitmap);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Image has not been selected", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
    * Capturing Camera Image will lauch camera app requrest image capture
    */
    private void captureImage()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void selectImage() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");

            startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
	 * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        }  else {
            return null;
        }

        return mediaFile;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // FUNCTION TO CONVERT IMAGE INTO BASE64 STRING
    public static String encodeToBase64(Bitmap image) {
        System.gc();
        if (image == null) {
            return null;
        }
        Bitmap image64 = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image64.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageencoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageencoded;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__counselor__register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
