package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class RequestProfileActivity extends Activity {
    ImageView create,get;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestprofile);
        create=(ImageView)findViewById(R.id.crpreq);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RequestProfileActivity.this,RequestActivity.class));
            }
        });
        get=(ImageView)findViewById(R.id.getreq);
        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RequestProfileActivity.this,GetrequestProfile.class));
            }
        });

    }
}
