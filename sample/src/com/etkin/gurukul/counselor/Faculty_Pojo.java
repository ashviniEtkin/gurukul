package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 4/6/2015.
 */
public class Faculty_Pojo {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getFphoto() {
        return fphoto;
    }

    public void setFphoto(String fphoto) {
        this.fphoto = fphoto;
    }

    public String getFpassword() {
        return fpassword;
    }

    public void setFpassword(String fpassword) {
        this.fpassword = fpassword;
    }

    public String getFdateofjoining() {
        return fdateofjoining;
    }

    public void setFdateofjoining(String fdateofjoining) {
        this.fdateofjoining = fdateofjoining;
    }

    public String getFstandardalloted() {
        return fstandardalloted;
    }

    public void setFstandardalloted(String fstandardalloted) {
        this.fstandardalloted = fstandardalloted;
    }

    public String getFqualification() {
        return fqualification;
    }

    public void setFqualification(String fqualification) {
        this.fqualification = fqualification;
    }

    public String getFsubjectalloted() {
        return fsubjectalloted;
    }

    public void setFsubjectalloted(String fsubjectalloted) {
        this.fsubjectalloted = fsubjectalloted;
    }

    public String getFsalary() {
        return fsalary;
    }

    public void setFsalary(String fsalary) {
        this.fsalary = fsalary;
    }

    public String getFemailid() {
        return femailid;
    }

    public void setFemailid(String femailid) {
        this.femailid = femailid;
    }

    public String getFcontactno() {
        return fcontactno;
    }

    public void setFcontactno(String fcontactno) {
        this.fcontactno = fcontactno;
    }

    public String getFaddress() {
        return faddress;
    }

    public void setFaddress(String faddress) {
        this.faddress = faddress;
    }

    public String id;
    public String fname;
    public String fphoto;
    public String fpassword;
    public String fdateofjoining;
    public String fqualification;
    public String fstandardalloted;
    public String fsubjectalloted;
    public String fsalary;
    public String femailid;
    public String fcontactno;
    public String faddress;

}
