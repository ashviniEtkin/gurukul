package com.etkin.gurukul.counselor;

import android.app.Application;
import android.content.Context;

/**
 * Created by Administrator on 20/04/2015.
 */
public class MyApplication extends Application {
    private static MyApplication sInstance;

    public static MyApplication getInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance=this;

    }

}