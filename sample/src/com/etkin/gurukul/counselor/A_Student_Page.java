package com.etkin.gurukul.counselor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class A_Student_Page extends ActionBarActivity {

    Button Standard,Category,Gender,Show,Class;
    public String GENDER,CATEGORY,STANDARD1;
    ArrayList<String> crslist ;
    ProgressDialog pg;
    String crsnm;
    String [] STANDARD;
    ArrayList<String> classlist;
    String  clsnm;
    String gen1,cat1;
    String s_class = "1";
    protected CharSequence[] Gender_Data = {"MALE", "FEMALE","ALL"};
    protected CharSequence[] Category_Data = {"FeesPaid", "Fees Remaining"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__student__page);

        Standard=(Button)findViewById(R.id.standard);
        /*Category=(Button)findViewById(R.id.category);*/
        Gender=(Button)findViewById(R.id.gender);
        Class= (Button) findViewById(R.id.sclass);
        Show=(Button)findViewById(R.id.show);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);



        Standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CoursesList().execute(getString(R.string.AdminIp) + "/GetCourseName");
            }
        });

        Class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ClassAsynk().execute(getString(R.string.AdminIp)+"/getSudentClass", Standard.getText().toString().toString());
            }
        });


        final AlertDialog.Builder ad2 = new AlertDialog.Builder(this);
        ad2.setTitle("Select Category....");
        ad2.setSingleChoiceItems(Category_Data, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                CATEGORY=Category_Data[arg1].toString();
                Category.setText(CATEGORY.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

/*        Category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad2.show();
            }
        });*/

        final AlertDialog.Builder ad3 = new AlertDialog.Builder(this);
        ad3.setTitle("Select Gender....");
        ad3.setSingleChoiceItems(Gender_Data, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                GENDER=Gender_Data[arg1].toString();
                Gender.setText(GENDER.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

        Gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad3.show();
            }
        });


        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String gen=Gender.getText().toString().trim();





                Intent intent=new Intent(getApplicationContext(),Student_List.class);
                intent.putExtra("gen", gen);

                intent.putExtra("course", Standard.getText().toString().trim());
                intent.putExtra("class", Class.getText().toString().trim());
                startActivity(intent);

            }
        });

    }


    public  class CoursesList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONArray jarray = new JSONArray(data);
                    crslist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        crsnm = jrealobj.getString("course");

                        crslist.add(crsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
            pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectCoursesDialog();
            }
        }

    }

    private void showSelectCoursesDialog() {

       STANDARD=new String[crslist.size()];
        STANDARD=crslist.toArray(STANDARD);

        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard....");
        ad1.setSingleChoiceItems(STANDARD, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                STANDARD1=STANDARD[arg1].toString();
                Standard.setText(STANDARD1.toString());
                arg0.dismiss();
            }
        });
        /**   ad2.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

        }
        }); */

       ad1.show();
    }


    public class ClassAsynk extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

            nameValuePairList.add(new BasicNameValuePair("crs", params[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("sclass");
                    classlist = new ArrayList<String>();

                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        clsnm = jrealobj.getString("standard");

                        classlist.add(clsnm);
                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pg.dismiss();
            if (result == false) {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            } else {
//                ArrayAdapter<String> cad = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_single_choice,classlist);
//                ListView clv = (ListView) findViewById(R.id.classlistView);
//                clv.setAdapter(cad);

                showSelectClassDialog();
            }
        }

    }

    protected void showSelectClassDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Class Student want to join");

        builder.setSingleChoiceItems((String[]) classlist.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String[] classarray = (String[]) classlist.toArray(new String[0]);
                s_class = classarray[which].toString();
                Class.setText(s_class.toString());
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }// end of dept dialog



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a__student__page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
