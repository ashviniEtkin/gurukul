package com.etkin.gurukul.counselor;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.faculty.AttendanceNow;
import com.etkin.gurukul.faculty.DbHandler;
import com.etkin.gurukul.faculty.FacultyAttendancePojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SONY on 16/03/2016.
 */
public class C_NoticeAdapter extends RecyclerView.Adapter<C_NoticeAdapter.ViewHolder> {

    SharedPreferences preferences, pref2;
    SharedPreferences.Editor editor;

    public static String  eid;

    public static int not_checked = 0;

    public static List<String> list = new ArrayList<String>();

    private List<AttendanceNow> stList;
    DbHandler dbHandler;
    Context con;
    CheckBox check_box;

    public static String id;

    public C_NoticeAdapter(List<AttendanceNow> students, Context context,CheckBox checkBox_one) {
        this.stList = students;
        dbHandler = new DbHandler(context);
        con = context;
        check_box=checkBox_one;

        checkBox_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                for(int i=0;i<stList.size();i++){
                    stList.get(i).setSelected(b);
                }

                notifyDataSetChanged();
            }
        });


    }

    @Override
    public C_NoticeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.insert_notice_list, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    public List<AttendanceNow> getArrylist(){
        return stList;
    }
    @Override
    public void onBindViewHolder(final C_NoticeAdapter.ViewHolder viewHolder, int position) {

        final int pos = position;

        viewHolder.tvName.setText(stList.get(position).getNAME());
        viewHolder.rollNo.setText(stList.get(position).getROLL());
        viewHolder.tvEmailId.setText(stList.get(position).getMOB_NO());
        viewHolder.emailId.setText(stList.get(position).getSTUD_ID());

        viewHolder.chkSelected.setChecked(stList.get(position).isSelected());

        viewHolder.chkSelected.setTag(pos);

      //  setCheckOne(viewHolder.chkSelected);


      //  setCheckTwo(viewHolder.chkSelected.isChecked());


        viewHolder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // CheckBox cb = (CheckBox) v;
                CheckBox cb = (CheckBox) v;
                int p=(int)v.getTag();
                stList.get(p).setSelected(cb.isChecked());



            }

        });


    }

    public void setCheckOne(CheckBox cb) {
     /*  // CheckBox cb = (CheckBox) v;
        AttendanceNow contact = (AttendanceNow) cb.getTag();
        contact.setSelected(cb.isChecked());*/

        preferences = con.getSharedPreferences("MyPREFERENCE", Context.MODE_PRIVATE);
        int a = preferences.getInt("ChECKBOX_CHECKED", NextInsertActivity.bb);
        if (a == 1 ) {
            cb.setChecked(true);
           list.add(eid);

        } else {
        cb.setChecked(false);
        }

    }


  /* public void setCheckTwo(boolean a){

        if( a){

            list.add(eid);
        }else{

        }

    } */


    @Override
    public int getItemCount() {
        return stList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public TextView tvEmailId, rollNo, emailId;
        public CheckBox chkSelected, select_all;

        public FacultyAttendancePojo singlestudent;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

/*
            select_all=(CheckBox) itemLayoutView.findViewById(R.id.insert_notice_check_all);
*/
            tvName = (TextView) itemLayoutView.findViewById(R.id.insert_notice_tvName);
            rollNo = (TextView) itemLayoutView.findViewById(R.id.insert_notice_rollNo);
            tvEmailId = (TextView) itemLayoutView.findViewById(R.id.insert_notice_tvEmailId);
            chkSelected = (CheckBox) itemLayoutView
                    .findViewById(R.id.insert_notice_chkSelected);

            emailId = (TextView) itemLayoutView.findViewById(R.id.insert_notice_email_id);

        }

    }

    // method to access in activity after updating selection
    public List<AttendanceNow> getStudentist() {
        return stList;
    }

}
