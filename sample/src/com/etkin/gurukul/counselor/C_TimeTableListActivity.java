package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class C_TimeTableListActivity extends Activity {

    RecyclerView recList,res;
    View v;
    String title,date,st1,su1,co1;
    List<C_TimeTableListPojo> result1 = new ArrayList();
    public static Activity activity;

    C_TimeTableListAdapter ca;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.c_time_table_list);

       // b=getIntent().getExtras();
       // st1=b.getString("std");
       // su1=b.getString("sub");
       // co1=b.getString("course");


        SharedPreferences prefs = getSharedPreferences(C_TableListSpinner.class.getSimpleName(),Context.MODE_PRIVATE);

        st1=prefs.getString("STAD","");
        su1=prefs.getString("SUBJ","");
        co1=prefs.getString("CRS","");

        recList = (RecyclerView) findViewById(R.id.cardList);
        activity=this;
        LinearLayoutManager llm = new LinearLayoutManager(C_TimeTableListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        new TimeTable().execute(getString(R.string.AdminIp)+"/getTimeTable",co1,st1,su1);

    }


    public class TimeTable extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("course", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("std", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[3]));

            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);
                    Log.e("msg", data);
                    JSONArray jarray = jobj.getJSONArray("tabledata");


                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        C_TimeTableListPojo ci = new C_TimeTableListPojo();

                        ci.setDate(jrealobj.getString("date"));
                        ci.setCourse(jrealobj.getString("course"));
                        ci.setStd(jrealobj.getString("std"));
                        ci.setSub(jrealobj.getString("sub"));
                        ci.setTime(jrealobj.getString("time"));
                        ci.setType(jrealobj.getString("type"));
                        result1.add(ci);

                    }



                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "Check For Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                // Toast.makeText(getBaseContext(), title+date , Toast.LENGTH_LONG).show();
                ca = new C_TimeTableListAdapter(C_TimeTableListActivity.this,result1);
                recList.setAdapter(ca);

            }
        }

    }


}
