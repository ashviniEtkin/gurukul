package com.etkin.gurukul.counselor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.List;

/**
 * Created by Administrator on 22/04/2015.
 */
public class GetstudAdapter extends RecyclerView.Adapter<GetstudAdapter.ContactViewHolder> {
    private List<GetstudInfo> contactList;
    Context context;
    View itemView;
    public GetstudAdapter(Context context, List<GetstudInfo> contactList) {
        this.contactList = contactList;
        this.context=context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.studcardview, viewGroup, false);




        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {

        // PersonalInfo ci = contactList.get(i);
        //contactViewHolder.vNewsHeading.setText(ci.name);
        //contactViewHolder.vNewsContent.setText(ci.surname);
        contactViewHolder.ttl.setText(contactList.get(i).getReqtitle());
        contactViewHolder.dt.setText(contactList.get(i).getReqdate());
        //  contactViewHolder.nb.setText(contactList.get(i).getReqdes());
        contactViewHolder.layout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
//                // on click action here
//                //-- use context to start the new Activity
                GetstudInfo p=contactList.get(i);
//
//                // Toast.makeText(context, "Clicked on "+i, Toast.LENGTH_LONG).show();
//                String t=contactViewHolder.ttl.getText().toString();
//                String s=contactViewHolder.nb.getText().toString();
                Intent in = new Intent(context,Sub_Stud_Request.class);
                in.putExtra("date",p.getReqdate());
                in.putExtra("title",p.getReqtitle());
                in.putExtra("description",p.getReqdes());
                context.startActivity(in);
           }
        });

        //contactViewHolder.vTitle.setText(ci.name + " " + ci.surname);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;
        protected TextView ttl;
        protected TextView dt;
        //  protected TextView nb;
        protected RelativeLayout layout;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ttl =  (TextView)itemView.findViewById(R.id.suudtit);
            dt = (TextView) itemView.findViewById(R.id.studdate);
            //  nb = (TextView) itemView.findViewById(R.id.nby);
            layout = (RelativeLayout) itemView.findViewById(R.id.studcardview);
        }
    }

}