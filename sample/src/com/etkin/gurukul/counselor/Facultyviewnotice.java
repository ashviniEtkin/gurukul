package com.etkin.gurukul.counselor;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 20/04/2015.
 */
public class Facultyviewnotice extends Activity {
    ArrayList<Facultynoticepojo> mDatasetHome = new ArrayList<Facultynoticepojo>();
    protected RecyclerView mRecyclerView;
    protected facultynoticeAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Context context;
    String Register;
    Intent in;
    String s1;
    public static Activity notice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultyviewnotice);
        notice=this;
        mRecyclerView = (RecyclerView) findViewById(R.id.notice_view);
        //initData();
        mLayoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        savedInstanceState=getIntent().getExtras();
        in=getIntent();
        s1=savedInstanceState.getString("standard");

        new Notice().execute(getString(R.string.AdminIp)+"/FacultyNoticeBoard",s1);
    }

    public class Notice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("standard",params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);
                JSONArray jArray = jobj.getJSONArray("notice");
                for (int i = 0; i < jArray.length() - 1; i++) {
                    JSONObject jrealobj = jArray.getJSONObject(i);
                    Facultynoticepojo company = new Facultynoticepojo();
                    company.setDate(jrealobj.getString("noticedate"));
                    company.setTitle(jrealobj.getString("noticetitle"));
                    company.setDesc(jrealobj.getString("noticedesc"));
                    company.setId(jrealobj.getString("id"));
                    company.setStd(jrealobj.getString("standard"));
                    mDatasetHome.add(company);

                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                Register = "6";
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mAdapter=new facultynoticeAdapter(Facultyviewnotice.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);


        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_facultyviewnotice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

