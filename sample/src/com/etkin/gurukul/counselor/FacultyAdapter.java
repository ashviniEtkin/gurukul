package com.etkin.gurukul.counselor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import network.LruBitmapCache;
import network.VolleySingleton;

/**
 * Created by Administrator on 4/6/2015.
 */
public class FacultyAdapter extends RecyclerView.Adapter<FacultyAdapter.FacultyViewHolder> {
        private ArrayList<Faculty_Pojo> facultyData;
        public static Context con;
        RequestQueue queue;
        ImageLoader imgLoader;
        ProgressDialog pb;
        String Delete;

    public FacultyAdapter(){

        }

        public FacultyAdapter(Context context,ArrayList<Faculty_Pojo> facultyData) {
        //this.cardData = cardData;
        this.facultyData=facultyData;
        con=context;
         pb = new ProgressDialog(con);
        }

    @Override
    public FacultyAdapter.FacultyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(parent.getContext())));
        View itemView = LayoutInflater.
        from(parent.getContext()).
        inflate(R.layout.faculty_list_row, parent, false);
        return new FacultyViewHolder(itemView,parent.getContext());

        }

    @Override
    public void onBindViewHolder(final FacultyViewHolder holder, int i) {
        final Faculty_Pojo faculty_pojo=facultyData.get(i);
        holder.nImageView.setImageUrl(con.getString(R.string.image)+faculty_pojo.getFphoto(),imgLoader);

        //
        // holder.id.setText(counselor_pojo.getId());
        final String id=faculty_pojo.getId();
        holder.name.setText(faculty_pojo.getFname());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        con);

                // set title
                alertDialogBuilder.setTitle("Warning !");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this faculty ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int i) {
                                new DeleteFaculty().execute(con.getString(R.string.AdminIp) + "/DeleteFaculty", id);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                    }
                });

                alertDialogBuilder.show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.context, Sub_Faculty_List.class);
                intent.putExtra("id", faculty_pojo.getId());
                intent.putExtra("photo", faculty_pojo.getFphoto());
                intent.putExtra("name", faculty_pojo.getFname());
                intent.putExtra("date", faculty_pojo.getFdateofjoining());
                intent.putExtra("qualification", faculty_pojo.getFqualification());
                intent.putExtra("standard", faculty_pojo.getFstandardalloted());
                intent.putExtra("subject", faculty_pojo.getFsubjectalloted());
                intent.putExtra("salary", faculty_pojo.getFsalary());
                intent.putExtra("email", faculty_pojo.getFemailid());
                intent.putExtra("password", faculty_pojo.getFpassword());
                intent.putExtra("contact", faculty_pojo.getFcontactno());
                intent.putExtra("address", faculty_pojo.getFaddress());
                holder.context.startActivity(intent);
            }
        });
        holder.itemView.setClickable(true);
    }


    @Override
    public int getItemCount() {
        return facultyData.size();
    }

    public static class FacultyViewHolder extends RecyclerView.ViewHolder {

    // protected ImageView ivImage;

    protected NetworkImageView nImageView;
    protected TextView id,name;
        protected Button delete;
    private Context context;

    public FacultyViewHolder(View itemView,final Context context) {
        super(itemView);
        this.context=context;
        nImageView= (NetworkImageView) itemView.findViewById(R.id.photo);
        name=(TextView)itemView.findViewById(R.id.name);
        delete=(Button)itemView.findViewById(R.id.delete);

    }
  }

    public class DeleteFaculty extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                Delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (Delete.equals("6")){
                Toast.makeText(con, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (Delete.equals("1")) {

                Toast.makeText(con, "Faculty deleted successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(con, Faculty_List.class);
                con.startActivity(intent1);
                Faculty_List.faculty_list.finish();
            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting faculty....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }
}
