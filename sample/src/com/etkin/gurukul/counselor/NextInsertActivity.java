package com.etkin.gurukul.counselor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;
import com.etkin.gurukul.faculty.AttendanceNow;

import com.etkin.gurukul.faculty.DbHandler;
import com.etkin.gurukul.faculty.AttendanceNow;
import com.etkin.gurukul.faculty.QrActivity;
import com.etkin.gurukul.gurukul_notify.Notification;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class NextInsertActivity extends ActionBarActivity {

    Button btn1, btn2;

    ArrayList<String> arr, select_all;
    public static int bb = 0;
    String Type;
    ArrayList<String> sel;
    String date;
    CheckBox check_all;
    TextView total;
    RecyclerView lv_stud_list;
    Context context;
    ProgressDialog pb;
    SharedPreferences preferences, pref1, pref3;
    String chk = "";
    String data;
    AttendanceNow model;
    SharedPreferences.Editor editor;
    public static int a;
    private List<AttendanceNow> studentList;
    ArrayList<AttendanceNow> attend = new ArrayList<AttendanceNow>();
    // private RecyclerView.Adapter mAdapter;

    public C_NoticeAdapter mAdapter;

  public static  String selectItem1, selectItem2, selectItem3, selectItem, tittle, desc;
    int result1, not_checked;
    int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_insert);
        pb = new ProgressDialog(NextInsertActivity.this);
        btn1 = (Button) findViewById(R.id.submit);
        btn2 = (Button) findViewById(R.id.cancel);
        check_all = (CheckBox) findViewById(R.id.insert_notice_check_all);

        lv_stud_list = (RecyclerView) findViewById(R.id.stud_recycler_view);
        total = (TextView) findViewById(R.id.total_stud);
        lv_stud_list.setLayoutManager(new LinearLayoutManager(this));
        savedInstanceState = getIntent().getExtras();

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        date = String.format("%d-%d-%d", day, month + 1, year);


        arr = new ArrayList<String>();
        select_all = new ArrayList<String>();

      /*  preferences = getSharedPreferences(InsertNotice.MyPREFERENCE, Context.MODE_PRIVATE);
        result1 = preferences.getInt("CHECK_ALL", InsertNotice.aa);*/

        pref3 = getSharedPreferences("NOTCHECKED", Context.MODE_PRIVATE);
        not_checked = pref3.getInt("NOTCHECKED", C_NoticeAdapter.not_checked);

        sel = new ArrayList<String>();

        selectItem = savedInstanceState.getString("course");
        selectItem1 = savedInstanceState.getString("std");
        selectItem2 = savedInstanceState.getString("sub");
        selectItem3 = savedInstanceState.getString("time");
        tittle = savedInstanceState.getString("tittle");
        desc = savedInstanceState.getString("desc");

        context = this;


        String url1 = getResources().getString(R.string.studListURL);

        DbHandler handler = new DbHandler(context);
        new ListAsynTask().execute(url1, selectItem, selectItem1, selectItem2, selectItem3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitDialog();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getApplicationContext(), Facultyviewnotice.class);
                in.putExtra("standard",selectItem1 );
                startActivity(in);

            }
        });


    }

    public void submitDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NextInsertActivity.this);

        // set title
        alertDialogBuilder.setTitle("Warning !");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you really want to send Notice ?")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {

                        //LiseC_NoticeAdapter.list;


                        List<AttendanceNow> list_one = mAdapter.getArrylist();
                        for (int m = 0; m < list_one.size(); m++) {
                            list_one.get(m).isSelected();
                            if (list_one.get(m).isSelected()) {
                                sel.add(list_one.get(m).STUD_ID);
                                // Toast.makeText(NextInsertActivity.this,sel.get(m)+"",Toast.LENGTH_SHORT).show();
                            }

                        }

                        for (int g = 0; g < sel.size(); g++) {
                            Toast.makeText(NextInsertActivity.this, sel.get(g) + "", Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(getApplicationContext(),desc,Toast.LENGTH_LONG).toString();
                        Type = "Student_Inbox";
                        new Feedback().execute(getString(R.string.AdminIp) + "/Insertnotice", date, tittle, Type, desc);


                    }


                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

    class ListAsynTask extends AsyncTask<String, Void, Boolean> {
        DbHandler db = new DbHandler(context);
        String curDate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... arg0) {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("course", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("std", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("time", arg0[4]));

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    if (data.equals("0")) {
                        chk = data;
                    } else {
                        JSONObject jobj = new JSONObject(data);

                        JSONArray jarray = jobj.getJSONArray("studentdata");
                        studentList = new ArrayList<AttendanceNow>();

                        for (int i = 0; i < jarray.length() - 1; i++) {
                            JSONObject jrealobj = jarray.getJSONObject(i);
                            String name = jrealobj.getString("name");
                            String id = jrealobj.getString("id");

                            model = new AttendanceNow();
                            model.setNAME(jrealobj.getString("name"));
                            model.setSTUD_ID(jrealobj.getString("id"));
                            model.setROLL(jrealobj.getString("rollNo"));
                            model.setMOB_NO(jrealobj.getString("mob"));
                            curDate = jrealobj.getString("date");
                            //     db.addStudList(model, selectItem1, curDate, selectItem, selectItem2, selectItem3);
                            studentList.add(model);
                           /* arr.add(id);
                            //  Toast.makeText(getApplicationContext(),"Id of "+name+" is "+id,Toast.LENGTH_LONG).show();
                            Log.d("Id of " + name + " is ", id);*/

                            select_all.add(id);


                        }


                    }

                    Log.e("msg", data);
                    //adapter = new CustomAdapter(getApplicationContext(),list);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result) {

            //   db.deleteAttendance(curDate);
            dismissProgressDialog();
            mAdapter = new C_NoticeAdapter(studentList, getApplicationContext(), check_all);
            a = studentList.size();
            total.setText("Total : " + (a + ""));
            lv_stud_list.setAdapter(mAdapter);


            if (a > 0) {
                check_all.setVisibility(View.VISIBLE);
            }

        }

    }

    public class Feedback extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

                for(int k=0;k<sel.size();k++){
                    nameValuePairList.add(new BasicNameValuePair("email_id", sel.get(k)));
                }

                nameValuePairList.add(new BasicNameValuePair("notice_date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("notice_title", params[2]));
                nameValuePairList.add(new BasicNameValuePair("status", params[3]));
                nameValuePairList.add(new BasicNameValuePair("notice_desc", params[4]));


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                JSONArray jsonArray = new JSONArray(data);
                Log.d("jsonArray",jsonArray.toString());

                List<String> gcmids = new ArrayList<String>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    gcmids.add(jsonArray.getString(i));
                }

                Notification.sendNotification(gcmids, params[4], params[3], getApplicationContext());
                data = "1";
                //  Register1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                data = "6";

            } catch (UnknownHostException e) {
                data = "6";
            } catch (IOException e) {
                e.printStackTrace();
                data = "6";
            } catch (Exception e) {
                e.printStackTrace();
                data = "6";
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();


           /* if (data.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }*/
            if (data.equals("6")) {
                Toast.makeText(getApplicationContext(), "Notice send...", Toast.LENGTH_LONG).show();
            } else if (data.equals("1")) {


                Toast.makeText(getApplicationContext(), "Notice send...", Toast.LENGTH_LONG).show();
                finish();


            } else if (data.equals("2")) {
                Toast.makeText(getApplication(), "You have already send....", Toast.LENGTH_LONG).show();
            }


        }
    }


    public class StudentList extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

                nameValuePairList.add(new BasicNameValuePair("gender", params[1]));
                nameValuePairList.add(new BasicNameValuePair("course", params[2]));
                nameValuePairList.add(new BasicNameValuePair("sclass", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                // JSONObject jobj = new JSONObject(data);
                // JSONArray jArray = jobj.getJSONArray("information");
                JSONArray jsonArray = new JSONArray(data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    AttendanceNow fap = new AttendanceNow();
                    String id = jrealobj.getString("email");
                    String name = jrealobj.getString("sname");
                    fap.setSTUD_ID(jrealobj.getString("email"));
                    fap.setNAME(jrealobj.getString("sname"));
                    fap.setMOB_NO(jrealobj.getString("contact"));
                    fap.setROLL(jrealobj.getString("rollNo"));
                    //  fap.setS;

                    attend.add(fap);
                    arr.add(id);
                    //  Toast.makeText(getApplicationContext(),"Id of "+name+" is "+id,Toast.LENGTH_LONG).show();
                    Log.d("Id of " + name + " is ", id);

                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            //mAdapter = new C_NoticeAdapter(attend, getApplicationContext(),check_all);

            a = attend.size();
            total.setText("Total : " + (a + ""));
            // lv_stud_list.setAdapter(mAdapter);
            lv_stud_list.setVisibility(View.GONE);


            if (a > 0) {
                check_all.setVisibility(View.GONE);
            }


        }
    }

    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }

}
