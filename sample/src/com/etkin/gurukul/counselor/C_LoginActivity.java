package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 21/04/2015.
 */
public class C_LoginActivity extends Activity {
    ArrayList std=new ArrayList();
    ArrayList sub=new ArrayList();
    String data;
    EditText user, pass;
    String selectItem1,selectItem2;
    ProgressDialog pg;
    Button login,cancel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.counselorlogin);
        login= (Button) findViewById(R.id.loginbutton);
        cancel=(Button)findViewById(R.id.canlogin);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Log In....");
        pg.setCancelable(false);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               loginfunction();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void loginfunction()
    {
        user = (EditText) findViewById(R.id.cid);
        pass = (EditText) findViewById(R.id.cpass);
        String counsid=user.getText().toString();
        String counpass=pass.getText().toString();
        //Toast.makeText(getBaseContext(),""+u+p+selectItem1+selectItem2, Toast.LENGTH_LONG).show();
        // String url = getResources().getString(R.string.loginURL);
        new Counselorlogin().execute(getString(R.string.AdminIp)+"/Councellor_Login",counsid,counpass);
    }


    public class Counselorlogin extends AsyncTask<String, Void, String>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("Cemailid", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("Cpassword", arg0[2]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                String s="a";


            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
                data="2";
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
                data="2";
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                data="2";
            } catch (IOException e1) {
                e1.printStackTrace();
                data="2";
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            pg.dismiss();
            super.onPostExecute(result);

            if (data.equals("1")) {
                //Toast.makeText(getBaseContext(), "login successfully...", Toast.LENGTH_LONG).show();
                String u=user.getText().toString();
                SharedPreferences sharedPreferences = getSharedPreferences(C_LoginActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("name",u);
                editor.putString("login1", "true");
                editor.commit();

                Intent in=new Intent(getApplication(),C_MainActivity.class);
                startActivity(in);
                finish();


            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Login Failed", Toast.LENGTH_LONG).show();
            }


        }
    }
}
