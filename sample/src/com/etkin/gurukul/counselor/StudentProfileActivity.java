package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class StudentProfileActivity extends Activity {
    ImageView studp,stl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.studentprofile);

        studp=(ImageView)findViewById(R.id.sregister);
        studp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startActivity(new Intent(StudentProfileActivity.this,C_studentReg.class));
            }
        });

        stl=(ImageView)findViewById(R.id.studlist);
        stl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(StudentProfileActivity.this,A_Student_Page.class));
            }
        });

    }
}
