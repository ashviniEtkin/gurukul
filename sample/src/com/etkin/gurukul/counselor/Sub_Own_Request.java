package com.etkin.gurukul.counselor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 4/23/2015.
 */
public class Sub_Own_Request extends ActionBarActivity {

    TextView date,title,description;
    String Date,Title,Description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_faculty_request);
        date=(TextView)findViewById(R.id.Date);
        title=(TextView)findViewById(R.id.Title);
        description=(TextView)findViewById(R.id.Description);

        Intent intent=getIntent();

        Date=intent.getStringExtra("date");
        Title=intent.getStringExtra("title");
        Description=intent.getStringExtra("description");

        date.setText(Date);
        title.setText(Title);
        description.setText(Description);
    }
}
