package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etkin.gurukul.R;

import java.util.List;

/**
 * Created by Administrator on 4/6/2015.
 */
class FacultydetailAdapter extends RecyclerView.Adapter<com.etkin.gurukul.counselor.FacultydetailAdapter.ContactViewHolder> {
    private List<Faculltydetailpojo> contactList;
    Context context;
    View itemView;
    RequestQueue queue;

    ImageLoader imgLoader;

    public FacultydetailAdapter(Context context, List<Faculltydetailpojo> contactList) {
        this.contactList = contactList;
        this.context = context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        try {
            queue = VolleySingleton.getInstance().getRequestQueue();
            imgLoader = new ImageLoader(queue, new LruBitmapCache(LruBitmapCache.getCacheSize(viewGroup.getContext())));

            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.facultycardview, viewGroup, false);
        }catch (Exception e){
            e.printStackTrace();
        }

        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {
        final Faculltydetailpojo facultyInfo = contactList.get(i);
        contactViewHolder.nImageView.setImageUrl(context.getString(R.string.image) + facultyInfo.getPhoto(), imgLoader);
        final String facid = facultyInfo.getFacid();

        // FacultyInfo ci = contactList.get(i);
        // contactViewHolder.facname.setText(ci.name);
        //  contactViewHolder.facid.setText(ci.facid);
        contactViewHolder.facid.setText(contactList.get(i).getFacid());
        contactViewHolder.facname.setText(contactList.get(i).getName());
        //contactViewHolder.p.setText(contactList.get(i).getPhoto());
//     contactViewHolder.layout.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // on click action here
//              //-- use context to start the new Activity
//               FacultyInfo p = contactList.get(i);
//
////                // Toast.makeText(context, "Clicked on "+i, Toast.LENGTH_LONG).show();
//              String t = contactViewHolder.facid.getText().toString();
//               String s = contactViewHolder.facname.getText().toString();
//          // String p = contactViewHolder.photo.getp().toString();
//                Intent in = new Intent(context,FacultyDetails.class);
//               in.putExtra("facid", t);
//                in.putExtra("facname", s);
//               //in.putExtra("photo",p);
//               context.startActivity(in);
//          }
//       });

        //contactViewHolder.vTitle.setText(ci.name + " " + ci.surname);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;
        protected NetworkImageView nImageView;
        protected TextView facid;
        protected TextView facname;
        //  protected ImageView facphoto
        protected RelativeLayout layout;

        public ContactViewHolder(View itemView) {
            super(itemView);
            nImageView = (NetworkImageView) itemView.findViewById(R.id.iv_cardview);
            facid = (TextView) itemView.findViewById(R.id.tv_newstitle);
            facname = (TextView) itemView.findViewById(R.id.tv_news);

            layout = (RelativeLayout) itemView.findViewById(R.id.faccardview);
        }
    }
}



