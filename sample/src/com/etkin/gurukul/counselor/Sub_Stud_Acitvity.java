package com.etkin.gurukul.counselor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;



import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import network.LruBitmapCache;

/**
 * Created by Administrator on 4/22/2015.
 */
public class Sub_Stud_Acitvity extends ActionBarActivity {

    EditText Name,Email,Contact,Address,Class1,Fees;
    String name,email,contact,address,class1,fees,id,Image,totalFees;
    NetworkImageView nImageView;
    RequestQueue queue;
    ImageLoader imgLoader;
    Button Delete,Update;
    String delete,update;
    ProgressDialog pb,pb1;
    private static ActionBarActivity sub_student_list;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_student_activity);
        context=this;
        sub_student_list=this;
        pb=new ProgressDialog(Sub_Stud_Acitvity.this);
        pb1=new ProgressDialog(Sub_Stud_Acitvity.this);
        Name=(EditText)findViewById(R.id.Name);
        Email=(EditText)findViewById(R.id.Email);
        Class1=(EditText)findViewById(R.id.Standard);
        Fees=(EditText)findViewById(R.id.Fees);
        Contact=(EditText)findViewById(R.id.Contact);
        Address=(EditText)findViewById(R.id.Address);
        nImageView=(NetworkImageView)findViewById(R.id.photo);
        Delete=(Button)findViewById(R.id.delete);
        Update=(Button)findViewById(R.id.update);
        queue= network.VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(getApplicationContext())));
        sub_student_list=this;

        Intent intent=getIntent();
        id=intent.getStringExtra("studid");
        name=intent.getStringExtra("sname");
        Image=intent.getStringExtra("photo");
        email=intent.getStringExtra("email");
        class1=intent.getStringExtra("sclass");
        fees=intent.getStringExtra("feesPaid");
        contact=intent.getStringExtra("contact");
        address=intent.getStringExtra("address");
        totalFees=intent.getStringExtra("totalFees");

        nImageView.setImageUrl(getString(R.string.image)+Image,imgLoader);
        Name.setText(name);
        Email.setText(email);
        Address.setText(address);
        Contact.setText(contact);
        Class1.setText(class1);
        Fees.setText(fees);

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Warning");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this student ?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {
                                new DeleteStudent().execute(getString(R.string.AdminIp) + "/DeleteStudent", id);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();
            }
        });


        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name1,classes1,fees1,contact1,address1,email1;
                name1=Name.getText().toString().trim();
                classes1=Class1.getText().toString().trim();
                fees1=Fees.getText().toString().trim();
                contact1=Contact.getText().toString().trim();
                address1=Address.getText().toString().trim();
                email1=Email.getText().toString().trim();


                new UpdateStudent().execute(getString(R.string.AdminIp) + "/UpdateStudent", name1, classes1, fees1, contact1, address1,email1, id);

            }
        });


    }

    public class DeleteStudent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                delete = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (delete.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (delete.equals("1")) {

                Toast.makeText(getApplicationContext(), "Stduent deleted successfully...", Toast.LENGTH_LONG).show();
                Student_List.context1.finish();
                Sub_Stud_Acitvity.sub_student_list.finish();
                Intent intent1 = new Intent(Sub_Stud_Acitvity.this,A_Student_Page.class);
                startActivity(intent1);

            }
        }
    }

    public class UpdateStudent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog1();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("name", params[1]));
                nameValuePairList.add(new BasicNameValuePair("classes", params[2]));
                nameValuePairList.add(new BasicNameValuePair("feesPaid", params[3]));
                nameValuePairList.add(new BasicNameValuePair("contactno", params[4]));
                nameValuePairList.add(new BasicNameValuePair("address", params[5]));
                nameValuePairList.add(new BasicNameValuePair("email", params[6]));
                nameValuePairList.add(new BasicNameValuePair("id", params[7]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                String abc = "0";
                update = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                delete = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog1();

            if (update.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (update.equals("1")) {

                Toast.makeText(getApplicationContext(), "Student updated successfully...", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Sub_Stud_Acitvity.this, A_Student_Page.class);
                startActivity(intent1);
                Student_List.context1.finish();
                Sub_Stud_Acitvity.sub_student_list.finish();
            }
        }
    }

    public void showProgressDialog() {

        pb.setMessage("Deleting student....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }
    public void showProgressDialog1() {

        pb1.setMessage("Updating student....");
        pb1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb1.setIndeterminate(true);
        pb1.show();
    }


    public void dismissProgressDialog1() {

        pb1.dismiss();
    }

}
