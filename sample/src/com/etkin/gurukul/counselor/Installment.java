package com.etkin.gurukul.counselor;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Installment extends ActionBarActivity {

    Button search,studName,totalFees,feesPaid,Update;
    EditText emailid,newAmount;
    String id,Contact;
    String selected_doins = "1";
    TextView sdoinsbtn,t3,t4,t5,t6,t1;
    ProgressDialog pb;
    Context context;
    String update;
    String Date;
    int remain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installment);
        emailid=(EditText)findViewById(R.id.Name);
        search=(Button)findViewById(R.id.search);
        context=this;
        pb=new ProgressDialog(context);
        t1=(TextView)findViewById(R.id.t1);
        t3=(TextView)findViewById(R.id.t3);
        t4=(TextView)findViewById(R.id.t4);
        t5=(TextView)findViewById(R.id.t5);
        t6=(TextView)findViewById(R.id.t6);
        studName=(Button)findViewById(R.id.StudName);
        totalFees=(Button)findViewById(R.id.TotalFees);
        feesPaid=(Button)findViewById(R.id.FeesPaid);
        Update=(Button)findViewById(R.id.update);
        newAmount=(EditText)findViewById(R.id.NewAmount);
        sdoinsbtn = (TextView) findViewById(R.id.next);

        t3.setVisibility(View.INVISIBLE);
        t4.setVisibility(View.INVISIBLE);
        t5.setVisibility(View.INVISIBLE);
        t6.setVisibility(View.INVISIBLE);
        studName.setVisibility(View.INVISIBLE);
        totalFees.setVisibility(View.INVISIBLE);
        feesPaid.setVisibility(View.INVISIBLE);
        newAmount.setVisibility(View.INVISIBLE);
        sdoinsbtn.setVisibility(View.INVISIBLE);
        Update.setVisibility(View.INVISIBLE);






            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id=emailid.getText().toString().trim();
                    if (id.equals("")){
                        Toast.makeText(getApplicationContext(),"Please enter email id !",Toast.LENGTH_SHORT).show();
                    }else {
                        new InstallmentData().execute(getString(R.string.AdminIp)+"/Installment",id);
                    }
                    }
            });


        final DatePickerDialog doiDlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                selected_doins = strDate.toString();
                sdoinsbtn.setText(selected_doins);
            }
        }, 2015, 0, 1);



        sdoinsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doiDlg.show();
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String total=totalFees.getText().toString().trim();
                String Fees=newAmount.getText().toString().trim();
                String FeesPaid=feesPaid.getText().toString().trim();

                int Fees1=Integer.parseInt(Fees);
                int FeesPaid2=Integer.parseInt(FeesPaid);
                int nowFees=Fees1+FeesPaid2;
                int total1=Integer.parseInt(total);





                if (total1==nowFees) {
                Date=" ";
                }
                else if (nowFees<total1)
                {
                    remain=total1-nowFees;
                    Date=sdoinsbtn.getText().toString().trim();
                }


                if (total.equals(FeesPaid))
                {
                    Toast.makeText(getApplicationContext(),"Your fees is already completed !",Toast.LENGTH_SHORT).show();
                }else
                if (nowFees>total1)
                {
                    Toast.makeText(getApplicationContext(),"New amount should not be more than remaining amount !",Toast.LENGTH_SHORT).show();

                }
                else {
                    String nowString1=nowFees+"";
                    String remain1=remain+"";
                    new InstallmentUpdate().execute(getString(R.string.AdminIp)+"/InstallmentUpdate",id,nowString1,Date,remain1,Contact,Fees);

                }

            }
        });
    }

    public class InstallmentData extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id",params[1]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                //JSONObject jobj = new JSONObject(data);
                //  JSONArray jArray = jobj.getJSONArray("Information");
                JSONArray jsonArray=new JSONArray(data);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    studName.setText(jrealobj.getString("name"));
                    totalFees.setText(jrealobj.getString("totalFees"));
                    feesPaid.setText(jrealobj.getString("feesRemain"));
                    Contact=jrealobj.getString("contact");
                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            t1.setVisibility(View.INVISIBLE);
            emailid.setVisibility(View.INVISIBLE);
            search.setVisibility(View.INVISIBLE);

            t3.setVisibility(View.VISIBLE);
            t4.setVisibility(View.VISIBLE);
            t5.setVisibility(View.VISIBLE);
            t6.setVisibility(View.VISIBLE);
            studName.setVisibility(View.VISIBLE);
            totalFees.setVisibility(View.VISIBLE);
            feesPaid.setVisibility(View.VISIBLE);
            newAmount.setVisibility(View.VISIBLE);
            sdoinsbtn.setVisibility(View.VISIBLE);
            Update.setVisibility(View.VISIBLE);

        }
    }


    public class InstallmentUpdate extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id",params[1]));
                nameValuePairList.add(new BasicNameValuePair("fees",params[2]));
                nameValuePairList.add(new BasicNameValuePair("date",params[3]));
                nameValuePairList.add(new BasicNameValuePair("remain",params[4]));
                nameValuePairList.add(new BasicNameValuePair("contact",params[5]));
                nameValuePairList.add(new BasicNameValuePair("newamount",params[6]));

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                //  post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                update=data;


            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            if (update.equals("1"))
            {
                Toast.makeText(getApplicationContext(),"Updated successfully !",Toast.LENGTH_SHORT).show();
                finish();
            }

        }
    }

    public void showProgressDialog() {
        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }

    public void dismissProgressDialog() {

        pb.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_installment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
