package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.List;


/**
 * Created by Administrator on 17/04/2015.
 */
public class ReportAdapter  extends RecyclerView.Adapter<ReportAdapter.ContactViewHolder> {
    private List<ReportInfo> contactList;
    Context context;
    View itemView;

    public ReportAdapter(Context context, List<ReportInfo> contactList) {
        this.contactList = contactList;
        this.context = context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.reportcardview, viewGroup, false);


        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {

        // PersonalInfo ci = contactList.get(i);
        //contactViewHolder.vNewsHeading.setText(ci.name);
        //contactViewHolder.vNewsContent.setText(ci.surname);
        contactViewHolder.ttl.setText(contactList.get(i).getTitle());
        contactViewHolder.dt.setText(contactList.get(i).getDate());
        contactViewHolder.repid.setText(contactList.get(i).getReportid());
        contactViewHolder.layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // on click action here
                //-- use context to start the new Activity
                ReportInfo p = contactList.get(i);

                // Toast.makeText(context, "Clicked on "+i, Toast.LENGTH_LONG).show();
                //  String t = contactViewHolder.ttl.getText().toString();
                String s = contactViewHolder.repid.getText().toString();
                Intent in = new Intent(context,ReportDetail.class);
                // in.putExtra("title", t);
                in.putExtra("reportid", s);
                context.startActivity(in);
            }
        });

        //contactViewHolder.vTitle.setText(ci.name + " " + ci.surname);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        // protected ImageView ivImage;
        protected TextView ttl;
        protected TextView dt;
        protected TextView repid;
        // protected TextView nb;
        protected RelativeLayout layout;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ttl = (TextView) itemView.findViewById(R.id.reporttitle);
            dt = (TextView) itemView.findViewById(R.id.reportdt);
            repid = (TextView) itemView.findViewById(R.id.rid);
            // nb = (TextView) itemView.findViewById(R.id.nby);
            layout = (RelativeLayout) itemView.findViewById(R.id.reportcardviewone);
        }
    }
}


