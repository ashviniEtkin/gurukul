package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class C_FinalTimeTableActivity extends Activity {

    ListView lv;
    Button btn1;
    int i;
    View v1;
    String dt,s,ti;
    ArrayList<C_FinalTimeTablePojo> list;
    C_FinalTimeTableAdapter mAdapter;
    C_FinalTimeTablePojo model;
    ArrayList<String> course ;
    ArrayList std;
    ArrayList sub;
    String []type=new String[]{"Divisional Test","Final Test"};
    String crsnm,su1,dt1,ti1,sy1;
    String selectItem1,selectItem2,st,cors,ty,selectItem3,msg,data;
    ArrayList<Integer> mSelectedItems;
    Button btn,btn_course,btn_std,btn_type;
    EditText etmsg;
    String []COURSE1;
    String []STD1;
    String [] testtype1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_final_timetable);

        lv = (ListView)findViewById(R.id.lv);
        etmsg=(EditText)findViewById(R.id.tmsg);

        btn=(Button)findViewById(R.id.createFun);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (i = 0; i < lv.getCount(); i++) {
                    v1 = lv.getChildAt(i);

                    etmsg=(EditText)findViewById(R.id.tmsg);
                    TextView sub = (TextView) v1.findViewById(R.id.tx_sub);
                    TextView date= (TextView) v1.findViewById(R.id.tx_date);
                    EditText time=(EditText) v1.findViewById(R.id.et_time);
                    EditText syllabus=(EditText) v1.findViewById(R.id.et_syllabus);

                    su1 = sub.getText().toString();
                    dt1=date.getText().toString();
                    ti1=time.getText().toString();
                    sy1=syllabus.getText().toString();
                    msg=etmsg.getText().toString();
                    new CreateAsyn().execute(getString(R.string.AdminIp)+"/createTimeTable",selectItem2,su1,selectItem3,ti1,dt1,selectItem1,sy1,msg);

                }}



        });

        btn_course=(Button)findViewById(R.id.tcoursename);
        btn_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CoursesAsyn().execute(getString(R.string.AdminIp)+"/getCoursename");
            }
        });

        btn_std=(Button)findViewById(R.id.tclass);
        btn_std.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new StandardAsyn().execute(getString(R.string.AdminIp)+"/getSudentClass",selectItem1);


            }
        });

        btn_type=(Button)findViewById(R.id.ttype);
        btn_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSelectTypeDialog();
            }
        });
    }

    public void showDateDialog(View v)
    {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub

                selectedmonth = selectedmonth + 1;
                // date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();
    }


    private void showSelectTypeDialog() {
        //COURSE1=new String[course.size()];


        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Test Type...");
        ad1.setSingleChoiceItems(type,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                ty=type[array];

                btn_type.setText(ty);
                selectItem3=btn_type.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();

    }

    public class SubjectData extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("crs", arg0[2]));

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                Log.e("m", "err1");
                if (status == 200) {
                    Log.e("m", "e0");
                    HttpEntity entity = response.getEntity();
                    Log.e("m", "er0");
                    String data = EntityUtils.toString(entity);
                    Log.e("m", "err0");
                    JSONObject jobj = new JSONObject(data);
                    Log.e("m", "er");
                    JSONArray jarray = jobj.getJSONArray("studsubjects");
                    Log.e("m", "err");
                    list = new ArrayList<C_FinalTimeTablePojo>();
                    Log.e("m", "err2");
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        Log.e("m", "err3");
                        model = new C_FinalTimeTablePojo();

                        model.setSub(jrealobj.getString("studsub"));

                        list.add(model);
                        Log.e("m", "err4");
                        //String name = (model.getSub().toString());


                    }
                    // mAdapter = new TestReportAdapter(getApplicationContext(),R.layout.f_testreport_row,list);

                    Log.e("msg",data);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                mAdapter = new C_FinalTimeTableAdapter(getApplicationContext(),R.layout.c_final_timetable_row,list);

                lv.setAdapter(mAdapter);
            }
        }

    }



    public  class CoursesAsyn extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... params)
        {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post  = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("courses");
                    course = new ArrayList<String>();
                    for (int i = 0; i < jarray.length()-1; i++)
                    {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        crsnm = jrealobj.getString("crs");

                        course.add(crsnm);
                    }
                    return true;
                }
            }
            catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result)
        {
           // pg.dismiss();
            if(result==false)
            {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
            else
            {
                showSelectCoursesDialog();
            }
        }

    }

    public class StandardAsyn extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("crs", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray1 = jobj.getJSONArray("sclass");
                    std=new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray1.length() - 1; i++) {
                        JSONObject jrealobj = jarray1.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        std.add(jrealobj.getString("standard"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {

                showSelectStdDialog();

            }

        }

    }

    private void showSelectCoursesDialog() {
        //COURSE1=new String[course.size()];

        COURSE1=(String[])course.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course...");
        ad1.setSingleChoiceItems(COURSE1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                cors=COURSE1[array];

                btn_course.setText(cors);
                selectItem1=btn_course.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        course.clear();

    }


    private void showSelectStdDialog() {
        //COURSE1=new String[course.size()];
        STD1=(String[])std.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Standard...");
        ad1.setSingleChoiceItems(STD1,-1,new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                st=STD1[array];

                btn_std.setText(st);
                selectItem2=btn_std.getText().toString();

                dialog.dismiss();

            }
        });
        ad1.show();
        std.clear();
        //Toast.makeText(this,selectItem2+selectItem1,Toast.LENGTH_LONG).show();
        new SubjectData().execute(getString(R.string.AdminIp)+"/getStudentSubject",selectItem2,selectItem1);
        //sub.clear();
        //time.clear();
    }


    public class CreateAsyn extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... arg0)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            nameValuePairList.add(new BasicNameValuePair("testtype", arg0[3]));
            nameValuePairList.add(new BasicNameValuePair("testtime", arg0[4]));
            nameValuePairList.add(new BasicNameValuePair("testdate", arg0[5]));
            nameValuePairList.add(new BasicNameValuePair("course", arg0[6]));
            nameValuePairList.add(new BasicNameValuePair("syllabus", arg0[7]));
            nameValuePairList.add(new BasicNameValuePair("msg", arg0[8]));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                Log.e("msg",data);

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //if(data.equals("1")) {
            //return true;
            //}
            //return false;
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (data.equals("1")) {
                Toast.makeText(getBaseContext(), "Successfully Done", Toast.LENGTH_LONG).show();

                //Intent in=new Intent(getApplication(),AttendanceActivity.class);
                //startActivity(in);

            } else {

                //Log.d("message: ", "> No network");
                Toast.makeText(getBaseContext(), "Oopzs..Sorry Not Done!!!", Toast.LENGTH_LONG).show();
            }


        }
    }
}
