package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 22/04/2015.
 */
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 21/04/2015.
 */
public class GetRequestFacRecyclerActivity extends Activity {
    RecyclerView recList,res;
    CardView cardView;
    View v;
    String title,date;
    List<GetfacultyrepInfo> result = new ArrayList();

    GetFacultyreportAdapter ca;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getfacultyrecyclerview);

        recList = (RecyclerView) findViewById(R.id.getfac);

        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(GetRequestFacRecyclerActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        new RequestList().execute(getString(R.string.AdminIp)+"/getAllFacultyRequests");

    }


    public class RequestList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("AllFacultyRequests");



                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        // date=jrealobj.getString("date").toString();
                        //title=jrealobj.getString("title").toString();
                        GetfacultyrepInfo ci = new GetfacultyrepInfo();


                        ci.setReqdate(jrealobj.getString("r_date"));
                        ci.setReqtitle(jrealobj.getString("r_title"));
                        ci.setReqdes(jrealobj.getString("r_desc"));

                        result.add(ci);
                        Log.e("msg", ci.getReqtitle());
                        Log.e("msg",ci.getReqdate());
                        //Log.e("msg",ci.getReqdes());

                    }


                    Log.e("msg",data);
                    ca = new GetFacultyreportAdapter(GetRequestFacRecyclerActivity.this,result);


                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {


            // Toast.makeText(getBaseContext(), title+date , Toast.LENGTH_LONG).show();
            recList.setAdapter(ca);

        }
    }

}

