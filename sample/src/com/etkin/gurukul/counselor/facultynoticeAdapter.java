package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/15/2015.
 */
public class facultynoticeAdapter  extends RecyclerView.Adapter<facultynoticeAdapter.InboxViewHolder> {
    private ArrayList<Facultynoticepojo> myDatas;
    Context context;
    ProgressDialog pb;
    String Delete1;
    public void setDataAdaptor(ArrayList<Facultynoticepojo> mDatasetHome) {
        myDatas=mDatasetHome;

    }

    public facultynoticeAdapter(Context context,ArrayList<Facultynoticepojo> myDatas)
    {
        this.myDatas=myDatas;
        this.context=context;
        pb = new ProgressDialog(context);

    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.facultynotice_row, parent, false);


        return new InboxViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        final Facultynoticepojo myData=myDatas.get(position);
        holder.date.setText(myData.getDate());
        holder.noticetype.setText(myData.getTitle());
        holder.noticedesc.setText(myData.getDesc());
        holder.std.setText(myData.getStd());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteNotice().execute(context.getString(R.string.AdminIp)+"/DeleteNotice1",myData.getId(),"counselor");

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            Facultynoticepojo data=myDatas.get(position);

            public void onClick(View view) {
                //  stud.overridePendingTransition(R.anim.page_in_left, R.anim.page_out_left);
                Intent intent = new Intent(holder.context1, Facultynotice_sub.class);
                intent.putExtra("noticedate", data.getDate());
                intent.putExtra("noticetitle", data.getTitle());
                intent.putExtra("noticedesc", data.getDesc());
                holder.context1.startActivity(intent);


            }


        });

    }

    @Override
    public int getItemCount() {
        return myDatas.size();
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder{
        protected TextView date,noticetype,noticedesc,std;
        protected Button delete;

        private Context context1;

        public InboxViewHolder(View itemView,Context context) {
            super(itemView);
            this.context1=context;



            date = (TextView) itemView.findViewById(R.id.date);
            noticetype = (TextView) itemView.findViewById(R.id.notice_text);
            noticedesc = (TextView) itemView.findViewById(R.id.notice);
            delete=(Button)itemView.findViewById(R.id.delete);
            std=(TextView) itemView.findViewById(R.id.standard);

        }

    }


    public class DeleteNotice extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("id", params[1]));
                nameValuePairList.add(new BasicNameValuePair("type", params[2]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                Delete1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

            } catch (UnknownHostException e) {
                Delete1 = "6";
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            dismissProgressDialog();

            if (Delete1.equals("6")){
                Toast.makeText(context, "no internet connection...", Toast.LENGTH_LONG).show();
            }
            else if (Delete1.equals("1")) {

                Toast.makeText(context, "Notice deleted successfully...", Toast.LENGTH_LONG).show();

                Facultyviewnotice.notice.finish();



            }
        }
    }
    public void showProgressDialog() {

        pb.setMessage("Deleting notice....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();

    }


    public void dismissProgressDialog() {

        pb.dismiss();
    }

}
