package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 22/04/2015.
 */
public class GetfacultyrepInfo {
    public String reqtitle;
    public String reqdate;
    public String reqdes;

    public String getReqtitle() {
        return reqtitle;
    }

    public void setReqtitle(String reqtitle) {
        this.reqtitle = reqtitle;
    }



    public String getReqdate() {
        return reqdate;
    }

    public void setReqdate(String reqdate) {
        this.reqdate = reqdate;
    }

    public String getReqdes() {
        return reqdes;
    }

    public void setReqdes(String reqdes) {
        this.reqdes = reqdes;
    }
}
