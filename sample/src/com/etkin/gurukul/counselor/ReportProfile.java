package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class ReportProfile extends Activity {
    Button cretrep,replist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reportprofile);
        cretrep=(Button)findViewById(R.id.crereport);
        cretrep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReportProfile.this,ReportActivity.class));

            }
        });
        replist=(Button)findViewById(R.id.replist);
        replist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReportProfile.this,ReportRecyclerActivity.class));

            }
        });

    }
}
