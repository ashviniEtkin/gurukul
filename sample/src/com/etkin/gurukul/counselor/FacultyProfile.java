package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class FacultyProfile extends Activity {
    ImageView facultyreg, faclist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facultyprofile);
        facultyreg = (ImageView) findViewById(R.id.fregister);
        facultyreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FacultyProfile.this, FacultyRegistration.class));
            }
        });
        faclist = (ImageView) findViewById(R.id.flist);
        faclist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FacultyProfile.this, Faculty_List.class));
            }
        });

    }
}
