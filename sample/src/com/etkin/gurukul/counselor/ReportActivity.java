package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 20/04/2015.
 */
public class ReportActivity extends Activity {
    Button sendreport,reportdt;
    Context context;
   EditText reporttit, reportdes;
    String tvrepdt, tvreptit, tvrepdes, output, selected_date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createreport);

        final DatePickerDialog doadDlg = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                selected_date= strDate.toString();
                reportdt.setText(selected_date);

            }}, 2011,0, 1);
        //Read the content
        reportdt = (Button) findViewById(R.id.repdt);
        reportdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doadDlg.show();
            }
        });
        reporttit = (EditText) findViewById(R.id.reptit);
        reportdes = (EditText) findViewById(R.id.redes);
        sendreport = (Button) findViewById(R.id.rsend);
        sendreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //srting conversion
                if (reporttit.length() == 0)
                    reporttit.setError("You Must Enter Title");
                else if (reportdes.length() == 0)
                    reportdes.setError("Plese Enter Description");
                else if (reportdt == null) {
                    reportdt.setError("Please Select Date");

                } else {


                    tvrepdt = reportdt.getText().toString().trim();
                    tvreptit = reporttit.getText().toString().trim();
                    tvrepdes = reportdes.getText().toString().trim();
                    new SendReports().execute(getString(R.string.AdminIp)+"/sendReport", tvreptit, tvrepdes, tvrepdt);
                }
            }
        });
    }
    class SendReports extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            List<BasicNameValuePair> nameValuePairList = new ArrayList<>(1);
            nameValuePairList.add(new BasicNameValuePair("reporttitle1", params[1]));
            nameValuePairList.add(new BasicNameValuePair("reportdesc1", params[2]));
            nameValuePairList.add(new BasicNameValuePair("reportdate1", params[3]));
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                output = data;

            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {
                output = "6";
            } catch (IOException e1) {
                e1.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (output.equals("1")) {

                Toast.makeText(getApplicationContext(), "Report has been Send...", Toast.LENGTH_LONG).show();
                reportdt.setText("");
                reporttit.setText("");
                reportdes.setText("");
                finish();
            } else {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }


        }

    }
}
