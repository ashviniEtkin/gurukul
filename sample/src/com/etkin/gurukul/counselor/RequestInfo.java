package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */
public class RequestInfo {
    public String title;
    public String date;
    public String desc;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
