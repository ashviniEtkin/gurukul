package com.etkin.gurukul.counselor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.etkin.gurukul.R;
import com.etkin.gurukul.menu.MenuActivity;


public class C_MainActivity extends ActionBarActivity {
    ImageView faculty,stud ,not, payfees,request,report,notice,timetable;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_main);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Counselor Menu");
        faculty=(ImageView)findViewById(R.id.faculty);
        faculty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,FacultyProfile.class));
            }
        });
        notice=(ImageView)findViewById(R.id.notice);
        notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,InsertNotice.class));
            }
        });
        report=(ImageView)findViewById(R.id.reportone);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,ReportProfile.class));
            }
        });
        report=(ImageView)findViewById(R.id.reportone);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,ReportProfile.class));
            }
        });
        request=(ImageView)findViewById(R.id.req);
        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,RequestProfileActivity.class));
            }
        });
        stud=(ImageView)findViewById(R.id.student);
        stud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,StudentProfileActivity.class));
            }
        });
    /*    timetable=(ImageView)findViewById(R.id.timetable);
        timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,C_TimeTableMenu.class));
            }
        });*/
        payfees=(ImageView)findViewById(R.id.payfees);
        payfees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_MainActivity.this,Installment.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.counselor_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout() {

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences(C_LoginActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        sharedPreferences1.edit().clear().commit();
        Intent intent=new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
        finish();
    }

}
