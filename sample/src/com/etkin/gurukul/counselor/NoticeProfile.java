package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class NoticeProfile extends Activity {
    ImageView createn,noticelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noticeprofile);

        createn=(ImageView)findViewById(R.id.createnotice);
        createn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NoticeProfile.this,InsertNotice.class));
            }
        });
        noticelist=(ImageView)findViewById(R.id.noticelist);
        noticelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NoticeProfile.this,Facultyviewnotice.class));
            }
        });

    }
}
