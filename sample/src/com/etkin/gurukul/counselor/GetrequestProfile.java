package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.etkin.gurukul.R;

/**
 * Created by Administrator on 20/04/2015.
 */
public class GetrequestProfile extends Activity {
    Button stud,fac,own,verify,declined;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getrequestprofile);
      /*  stud=(Button)findViewById(R.id.studreq);
        stud.setOnClickListener(new View.OnClickListener() {
            @Override
               public void onClick(View v) {
                startActivity(new Intent(GetrequestProfile.this,GetRequeststudRecyclerActivity.class));
           }
        });*/
       fac=(Button)findViewById(R.id.facreq);
        fac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetrequestProfile.this,GetRequestFacRecyclerActivity.class));
            }
        });
        own=(Button)findViewById(R.id.ownreq);
        own.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetrequestProfile.this,GetRequestownRecyclerActivity.class));
            }
        });
//        verify=(Button)findViewById(R.id.verreq);
//        verify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(GetrequestProfile.this,VerifyRecyclerActivity.class));
//            }
//        });
//        declined=(Button)findViewById(R.id.decreq);
//        declined.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(GetrequestProfile.this,DeclinedRecyclerActivity.class));
//            }
//        });
//
   }
}
