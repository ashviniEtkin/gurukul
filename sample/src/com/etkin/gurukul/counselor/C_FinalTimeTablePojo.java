package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 14/04/2015.
 */
public class C_FinalTimeTablePojo {

    public String date;
    public String sub;
    public String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




}
