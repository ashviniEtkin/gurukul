package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 04/04/2015.
 */
public class C_studentReg extends Activity
{
    // Activity request codes                    FROM HERE CAMERA


    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    String output;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    private Uri fileUri; // file url to store image/video
    private ImageView imgPreview,cam,gal;
    private String imagepath = "1";   // TO CAMERA
    String selected_dob= "1";
    String selected_doadm= "1";

    TextView sdobbtn,sdoadmbtn;
    Spinner s_gensp;
    String studentImageString;

    String s_gen = "1";
    String s_nm,s_pnm,s_eid,s_pass,s_schoolnm,s_cno,s_add;
    EditText s_nmet,s_pnmet,s_eidet,s_passet,s_schoolnmet,s_cnoet,s_addet;

    Bitmap studentImageBitmap;
    ProgressDialog pg;



    String MobilePattern = "[0-9]{10}";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_studentreg);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");
        pg.setCancelable(false);

        s_nmet= (EditText) findViewById(R.id.snm);
        s_pnmet= (EditText) findViewById(R.id.sparentnm);
        s_eidet= (EditText) findViewById(R.id.slogin);
        s_passet= (EditText) findViewById(R.id.spass);

        s_schoolnmet= (EditText) findViewById(R.id.schoolnm);
        s_cnoet= (EditText) findViewById(R.id.scno);
        s_addet= (EditText) findViewById(R.id.sadd);
        s_gensp= (Spinner) findViewById(R.id.sgen);
        s_gensp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                s_gen= (String) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(),"Please Select Student Gender",Toast.LENGTH_SHORT);
            }
        });

        imgPreview = (ImageView) findViewById(R.id.scameraimg);
        cam = (ImageView) findViewById(R.id.scamerabtn);
        cam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                captureImage();
            }
        });
        gal = (ImageView) findViewById(R.id.sgallerybtn);
        gal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectImage();
            }
        });

//        date picker for date of birth
        final DatePickerDialog dobDlg = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                selected_dob= strDate.toString();
                sdobbtn.setText(selected_dob);

            }}, 1995,0, 1);
        sdobbtn = (TextView) findViewById(R.id.sdob);
        sdobbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dobDlg.show();
            }
        });

        //        date picker for date of admission

        final DatePickerDialog doadDlg = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                selected_doadm= strDate.toString();
                sdoadmbtn.setText(selected_doadm);
            }}, 2011,0, 1);
        sdoadmbtn = (TextView) findViewById(R.id.sdoa);
        sdoadmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doadDlg.show();
            }
        });

        Button sreg = (Button) findViewById(R.id.snextbtn);
        sreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                s_cno = s_cnoet.getText().toString().trim();
                String MobilePattern = "[0-9]{10}";
                final String eid1 = s_eidet.getText().toString().trim();
                if(s_nmet.length()==0)
                    s_nmet.setError("You haven't specified Student name");
                else if(s_pnmet.length()==0)
                    s_pnmet.setError("You haven't specified Parent Name");
                else if(s_passet.length()==0)
                    s_passet.setError("You haven't specified Password");

                else if(selected_dob.equals(1)) {
                    sdobbtn.setError("You haven't selected Date Of Birth");
                    sdobbtn.setText("Select Date Of Birth");
                }
                else if(selected_doadm.equals(1))
                {
                    sdoadmbtn.setError("You haven't selected the Subject(s)");
                    sdoadmbtn.setText("Tap to Select Date of Admission");
                }

                else if(imagepath.equals(1))
                {
                    Toast.makeText(getApplicationContext(),"Please Select Image",Toast.LENGTH_LONG).show();
                }
                else if( s_gen.equals(1))
                {
                    Toast.makeText(getApplicationContext(),"Please Select Gender",Toast.LENGTH_LONG).show();
                }

                else if(s_schoolnmet.length()==0)
                    s_schoolnmet.setError("You haven't specified School Name");
                else if(s_cnoet.length()==0)
                    s_cnoet.setError("You haven't specified Contact Number");
                else  if(!s_cno.matches(MobilePattern))
                    s_cnoet.setError(" Contact Number must be 10 digit");
                else if(s_addet.length()==0)
                    s_addet.setError("You haven't specified Address");
                else
                {
                    s_nm = s_nmet.getText().toString().trim();
                    s_pnm = s_pnmet.getText().toString().trim();
                    studentImageString = encodeToBase64(studentImageBitmap);
                    s_eid= s_eidet.getText().toString().trim();
                    s_pass =  s_passet.getText().toString().trim();
                    s_schoolnm = s_schoolnmet.getText().toString().trim();
                    s_cno = s_cnoet.getText().toString().trim();
                    s_add = s_addet.getText().toString().trim();



                    Bundle b = new Bundle();
                    b.putString("studnm",s_nm);
                    b.putString("studpnm",s_pnm);
                    b.putString("studdpnm",s_nm);
                    b.putString("studimage",studentImageString);
                    b.putString("studeid",s_eid);
                    b.putString("studpass",s_pass);
                    b.putString("studdob",selected_dob);
                    b.putString("studgen",s_gen);
                    b.putString("studdoad",selected_doadm);
                    b.putString("studschoolnm",s_schoolnm);
                    b.putString("studcno",s_cno);
                    b.putString("studadd",s_add);
                    Intent in = new Intent(C_studentReg.this,C_submit_StudentReg.class);
                    in.putExtras(b);
                    startActivity(in);
                }
            }
        });

    }

    //validating email
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                try {
                    // hide video preview
                    imgPreview.setVisibility(View.VISIBLE);
                    // bimatp factory
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger images
                    options.inSampleSize = 8;
                    final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                    studentImageBitmap = bitmap;
                    imgPreview.setImageBitmap(bitmap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                imagepath = getPath(selectedImageUri);
                Bitmap bitmap = BitmapFactory.decodeFile(imagepath);
                studentImageBitmap = bitmap;
                imgPreview.setImageBitmap(bitmap);
//            messageText.setText("Uploading file path:" + imagepath);
//                imagedata = encodeToBase64(facultyImageBitmap);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Image has not been selected", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera()
    {
        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /**
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void selectImage()
    {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String getPath(Uri uri)
    {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }



    /**
     * ------------ Helper Methods ----------------------
     * */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    // FUNCTION TO CONVERT IMAGE INTO BASE64 STRING
    public static String encodeToBase64(Bitmap image) {
        System.gc();
        if (image == null) {   return null;  }
        Bitmap image64 = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image64.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageencoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageencoded;
    }





}

