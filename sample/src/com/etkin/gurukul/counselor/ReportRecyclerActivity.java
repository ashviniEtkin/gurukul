package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 20/04/2015.
 */
public class ReportRecyclerActivity extends Activity {
    RecyclerView recList,res;
    CardView cardView;
    View v;
    String title,date;
    List<ReportInfo> result = new ArrayList();

    ReportAdapter ra;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reportrecyclerview);

        recList = (RecyclerView) findViewById(R.id.reportrecylerviewone);

        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(ReportRecyclerActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        new ReportList().execute(getString(R.string.AdminIp)+"/getReports");

    }


    public class ReportList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("reports");


                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        // date=jrealobj.getString("date").toString();
                        //title=jrealobj.getString("title").toString();
                        ReportInfo ci = new ReportInfo();

                        ci.setReportid(jrealobj.getString("r_id"));
                        ci.setDate(jrealobj.getString("r_date"));
                        //  ci.setNoticeby(jrealobj.getString("by"));
                        ci.setTitle(jrealobj.getString("r_title"));

                        result.add(ci);
                        Log.e("msg", ci.getTitle());
                        Log.e("msg",ci.getDate());
                        // Log.e("msg",ci.getNoticeby());

                    }


                    Log.e("msg",data);
                    ra = new ReportAdapter(ReportRecyclerActivity.this,result);

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                // Toast.makeText(getBaseContext(), title+date , Toast.LENGTH_LONG).show();
                recList.setAdapter(ra);

            }
        }

    }
}
