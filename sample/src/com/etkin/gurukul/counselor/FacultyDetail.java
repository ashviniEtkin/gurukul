package com.etkin.gurukul.counselor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 20/04/2015.
 */
public class FacultyDetail extends ActionBarActivity {
    List<Faculltydetailpojo> result = new ArrayList();
    RecyclerView recList,res;
    CardView cardView;
    View v;
    FacultydetailAdapter fa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facultyrecyclerview);

        recList = (RecyclerView) findViewById(R.id.cardList);

        // cardView.setCardBackgroundColor(getResources().getColor(R.color.headBack));
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(FacultyDetail.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        new FacultyList().execute(getString(R.string.AdminIp)+"/getfacultydetails");

    }


    public class FacultyList extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("facultydetails");


                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        // date=jrealobj.getString("date").toString();
                        //title=jrealobj.getString("title").toString();
                        Faculltydetailpojo ci = new Faculltydetailpojo();



                        ci.setName(jrealobj.getString("f_nm"));
                        ci.setPhoto(jrealobj.getString("f_photoname"));
                        ci.setFacid(jrealobj.getString("f_eid"));

                        result.add(ci);
                        Log.e("msg", ci.getFacid());
                        Log.e("msg",ci.getName());
                        Log.e("msg",ci.getPhoto());
                        // Log.e("msg",ci.getNoticeby());

                    }


                    Log.e("msg",data);
                    fa = new FacultydetailAdapter(FacultyDetail.this,result);

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {

                recList.setAdapter(fa);

           }
        }

    }




