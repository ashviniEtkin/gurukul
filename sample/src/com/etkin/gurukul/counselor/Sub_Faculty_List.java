package com.etkin.gurukul.counselor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etkin.gurukul.R;

import network.LruBitmapCache;
import network.VolleySingleton;


public class Sub_Faculty_List extends ActionBarActivity {

    EditText Name,Qualification,Salary,Email,Contact,Address,Password;
    Button Standard,Subject;
    String name,qualification,standard,subject,salary,email,contact,address,id,Image,date,password;
    NetworkImageView nImageView;
    RequestQueue queue;
    ImageLoader imgLoader;
    String delete,update;
    ProgressDialog pb,pb1;
    private static ActionBarActivity sub_faculty_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__faculty__list);
        pb=new ProgressDialog(Sub_Faculty_List.this);
        pb1=new ProgressDialog(Sub_Faculty_List.this);
        Name=(EditText)findViewById(R.id.Name);
        Qualification=(EditText)findViewById(R.id.Qualification);
        Standard=(Button)findViewById(R.id.Standard);
        Subject=(Button)findViewById(R.id.Subject);
        Salary=(EditText)findViewById(R.id.Salary);
        Email=(EditText)findViewById(R.id.Email);
        Password=(EditText)findViewById(R.id.Password);
        Contact=(EditText)findViewById(R.id.Contact);
        Address=(EditText)findViewById(R.id.Address);
        nImageView=(NetworkImageView)findViewById(R.id.photo);
        queue= VolleySingleton.getInstance().getRequestQueue();
        imgLoader=new ImageLoader(queue,new LruBitmapCache(LruBitmapCache.getCacheSize(getApplicationContext())));
        sub_faculty_list=this;

        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        Image=intent.getStringExtra("photo");
        name=intent.getStringExtra("name");
        qualification=intent.getStringExtra("qualification");
        standard=intent.getStringExtra("standard");
        subject=intent.getStringExtra("subject");
        salary=intent.getStringExtra("salary");
        email=intent.getStringExtra("email");
        password=intent.getStringExtra("password");
        address=intent.getStringExtra("address");
        contact=intent.getStringExtra("contact");
        date=intent.getStringExtra("date");

        nImageView.setImageUrl(getString(R.string.image)+Image,imgLoader);
        Name.setText(name);
        Qualification.setText(qualification);
        Salary.setText(salary);
        Email.setText(email);
        Password.setText(password);
        Address.setText(address);
        Contact.setText(contact);
        Standard.setText(standard);
        Subject.setText(subject);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub__faculty__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
