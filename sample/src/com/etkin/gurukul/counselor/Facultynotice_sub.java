package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.etkin.gurukul.R;


public class Facultynotice_sub extends Activity {
    TextView text1,text2,text3;
    String Date,Head,Notice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultynotice_sub);
        text1 = (TextView) findViewById(R.id.date);
        text2 = (TextView) findViewById(R.id.heading);
        text3 = (TextView) findViewById(R.id.notice);
        setDetail();
        text1.setText(Date);
        text2.setText(Head);
        text3.setText(Notice);


    }


    private void setDetail() {

        Intent intent=getIntent();
        Date =intent.getStringExtra("noticedate");
        Head = intent.getStringExtra("noticetitle");
        Notice =intent.getStringExtra("noticedesc");



    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_facultynotice_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
