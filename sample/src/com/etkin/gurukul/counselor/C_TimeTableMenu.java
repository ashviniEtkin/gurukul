package com.etkin.gurukul.counselor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.etkin.gurukul.R;


public class C_TimeTableMenu extends ActionBarActivity {

    Button btn_create,btn_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_timetablemenu);

        btn_create=(Button)findViewById(R.id.create);
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_TimeTableMenu.this,C_TestTimeTableActivity.class));
            }
        });

        btn_view=(Button)findViewById(R.id.view);
        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(C_TimeTableMenu.this,C_TableListSpinner.class));
            }
        });
    }

}
