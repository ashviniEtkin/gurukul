package com.etkin.gurukul.counselor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.etkin.gurukul.R;

import java.util.ArrayList;
import java.util.List;

public class C_FinalTimeTableAdapter extends ArrayAdapter<C_FinalTimeTablePojo> {

    private final List<C_FinalTimeTablePojo> list;
    //private final Activity context;

    LayoutInflater li;
    int textViewResourceId;
    Context context;
    String dt,day,mnth,yr;

    public C_FinalTimeTableAdapter(Context context, int textViewResourceId, ArrayList<C_FinalTimeTablePojo> list) {
        super(context,textViewResourceId,list);
        this.context = context;
        this.textViewResourceId=textViewResourceId;
        this.list = list;
        li=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        protected TextView text1;
        protected TextView text2;
        protected EditText et;
        protected EditText et1;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {



            convertView = li.inflate(textViewResourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.text1 = (TextView) convertView.findViewById(R.id.tx_sub);
            viewHolder.text2 = (TextView) convertView.findViewById(R.id.tx_date);
            viewHolder.et = (EditText) convertView.findViewById(R.id.et_time);
            viewHolder.et1 = (EditText) convertView.findViewById(R.id.et_syllabus);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.text1.setText(list.get(position).getSub());
        viewHolder.text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //viewHolder.text2.setText("hello");
                //showDateDialog();
                //Toast.makeText(context, dt, Toast.LENGTH_LONG).show();
            }
        });
        return convertView;
    }
 /*   public void showDateDialog(View v)
    {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub

                selectedmonth = selectedmonth + 1;
               // date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();
    }*/


}