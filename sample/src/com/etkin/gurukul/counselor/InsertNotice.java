package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.etkin.gurukul.StudentAdapter;
import com.etkin.gurukul.faculty.AttendanceNow;
import com.etkin.gurukul.faculty.CardViewActivity;

import com.etkin.gurukul.faculty.DbHandler;
import com.etkin.gurukul.faculty.FacultyAttendancePojo;
import com.etkin.gurukul.faculty.FacultyTestReportActivity;
import com.etkin.gurukul.faculty.QrActivity;
import com.etkin.gurukul.gurukul_notify.Notification;


public class InsertNotice extends Activity {
    String STD, DATE, SUBJECT, BATCH_TIME;
    int year, month, day;
    public static int aa=0;
    public  static  int a;

    SharedPreferences.Editor editor;

    public static String MyPREFERENCE = "MyPref";
    ArrayList<FacultyAttendancePojo> mDatasetHome = new ArrayList<FacultyAttendancePojo>();

    ArrayList<AttendanceNow> attend = new ArrayList<AttendanceNow>();

    String[] StudName;
    String[] StudMob;
    String[] StudStatus;
    String[] StudId;
    String[] TIME1;

    private RecyclerView.Adapter mAdapter;
    ArrayList sub;
    ArrayList time;
    String[] SUB1;
    String date, Register1;
    TextView text1, std, total, batch_time;
    EditText text3, text4;
    String data;
    String  Type, su, ti;
    AlertDialog alertDialog;
    ProgressDialog pb;
    ArrayList<String> course = new ArrayList<String>();
    ArrayList<String> coursebutton = new ArrayList<String>();
    String a1, selectItem1, a2, selectItem2, selectItem, selectItem3,tittle,desc;
    final CharSequence[] items = {"Student_Inbox"};
    Context con;
    String COURSE;
    CheckBox forword_to_all, check_all;
    Button sel_class, next,send_btn,view_notice;
    SharedPreferences sharedPreferences1;
    LinearLayout linearlayout_one;

    ArrayList<String>   arr;
    //  RecyclerView lv_stud_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_notice);
        con = this;
        pb = new ProgressDialog(con);

        std = (TextView) findViewById(R.id.std);
        batch_time = (TextView) findViewById(R.id.batch_time);
        text3 = (EditText) findViewById(R.id.title);
        //text1 = (TextView) findViewById(R.id.type);
        text4 = (EditText) findViewById(R.id.desc);
        next = (Button) findViewById(R.id.next_btn);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        date = String.format("%d-%d-%d", day, month + 1, year);
        forword_to_all = (CheckBox) findViewById(R.id.forword_to_all);

        sel_class = (Button) findViewById(R.id.sel_standard);
        send_btn=(Button)findViewById(R.id.send_btn);
        view_notice=(Button)findViewById(R.id.view_notice);
        linearlayout_one=(LinearLayout)findViewById(R.id.linearlayout_one);


        arr = new ArrayList<String>();

        sharedPreferences1 = getSharedPreferences(MyPREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences1.edit();

        forword_to_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    new StudentList().execute(getString(R.string.AdminIp) + "/GetStudent", "ALL", "Regular", "ALL");


                    aa = 1;
                    sel_class.setVisibility(View.GONE);
                    std.setVisibility(View.GONE);
                    batch_time.setVisibility(View.GONE);
                    linearlayout_one.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    editor.putInt("CHECK_ALL", aa);
                    editor.commit();
                } else {

                    aa = 0;
                    editor.putInt("CHECK_ALL", aa);
                    editor.commit();
                    next.setVisibility(View.VISIBLE);
                    linearlayout_one.setVisibility(View.GONE);
                    sel_class.setVisibility(View.VISIBLE);
                    std.setVisibility(View.VISIBLE);
                    batch_time.setVisibility(View.VISIBLE);


                }
            }
        });


        sel_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new showcourse().execute(getString(R.string.AdminIp) + "/noticestandard1");
            }
        });

       /* btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                coursebutton.clear();
                new showcoursebutton().execute(getString(R.string.AdminIp) + "/noticestandard1");
            }
        });*/
        std.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url2 = getResources().getString(R.string.stdDetailsURL);
                new DisplayTask().execute(url2, selectItem1);


            }
        });


        batch_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url3 = getResources().getString(R.string.courseTimeURL);
                new DisplayCourseTime().execute(url3, selectItem1, selectItem2);
            }
        });


        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                for(int k=0;k<arr.size();k++){
                    Toast.makeText(getApplicationContext(),arr.get(k).toString(),Toast.LENGTH_LONG).show();
                }

                submitDialog();
               // finish();

            }
        });

        view_notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getApplicationContext(), Facultyviewnotice.class);
                in.putExtra("standard", "ALL");
                startActivity(in);

            }
        });


        next.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {


                if (forword_to_all.isChecked()) {

                /*    aa=1;
                    editor.putInt("CHECK_ALL", aa);
                    editor.commit();

                    tittle=text3.getText().toString();
                    desc=text4.getText().toString();

                    Intent in = new Intent(getApplicationContext(), NextInsertActivity.class);
                    in.putExtra("course", "Regular");
                    in.putExtra("std", selectItem1);
                    in.putExtra("sub", selectItem2);
                    in.putExtra("time", selectItem3);
                    in.putExtra("tittle", selectItem2);
                    in.putExtra("desc", selectItem3);

                    //finish();
                    startActivity(in);*/


                    // new Feedback().execute(getString(R.string.AdminIp) + "/Insertnotice", Standard, date, Title, Type, Desc);
                    //check();
                } else {

                    aa=0;
                    editor.putInt("CHECK_ALL",aa);
                    editor.commit();

                    tittle=text3.getText().toString();
                    desc=text4.getText().toString();

                    Intent in = new Intent(getApplicationContext(), NextInsertActivity.class);
                    in.putExtra("course", "Regular");
                    in.putExtra("std", selectItem1);
                    in.putExtra("sub", selectItem2);
                    in.putExtra("time", selectItem3);
                    in.putExtra("tittle", tittle);
                    in.putExtra("desc", desc);
                    //finish();
                    startActivity(in);
                    
                  /*  Standard = std.getText().toString().trim();
                    Title = text3.getText().toString().trim();
                    Type = "Student_Inbox";
                    Desc = text4.getText().toString().trim();*/

                    // submitDialog();
                    //  new Feedback().execute(getString(R.string.AdminIp) + "/Insertnotice", Standard, date, Title, Type, Desc);

                }


            }


        });

    }

    public class DisplayCourseTime extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            nameValuePairList.add(new BasicNameValuePair("sub", arg0[2]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("coursedata");
                    time = new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        //sub.add(jrealobj.getString("coursename"));
                        time.add(jrealobj.getString("coursetiming"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if (result == false) {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            } else {

                showSelectTimeDialog();
            }
        }

    }


    private void showSelectTimeDialog() {
        //COURSE1=new String[course.size()];
        TIME1 = (String[]) time.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select BatchTime...");
        ad1.setSingleChoiceItems(TIME1, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                ti = TIME1[array];
                batch_time.setText(ti);
                selectItem3 = batch_time.getText().toString();
                dialog.dismiss();

            }
        });
        ad1.show();

    }

    public class StudentList extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

                nameValuePairList.add(new BasicNameValuePair("gender", params[1]));
                nameValuePairList.add(new BasicNameValuePair("course", params[2]));
                nameValuePairList.add(new BasicNameValuePair("sclass", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                // JSONObject jobj = new JSONObject(data);
                // JSONArray jArray = jobj.getJSONArray("information");
                JSONArray jsonArray = new JSONArray(data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    AttendanceNow fap = new AttendanceNow();
                    String id = jrealobj.getString("email");
                    String name = jrealobj.getString("sname");
                    fap.setSTUD_ID(jrealobj.getString("email"));
                    fap.setNAME(jrealobj.getString("sname"));
                    fap.setMOB_NO(jrealobj.getString("contact"));
                    fap.setROLL(jrealobj.getString("rollNo"));
                    //  fap.setS;

                    attend.add(fap);
                    arr.add(id);
                    //  Toast.makeText(getApplicationContext(),"Id of "+name+" is "+id,Toast.LENGTH_LONG).show();
                    Log.d("Id of " + name + " is ", id);

                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
           /* //mAdapter = new C_NoticeAdapter(attend, getApplicationContext(),check_all);

            a = attend.size();
            total.setText("Total : " + (a + ""));
            // lv_stud_list.setAdapter(mAdapter);
           *//* lv_stud_list.setVisibility(View.GONE);*/

        }
    }

    public void submitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InsertNotice.this);

        // set title
        alertDialogBuilder.setTitle("Warning !");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you really want to Send Notice ?")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {

                        Type = "Student_Inbox";
                        tittle=text3.getText().toString();
                        desc=text4.getText().toString();
                        new Feedback().execute(getString(R.string.AdminIp) + "/Insertnotice", date, tittle, Type, desc);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

    public class DisplayTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", arg0[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("stddata");
                    sub = new ArrayList();
                    // std=new String[jarray.length()];
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        // std[i]=jrealobj.getString("stadard");
                        sub.add(jrealobj.getString("subject"));
                        // time.add(jrealobj.getString("coursetiming"));

                    }

                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();

            if (result == false) {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            } else {
                showSelectSubDialog();


            }
        }

    }

    private void showSelectSubDialog() {
        //COURSE1=new String[course.size()];
        SUB1 = (String[]) sub.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Subject...");
        ad1.setSingleChoiceItems(SUB1, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                su = SUB1[array];
                std.setText(su);
                selectItem2 = std.getText().toString();
                dialog.dismiss();

              /*  if ((sel_class.getText().toString().equals("") || std.getText().toString().equals(""))) {
                    Toast.makeText(getApplicationContext(),"select batch and class",Toast.LENGTH_LONG).show();
                } else {
                    getList();
                }*/


            }
        });
        ad1.show();
        //std.clear();
        sub.clear();
        //time.clear();
    }


    public class showcourse extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);
                // post.setEntity(new UrlEncodedFormEntity(nameValuePairList));


                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("standard");
                for (int i = 0; i < jarray.length() - 1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    subjectpojo company1 = new subjectpojo();
                    company1.setCourse(jrealobj.getString("std"));
                    // branch.add(company.getsub().toString());
                    course.add(company1.getCourse().toString());
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            } catch (NullPointerException e2) {

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();

            Courses();

        }

    }

    String[] abc;

    public void Courses() {

        //abc=(String[])course.toArray(new String[0]);
        abc = new String[course.size()];
        abc = course.toArray(abc);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select Course...");
        ad1.setSingleChoiceItems(abc, 1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                sel_class.setText("");
                COURSE = abc[array].toString();
                selectItem1 = COURSE;
                sel_class.setText("");
                sel_class.setText(COURSE.toString());
                dialog.dismiss();

            }
        });
        ad1.show();
    }

    public class showcoursebutton extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {


            try {



                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);
                // post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jobj = new JSONObject(data);

                JSONArray jarray = jobj.getJSONArray("standard");
                for (int i = 0; i < jarray.length() - 1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(i);
                    subjectpojo company1 = new subjectpojo();
                    company1.setCourse(jrealobj.getString("std"));
                    // branch.add(company.getsub().toString());
                    coursebutton.add(company1.getCourse().toString());
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;

            } catch (NullPointerException e2) {

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }


            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            button();
        }

    }

    String[] abc1;

    public void button() {
        abc1 = (String[]) coursebutton.toArray(new String[0]);
        final AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        ad1.setTitle("Select notice standard...");
        ad1.setSingleChoiceItems(abc1, 1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int array) {
                a2 = abc1[array];
                selectItem2 = a2;
                Intent in = new Intent(getApplicationContext(), Facultyviewnotice.class);
                in.putExtra("standard", a2);
                startActivity(in);
                dialog.dismiss();

            }
        });
        ad1.show();


    }





    public class Feedback extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

                for(int k=0;k<arr.size();k++){
                    nameValuePairList.add(new BasicNameValuePair("email_id", arr.get(k)));
                }
                nameValuePairList.add(new BasicNameValuePair("notice_date", params[1]));
                nameValuePairList.add(new BasicNameValuePair("notice_title", params[2]));
                nameValuePairList.add(new BasicNameValuePair("status", params[3]));
                nameValuePairList.add(new BasicNameValuePair("notice_desc", params[4]));


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                data = EntityUtils.toString(entity);
                JSONArray jsonArray = new JSONArray(data);
                List<String> gcmids = new ArrayList<String>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    gcmids.add(jsonArray.getString(i));
                }

                Notification.sendNotification(gcmids, params[4], params[3], getApplicationContext());
                data = "1";
                //  Register1 = data;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                data = "6";

            } catch (UnknownHostException e) {
                data = "6";
            } catch (IOException e) {
                e.printStackTrace();
                data = "6";
            } catch (Exception e) {
                e.printStackTrace();
                data = "6";
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();


           /* if (data.equals("6")){
                Toast.makeText(getApplicationContext(), "no internet connection...", Toast.LENGTH_LONG).show();
            }*/
            if (data.equals("6")) {
                Toast.makeText(getApplicationContext(), "Notice send...", Toast.LENGTH_LONG).show();
            } else if (data.equals("1")) {


                Toast.makeText(getApplicationContext(), "Notice send...", Toast.LENGTH_LONG).show();
                finish();


            } else if (data.equals("2")) {
                Toast.makeText(getApplication(), "You have already send....", Toast.LENGTH_LONG).show();
            }


        }
    }

    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_insert_notice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

