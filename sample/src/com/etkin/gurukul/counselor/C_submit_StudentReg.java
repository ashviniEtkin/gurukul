package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 13/04/2015.
 */
public class C_submit_StudentReg extends Activity {

    String output;
    String s_nm, s_pnm, s_dpstring, s_eid, s_pass, s_gen, s_dob, s_doad, s_schoolnm, s_cno, s_add;//to catch intented data from previous page
    String s_remainingfees;
    String s_feespaid;
    String s_totalfees;
    String backlog = "no";
    TextView tvsnm, tvtotalfees;
    String s_dpnm = null;
    EditText etfeespaid, tvseid;
    TextView s_coursename, s_classname, s_batchname, sdoinsbtn;
    Spinner s_mediumsp;

    String crsnm, clsnm, batchname, subnm, subfees, studenttype, batchtime, noOfData;
    String s_class = "1";
    String s_courses = "1";
    String s_subject = "1";
    String s_medium = "1";
    String selected_doins = "1";

    //    s_subject,
    // to fetch courses
    ArrayList<Integer> mSelectedItems;
    ArrayList<String> crslist;
    // to fetch class
    ArrayList<String> classlist;
    // to fetch subjects
    ArrayList<String> sublist;
    // to fetch batch name
    ArrayList<String> batchlist;
    // to fetch courses fes
    ArrayList<Integer> feeslist;
    ArrayList<String> batchtimelist;

    //arraylist to send data to be stored
    ArrayList<String> finalfeeslist;
    ArrayList<String> finalsublist;
    ArrayList<String> finalbatchlist;
    ArrayList<String> finalbatchtimelist;

    String[] s_feesArray;
    String[] s_subArray;
    String[] s_batchArray;
    String[] s_batchtimeArray;

    Integer totalfees = 0;
    Integer feespaid = 0;
    Integer remainingfees = 0;
    Integer a;
    EditText etlper;
    String s_lper;
    ProgressDialog pg;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.c_submit_studreg);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Please wait....");
        pg.setCancelable(false);
//            Intent in = getIntent();
        tvsnm = (TextView) findViewById(R.id.snmtv);
        tvseid = (EditText) findViewById(R.id.seidtv);
        tvtotalfees = (TextView) findViewById(R.id.tvstudtotolfees);
       /* etlper = (EditText) findViewById(R.id.etlastyearpercentage);*/
        Bundle extra = this.getIntent().getExtras();
        if (extra != null) {
            s_nm = extra.getString("studnm");
            s_pnm = extra.getString("studpnm");
            s_dpnm = extra.getString("studdpnm");
            s_dpstring = extra.getString("studimage");
            s_eid = extra.getString("studeid");
            s_pass = extra.getString("studpass");
            s_dob = extra.getString("studdob");
            s_gen = extra.getString("studgen");
            s_doad = extra.getString("studdoad");
            s_schoolnm = extra.getString("studschoolnm");
            s_cno = extra.getString("studcno");
            s_add = extra.getString("studadd");

            tvsnm.setText(s_nm);
            tvseid.setText(s_eid);
        }


    /*    s_mediumsp = (Spinner) findViewById(R.id.smedium);*/
      /*  s_mediumsp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                s_medium = (String) parent.getSelectedItem();
                Toast.makeText(getApplicationContext(), s_medium, Toast.LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please Select Medium", Toast.LENGTH_SHORT);
            }
        });*/

        s_coursename = (TextView) findViewById(R.id.scoursename);
        s_coursename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CourseAsynk().execute(getString(R.string.AdminIp) + "/getCoursename");
            }
        });

        s_classname = (TextView) findViewById(R.id.sclass);
        s_classname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ClassAsynk().execute(getString(R.string.AdminIp) + "/getSudentClass", s_coursename.getText().toString().toString());
            }
        });

        s_batchname = (TextView) findViewById(R.id.sbatchname);
        s_batchname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BatchDetailsAsynk().execute(getString(R.string.AdminIp) + "/getStudentSubject", s_classname.getText().toString(), s_coursename.getText().toString().toString());
            }
        });

        etfeespaid = (EditText) findViewById(R.id.sfees);

       /* s_lper = etlper.getText().toString().trim();*/
        s_feespaid = etfeespaid.getText().toString().trim();

        // date picker for next installment date

        final DatePickerDialog doiDlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Time chosenDate = new Time();
                chosenDate.set(dayOfMonth, monthOfYear, year);
                long dtDob = chosenDate.toMillis(true);
                CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
                //  Toast.makeText(getApplicationContext(),"Date picked: " + strDate, Toast.LENGTH_SHORT).show();
                // Set the Selected Date in variable for indenting it
                selected_doins = strDate.toString();
                sdoinsbtn.setText(selected_doins);
            }
        }, 2011, 0, 1);
        sdoinsbtn = (TextView) findViewById(R.id.sinstallmentdate);
        sdoinsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doiDlg.show();
            }
        });

        Button reg = (Button) findViewById(R.id.registerStudentbtn);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (s_class.equals(1))
                    Toast.makeText(getApplicationContext(), "Please select Class", Toast.LENGTH_SHORT).show();


                else if (s_courses.equals(1))
                    Toast.makeText(getApplicationContext(), "Please select Course", Toast.LENGTH_SHORT).show();

                else if (s_subject.equals(1))
                    Toast.makeText(getApplicationContext(), "Please select Subject(s)", Toast.LENGTH_SHORT).show();

                else if (s_medium.equals(1))
                    Toast.makeText(getApplicationContext(), "Please select Student Medium", Toast.LENGTH_SHORT).show();

               /* else if (etlper.length() == 0)
                    etlper.setError("You haven't specified Last Year Percentage Of Studnet");*/
                else if (etfeespaid.length() == 0)
                    etfeespaid.setError("You haven't specified fees paid By Student");

                else {
                  /*  s_lper = etlper.getText().toString().trim();*/
                    s_feespaid = etfeespaid.getText().toString().trim();
                    feespaid = Integer.parseInt(s_feespaid);
                    remainingfees = totalfees - feespaid;
                    s_remainingfees = remainingfees.toString();
//                    Toast.makeText(getApplicationContext(), "Your Remaining fees is : " + s_remainingfees, Toast.LENGTH_SHORT).show();

                    if (backlog.equals("no")) {
                        if (remainingfees == 0) {
                            studenttype = "1";
//                            sdoinsbtn.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(), studenttype, Toast.LENGTH_SHORT).show();
                        } else if (remainingfees != 0) {
                            studenttype = "2";
//                            Toast.makeText(getApplicationContext(), studenttype, Toast.LENGTH_SHORT).show();
                            if (selected_doins.equals(1))
                                sdoinsbtn.setError("Please select Date of Installment");
//                            sdoinsbtn.setVisibility(View.VISIBLE);
                        }
                    } else if (backlog.equals("yes")) {
                        studenttype = "0";
//                        Toast.makeText(getApplicationContext(), studenttype, Toast.LENGTH_SHORT).show();
                    }

                    if (s_dpnm == null) {
                        s_dpnm = getString(R.string.blankImage);
                    }
                    String userid = tvseid.getText().toString().trim();
                    if (userid.equals("")) {
                        userid = s_eid;
                    }
                    new StudentRegAsynk().execute(getString(R.string.AdminIp) + "/student_reg", s_nm, s_pnm, s_dob, s_gen, s_dpnm, s_dpstring, userid, s_pass, s_class, s_lper, s_medium, s_courses, s_doad, s_feespaid, s_remainingfees, s_totalfees, selected_doins, s_schoolnm, s_cno, s_add, studenttype, noOfData, s_subject);
                }
            }
        });

    }

   /* public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioYes:
                if (checked)
                    Toast.makeText(getApplicationContext(), "yes", Toast.LENGTH_SHORT).show();
                backlog = "yes";
                break;
            case R.id.radioNo:
                if (checked)
                    backlog = "no";
                Toast.makeText(getApplicationContext(), "no", Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    public class CourseAsynk extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("courses");
                    crslist = new ArrayList<String>();
                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        crsnm = jrealobj.getString("crs");

                        crslist.add(crsnm);
                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            pg.dismiss();
            if (result == false) {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            } else {
                showSelectCoursesDialog();
            }
        }

    }

    protected void showSelectCoursesDialog() {
        // TODO Auto-generated method stub
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Student's Course Name to be Join");

        builder.setSingleChoiceItems((String[]) crslist.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String[] coursearray = (String[]) crslist.toArray(new String[0]);
                s_courses = coursearray[which].toString();
                s_coursename.setText(s_courses.toString());
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }// end of dept dialog

    public class ClassAsynk extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

            nameValuePairList.add(new BasicNameValuePair("crs", params[1]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("sclass");
                    classlist = new ArrayList<String>();

                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);

                        clsnm = jrealobj.getString("standard");

                        classlist.add(clsnm);
                    }
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pg.dismiss();
            if (result == false) {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            } else {
//                ArrayAdapter<String> cad = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_single_choice,classlist);
//                ListView clv = (ListView) findViewById(R.id.classlistView);
//                clv.setAdapter(cad);

                showSelectClassDialog();
            }
        }

    }

    protected void showSelectClassDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Class Student want to join");

        builder.setSingleChoiceItems((String[]) classlist.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String[] classarray = (String[]) classlist.toArray(new String[0]);
                s_class = classarray[which].toString();
                s_classname.setText(s_class.toString());
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }// end of dept dialog

    public class BatchDetailsAsynk extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("std", params[1]));
            nameValuePairList.add(new BasicNameValuePair("crs", params[2]));
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("studsubjects");
                    sublist = new ArrayList<String>();
                    feeslist = new ArrayList<Integer>();
                    batchlist = new ArrayList<String>();
                    batchtimelist = new ArrayList<String>();

                    for (int i = 0; i < jarray.length() - 1; i++) {
                        JSONObject jrealobj = jarray.getJSONObject(i);
                        batchname = jrealobj.getString("studbatch");
                        subnm = jrealobj.getString("studsub");
                        subfees = jrealobj.getString("studfees");
                        batchtime = jrealobj.getString("studtime");

                        batchlist.add(batchname);
                        sublist.add(subnm);
                        feeslist.add(Integer.parseInt(subfees));
                        batchtimelist.add(batchtime);
                    }
                    return true;
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            pg.dismiss();
            if (result == false) {
                Log.d("message: ", "> No network");
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            } else {
                showSelectSubjectDialog();
            }
        }

    }

    protected void showSelectSubjectDialog() {
        // TODO Auto-generated method stub
        boolean[] checkedbatch = new boolean[batchlist.size()];
        int count = batchlist.size();

        mSelectedItems = new ArrayList<Integer>();
//      for(int i = 0; i < count; i++)
//          checkedDept[i] = selectedCourse.contains(crslist.get(i));

        DialogInterface.OnMultiChoiceClickListener subDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    // if the user checked the item, add it to the selected items
                    mSelectedItems.add(which);
                } else {
                    s_batchname.setText("Tap to select Student Batch(s)");
                }
//                if (mSelectedItems.contains(which)) {
//                    // else if the item is already in the array, remove it
//                    mSelectedItems.remove(Integer.valueOf(which));
//                }

            }


        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Batch(s) Student want to join");


           builder.setMultiChoiceItems((String[]) batchlist.toArray(new String[0]), checkedbatch, subDialogListener);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                StringBuilder c2 = new StringBuilder();
                finalbatchlist = new ArrayList<String>();
                finalfeeslist = new ArrayList<String>();
                finalsublist = new ArrayList<String>();
                finalbatchtimelist = new ArrayList<String>();

                String selectedIndex = "";
                for (Integer i : mSelectedItems) {
                    selectedIndex += i + ", ";
                    c2.append(batchlist.get(i) + ",");
                    finalbatchlist.add(batchlist.get(i));
//                    s_batchArray = (String[]) finalbatchlist.toArray(new String[0]);
//                   Toast.makeText(getApplicationContext(),sf,Toast.LENGTH_LONG).show();
                }
                Integer[] feesarray = (Integer[]) feeslist.toArray(new Integer[0]);


                for (Integer i : mSelectedItems) {
                    totalfees += feesarray[i];
                    String fees;
                    fees = feeslist.get(i).toString();
                    finalfeeslist.add(fees);
//                    s_feesArray = (String[]) finalfeeslist.toArray(new String[0]);
                }

                for (Integer i : mSelectedItems) {
                    String sub;
                    sub = sublist.get(i);
                    finalsublist.add(sub);
//                    s_subArray  = (String[]) finalsublist.toArray(new String[0]);
                }
                for (Integer i : mSelectedItems) {
                    String btime;
                    btime = batchtimelist.get(i);
                    finalbatchtimelist.add(btime);
//                    s_batchtimeArray = (String[]) finalbatchtimelist.toArray(new String[0]);
                }
                dialog.dismiss();
//
                s_subject = c2.toString();
                s_batchname.setText(s_subject);
                int c = 0;
                for (int i = 0; i < finalsublist.size(); i++) {
                    c++;
                }
                a = c;
                noOfData = a.toString();
                s_totalfees = totalfees.toString();//final fees to be stored
                tvtotalfees.setText("Total fees is: " + s_totalfees);

            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }// end of dept dialog


    public class StudentRegAsynk extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }


        @Override
        protected String doInBackground(String... params) {
            s_batchArray = (String[]) finalbatchlist.toArray(new String[0]);
            s_feesArray = (String[]) finalfeeslist.toArray(new String[0]);
            s_subArray = (String[]) finalsublist.toArray(new String[0]);
            s_batchtimeArray = (String[]) finalbatchtimelist.toArray(new String[0]);

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("sname1", params[1]));
            nameValuePairList.add(new BasicNameValuePair("sparnm1", params[2]));
            nameValuePairList.add(new BasicNameValuePair("sdob1", params[3]));
            nameValuePairList.add(new BasicNameValuePair("sgen1", params[4]));
            nameValuePairList.add(new BasicNameValuePair("sphoto1", params[5]));
            nameValuePairList.add(new BasicNameValuePair("sphotocode", params[6]));
            nameValuePairList.add(new BasicNameValuePair("semailid1", params[7]));
            nameValuePairList.add(new BasicNameValuePair("spass1", params[8]));
            nameValuePairList.add(new BasicNameValuePair("sclass1", params[9]));
            nameValuePairList.add(new BasicNameValuePair("slastyrper1", params[10]));
            nameValuePairList.add(new BasicNameValuePair("smedium1", params[11]));
            nameValuePairList.add(new BasicNameValuePair("scourse1", params[12]));
//            nameValuePairList.add(new BasicNameValuePair("ssubject1", params[13]));
            nameValuePairList.add(new BasicNameValuePair("sdoadm1", params[13]));
            nameValuePairList.add(new BasicNameValuePair("sfeespaid1", params[14]));
            nameValuePairList.add(new BasicNameValuePair("sfeesremaining1", params[15]));
            nameValuePairList.add(new BasicNameValuePair("stotalfees1", params[16]));
            nameValuePairList.add(new BasicNameValuePair("snextfeesdate", params[17]));
            nameValuePairList.add(new BasicNameValuePair("schoolnm1", params[18]));
            nameValuePairList.add(new BasicNameValuePair("scno1", params[19]));
            nameValuePairList.add(new BasicNameValuePair("sadd1", params[20]));
            nameValuePairList.add(new BasicNameValuePair("sstudtype", params[21]));

            nameValuePairList.add(new BasicNameValuePair("noOfData1", params[22]));
            nameValuePairList.add(new BasicNameValuePair("batchnm", params[23]));


            for (int i = 0; i < a; i++) {

                nameValuePairList.add(new BasicNameValuePair("sbatchTime1", s_batchtimeArray[i]));
            }
            for (int i = 0; i < a; i++) {

                nameValuePairList.add(new BasicNameValuePair("sbatchName1", s_batchArray[i]));
            }
            for (int i = 0; i < a; i++) {

                nameValuePairList.add(new BasicNameValuePair("sbatchFees1", s_feesArray[i]));
            }

            for (int i = 0; i < a; i++) {

                nameValuePairList.add(new BasicNameValuePair("ssubject1", s_subArray[i]));

            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                output = data;

                Log.d("MSG", output);

                String s = "a";
            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (UnknownHostException e1) {
                output = "6";
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return output;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pg.dismiss();
            if (output.equals("1")) {
                Toast.makeText(getApplicationContext(), "You are registered successfully...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), C_MainActivity.class);
                startActivity(intent);
                finish();

            } else if (output.equals("2")) {
                Toast.makeText(getApplicationContext(), "This student id is already registered...", Toast.LENGTH_LONG).show();
                // Toast.makeText(context,"Network Problem",Toast.LENGTH_LONG).show();
            }
        }


    }
}
