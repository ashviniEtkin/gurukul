package com.etkin.gurukul.counselor;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 20/04/2015.
 */
public class ReportDetail extends Activity {
    String reid,ti,desc,dt;
    String titl,sr;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.reportdetail);
        b=getIntent().getExtras();

        reid=b.getString("reportid");
        // desc=b.getString("descrip");
        // title="Meeting";
        //srno="2";
        //String url = getResources().getString(R.string.noticeDescURL);
        // new noticeTask().execute(url,titl,sr);
        new Reportdetailed().execute(getString(R.string.AdminIp)+"/getReportDesc",reid);

        final RelativeLayout rl=(RelativeLayout)findViewById(R.id.reprel);

    }
    public class Reportdetailed extends AsyncTask<String, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(String... arg0)
        {

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("id", arg0[1]));


            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(arg0[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jobj = new JSONObject(data);

                    JSONArray jarray = jobj.getJSONArray("reportdesc");
                    // for (int i = 0; i < jarray.length() - 1; i++) {
                    JSONObject jrealobj = jarray.getJSONObject(0);

                    dt = jrealobj.getString("r_date");
                    ti=jrealobj.getString("r_title");
                    desc=jrealobj.getString("r_desc");

                    //}
                    //adapter = new CustomAdapter(getApplicationContext(),list);
                    return true;
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (JSONException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result==false)
            {

                Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else
            {
                EditText d=(EditText)findViewById(R.id.repdtd);
                EditText t=(EditText)findViewById(R.id.reptitl);
                EditText de=(EditText)findViewById(R.id.repd);


                d.setText(dt);
                t.setText(ti);
                de.setText(desc);

                //Toast.makeText(getBaseContext()," "+ nm, Toast.LENGTH_LONG).show();

            }
        }

    }
}
