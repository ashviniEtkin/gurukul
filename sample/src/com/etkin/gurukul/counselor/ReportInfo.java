package com.etkin.gurukul.counselor;

/**
 * Created by Administrator on 20/04/2015.
 */
public class ReportInfo {
    public String date;
    public String title;
    public String reportid;

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        this.date = date;

    }

    public String getReportid() {
        return reportid;
    }

    public void setReportid(String reportid) {
        this.reportid = reportid;
    }
}

