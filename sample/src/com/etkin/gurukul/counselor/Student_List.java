package com.etkin.gurukul.counselor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.etkin.gurukul.ConnectionDetector;
import com.etkin.gurukul.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/22/2015.
 */
public class Student_List extends ActionBarActivity {

    protected RecyclerView mRecyclerView;
    protected StudentAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Context context;
    ArrayList<Student_Pojo> mDatasetHome = new ArrayList<Student_Pojo>();
    ProgressDialog pb;
    Toolbar toolbar;
    public static ActionBarActivity student_list;
    ConnectionDetector cd;
    public static Student_List context1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_list);

        toolbar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Student List");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        context1=this;
        student_list=this;
        pb=new ProgressDialog(context);
        cd=new ConnectionDetector(getApplicationContext());
        Boolean isInternet = cd.isConnectingToInternet();
        mRecyclerView = (RecyclerView) findViewById(R.id.student_view);
        mLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent=getIntent();
        String gen=intent.getStringExtra("gen");

        String course=intent.getStringExtra("course");
        String Class=intent.getStringExtra("class");

        new StudentList().execute(getString(R.string.AdminIp)+"/GetStudent",gen,course,Class);

    }

    public class StudentList extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<NameValuePair> nameValuePairList=new ArrayList<NameValuePair>(1);

                nameValuePairList.add(new BasicNameValuePair("gender", params[1]));
                nameValuePairList.add(new BasicNameValuePair("course", params[2]));
                nameValuePairList.add(new BasicNameValuePair("sclass", params[3]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                // JSONObject jobj = new JSONObject(data);
                // JSONArray jArray = jobj.getJSONArray("information");
                JSONArray jsonArray=new JSONArray(data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jrealobj = jsonArray.getJSONObject(i);
                    Student_Pojo student_pojo = new Student_Pojo();
                    student_pojo.setId( jrealobj.getString("studid"));
                    student_pojo.setSname(jrealobj.getString("sname"));
                    student_pojo.setPhoto(jrealobj.getString("photo"));
                    student_pojo.setEmail(jrealobj.getString("email"));
                    student_pojo.setSclass(jrealobj.getString("sclass"));
                    student_pojo.setFeesPaid(jrealobj.getString("feesPaid"));
                    student_pojo.setContact(jrealobj.getString("contact"));
                    student_pojo.setAddress(jrealobj.getString("address"));
                    student_pojo.setTotalFees(jrealobj.getString("totalFees"));
                    student_pojo.setRollNo(jrealobj.getString("rollNo"));
                    mDatasetHome.add(student_pojo);
                }

            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;

            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            mAdapter=new StudentAdapter(Student_List.this,mDatasetHome);
            mRecyclerView.setAdapter(mAdapter);
        }
    }



    public void showProgressDialog() {

        pb.setMessage("Loading data....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();
    }

    public void dismissProgressDialog() {
        pb.dismiss();
    }



}
