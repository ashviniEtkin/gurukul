package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 7/25/2015.
 */
public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.AttendViewHolder> {


    private ArrayList<Attendance_Pojo> resultData;
    public static Context con;
    ProgressDialog pb;
    String classes,courses;

    public AttendanceAdapter(){

    }

    public AttendanceAdapter(Context context,ArrayList<Attendance_Pojo> resultData) {
        //this.cardData = cardData;
        this.resultData=resultData;
        con=context;
        pb = new ProgressDialog(con);

    }

    @Override
    public AttendanceAdapter.AttendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.attendance_row, parent, false);
        return new AttendViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(AttendanceAdapter.AttendViewHolder holder, int i) {
        final Attendance_Pojo list_pojo=resultData.get(i);


        holder.date.setText(list_pojo.getDate());


    }

    @Override
    public int getItemCount() {
        return resultData.size();
    }

    public static class AttendViewHolder extends RecyclerView.ViewHolder {

        protected TextView date,studId;
        private Context context;

        public AttendViewHolder(View itemView,final Context context) {
            super(itemView);
            this.context=context;
            date=(TextView)itemView.findViewById(R.id.date);



        }
    }
}
