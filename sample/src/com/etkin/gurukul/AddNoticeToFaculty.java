package com.etkin.gurukul;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class AddNoticeToFaculty extends ActionBarActivity {

    EditText title,description;
    Button send;
    ProgressDialog pg;
    String output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice_to_faculty);
        title= (EditText) findViewById(R.id.facultyTitle);
        description= (EditText) findViewById(R.id.facultyDescription);
        send= (Button) findViewById(R.id.send);

        pg = new ProgressDialog(this);
        pg.setTitle("Please wait");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage("Data is loading....");

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NoticeToFacultyAsync().execute(getString(R.string.AdminIp)+"/NoticeToFaculty","",title.getText().toString().trim(),description.getText().toString().trim());
            }
        });
    }




    public  class NoticeToFacultyAsync extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
            nameValuePairList.add(new BasicNameValuePair("noticedate", params[1]));
            nameValuePairList.add(new BasicNameValuePair("noticetitle", params[2]));
            nameValuePairList.add(new BasicNameValuePair("noticedesc", params[3]));


            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);

                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                output = data;

            }

            catch(ClientProtocolException e1)
            {
                e1.printStackTrace();
            }
            catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            catch (UnknownHostException e1)
            {
                output = "6";
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pg.dismiss();
            if(output.equals("1"))
            {
                Toast.makeText(getApplicationContext(), "Notice send successfully to faculty ...", Toast.LENGTH_LONG).show();
                finish();
            }
            else
            {

                Toast.makeText(getApplicationContext(),"Network Problem",Toast.LENGTH_LONG).show();
            }


        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_notice_to_faculty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
