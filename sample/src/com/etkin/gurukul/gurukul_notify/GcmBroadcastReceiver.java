package com.etkin.gurukul.gurukul_notify;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Created by Administrator on 2/1/2015.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
//        SharedPreferences sharedPreferences10 = context.getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
//        String login = sharedPreferences10.getString("login", "");
//        if(login.equals("true")) {
            // Explicitly specify that GcmIntentService will handle the intent.
            ComponentName comp = new ComponentName(context.getPackageName(),
                    GcmIntentService.class.getName());
            // Start the service, keeping the device awake while it is launching.
            startWakefulService(context, (intent.setComponent(comp)));
            setResultCode(Activity.RESULT_OK);
//        }
    }
}

