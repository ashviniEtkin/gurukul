package com.etkin.gurukul.gurukul_notify;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import com.etkin.gurukul.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 21/04/2015.
 */
public class Notification {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static String SENDER_ID = "296384354622";
    static final String TAG = "GCMDemo";

    private static GoogleCloudMessaging gcm;
    private static AtomicInteger msgId = new AtomicInteger();
    private static SharedPreferences prefs;
    public static Context context;
    public static String regid, email;


    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private static void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.

                    sendRegistrationIdToBackend();
                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.
                    storeRegistrationId(context, regid);
                    // Persist the regID - no need to register again.

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return (Object) msg;
            }


        }.execute(null, null, null);
    }

    private static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private static SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(Notification.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {

            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {

            return "";
        }

        return registrationId;
    }


    public static void getRegister(Context context1, String emailid) {
        context = context1;
        context = context1;
        email = emailid;
        gcm = GoogleCloudMessaging.getInstance(context);
        regid = getRegistrationId(context);

        if (regid.isEmpty()) {
            //new MyRegister().execute(null,null,null);
            registerInBackground();
        } else {
            sendRegistrationIdToBackend();
        }


    }

    static String[] reg;
    static String message, typ;

    public static void sendNotification(List<String> regid, String msg, String type, Context context)

    {

        reg = (String[]) regid.toArray(new String[0]);
        message = msg;
        typ = type;
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {


                    List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                    nameValuePairList.add(new BasicNameValuePair("noticeto", typ));

                    nameValuePairList.add(new BasicNameValuePair("message", message));
                    for (int i = 0; i < reg.length; i++) {
                        nameValuePairList.add(new BasicNameValuePair("deviceId", reg[i]));
                    }


                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost post = new HttpPost(params[0]);
                    post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                    HttpResponse response = httpClient.execute(post);
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    if (data.equals("1")) {
                        return "success";
                    } else
                        return "Fail";


                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "Fail";

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

            }
        }.execute(context.getString(R.string.AdminIp) + "/SendNotification");


    }


    private static void sendRegistrationIdToBackend() {
        // Your implementation here.

        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {


                    List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                    nameValuePairList.add(new BasicNameValuePair("email", email));

                    nameValuePairList.add(new BasicNameValuePair("gcmid", regid));

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost post = new HttpPost(params[0]);
                    post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                    HttpResponse response = httpClient.execute(post);
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    if (data.equals("1")) {
                        return "success";
                    } else
                        return "Fail";


                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "Fail";

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

            }
        }.execute(context.getString(R.string.AdminIp) + "/Update_stud", regid, email);

    }


}
