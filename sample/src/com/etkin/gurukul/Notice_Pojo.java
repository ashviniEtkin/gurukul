package com.etkin.gurukul;

/**
 * Created by Administrator on 4/9/2015.
 */
public class Notice_Pojo {

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNoticeFor() {
        return noticeFor;
    }

    public void setNoticeFor(String noticeFor) {
        this.noticeFor = noticeFor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String srNo;
    public String date;
    public String noticeFor;
    public String title;
    public String description;
}
