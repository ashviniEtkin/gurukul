package com.etkin.gurukul.menu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.etkin.gurukul.Brochures;
import com.etkin.gurukul.Facilities;
import com.etkin.gurukul.R;
import com.etkin.gurukul.ConnectionDetector;
import com.etkin.gurukul.ContactUs;
import com.etkin.gurukul.MainActivity;
import com.etkin.gurukul.Toppers;
import com.etkin.gurukul.counselor.C_LoginActivity;
import com.etkin.gurukul.faculty.FacultyLoginActivity;
import com.etkin.gurukul.student.Result;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.etkin.gurukul.gurukul_notify.Notification;


public class MenuActivity extends FragmentActivity implements View.OnClickListener{

    private ResideMenu resideMenu;
    private MenuActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemProfile;
    private ResideMenuItem itemCalendar;
    private ResideMenuItem itemSettings;
    private ResideMenuItem itemAbout;
    private ResideMenuItem itemVision;
    private ResideMenuItem itemFacility;
    private ResideMenuItem itemCourses;
    Dialog dialog;
    ProgressDialog pb;
    String result;
    EditText text1,text2;
    public static FragmentActivity contextFrag;

    SharedPreferences settings;
    public static final String REG_ID="email";
    public static final String PWD="pwd";
    SharedPreferences prefs;
    String user,pass;

    ConnectionDetector cd;
    String STUDENTS_USER_ID,STUDENTS_PASSWORD;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ImageView share= (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = getString(R.string.msg);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        mContext = this;
        cd=new ConnectionDetector(mContext);
        contextFrag=this;
        setUpMenu();
        if( savedInstanceState == null )
            changeFragment(new HomeFragment());

        pb=new ProgressDialog(mContext);


        
    }

    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.aim_menu1);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
        resideMenu.setScaleValue(0.5f);

        // create menu items;
        itemHome     = new ResideMenuItem(this, R.drawable.icon_home,     "Admin");
        itemProfile  = new ResideMenuItem(this, R.drawable.icon_profile,  "Counsel");
        itemCalendar = new ResideMenuItem(this, R.drawable.icon_profile, "Faculty");
        itemSettings = new ResideMenuItem(this, R.drawable.ic_perm_identity_white_48dp, "Student");
        itemAbout = new ResideMenuItem(this, R.drawable.ic_local_library_white_48dp, "Contact");
        itemVision = new ResideMenuItem(this, R.drawable.ic_thumb_up_white_48dp, "After XII ");
        itemFacility = new ResideMenuItem(this, R.drawable.ic_palette_white_48dp, "Brochure");
        itemCourses = new ResideMenuItem(this, R.drawable.icon_settings, "Toppers");

        itemHome.setOnClickListener(this);
        itemProfile.setOnClickListener(this);
        itemCalendar.setOnClickListener(this);
        itemSettings.setOnClickListener(this);
        itemAbout.setOnClickListener(this);
        itemVision.setOnClickListener(this);
        itemFacility.setOnClickListener(this);
        itemCourses.setOnClickListener(this);


        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemAbout, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemFacility, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemCalendar, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemCourses, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemVision, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);

        // You can disable a direction by setting ->
        // resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        findViewById(R.id.title_bar_right_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemHome){
            //changeFragment(new HomeFragment());
            showAdminLoginDialog(view);
        }else if (view == itemProfile){
           // changeFragment(new ProfileFragment());
            Intent intent=new Intent(getApplicationContext(), C_LoginActivity.class);
            startActivity(intent);
        }else if (view == itemCalendar){
            //changeFragment(new CalendarFragment());
            Intent intent=new Intent(getApplicationContext(), FacultyLoginActivity.class);
            startActivity(intent);
        }else if (view == itemSettings){
           // changeFragment(new SettingsFragment());
            showStudentLoginDialog();
        }else if (view == itemAbout){
            // changeFragment(new SettingsFragment());
            Intent intent=new Intent(getApplicationContext(), ContactUs.class);
            startActivity(intent);
        }else if (view == itemVision){
            // changeFragment(new SettingsFragment());
            Intent intent=new Intent(getApplicationContext(), Facilities.class);
            startActivity(intent);
        }else if (view == itemFacility){
            // changeFragment(new SettingsFragment());
            Intent intent=new Intent(getApplicationContext(), Brochures.class);
            startActivity(intent);
        }else if (view == itemCourses){
            // changeFragment(new SettingsFragment());
            Intent intent=new Intent(getApplicationContext(), Toppers.class);
            startActivity(intent);
        }

        resideMenu.closeMenu();
    }




    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
          //  Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
          //  Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu(){
        return resideMenu;
    }

    private void showAdminLoginDialog(View v) {
        final Context context = this;

        dialog = new Dialog(MenuActivity.this, R.style.PauseDialog);
        dialog.setContentView(R.layout.login_dialog);
        dialog.setTitle("Login here........");
        pb = new ProgressDialog(dialog.getContext());
        Button login_button = (Button) dialog.findViewById(R.id.login);
        Button cancel_button = (Button) dialog.findViewById(R.id.cancel);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet=cd.isConnectingToInternet();
                if(isInternet) {

                    EditText userid, password;
                    userid = (EditText) dialog.findViewById(R.id.user_id);
                    password = (EditText) dialog.findViewById(R.id.password);
                    user = userid.getText().toString().trim();
                    pass = password.getText().toString().trim();
                    new MyAsynck().execute(getString(R.string.AdminIp)+"/AdminLogin", user, pass);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"You seem to be offline",Toast.LENGTH_SHORT).show();

                }
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public class MyAsynck extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog();
        }


        @Override
        protected Boolean doInBackground(String... params) {
            try {

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("username", params[1]));
                nameValuePairList.add(new BasicNameValuePair("password", params[2]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    result = data;
                } else result = "2";

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                result = "2";

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            if (result.equals("1")) {

                SharedPreferences sharedPreferences = getSharedPreferences(MenuActivity.class.getSimpleName(), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("login", "true");
                editor.putString("admin",user);
                editor.commit();

                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Please enter valid user id & password...", Toast.LENGTH_SHORT).show();
            }

        }
    }
    public void showProgressDialog() {


        pb.setMessage("Logging In....");
        pb.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pb.setIndeterminate(true);
        pb.show();


    }


    public void dismissProgressDialog() {


        pb.dismiss();

    }


    private void showStudentLoginDialog() {

        final Context context = this;

        dialog = new Dialog(MenuActivity.this, R.style.PauseDialog);
        dialog.setContentView(R.layout.activity_login_dialog);
        dialog.setTitle("Login here........");
        pb = new ProgressDialog(dialog.getContext());
        Button save = (Button) dialog.findViewById(R.id.login);
        Button btnCancel = (Button) dialog.findViewById(R.id.cancel);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boolean isInternet=cd.isConnectingToInternet();
                if(isInternet) {
                    text1 = (EditText) dialog.findViewById(R.id.user_id);
                    text2 = (EditText) dialog.findViewById(R.id.password);
                    STUDENTS_USER_ID = text1.getText().toString().trim();
                    STUDENTS_PASSWORD = text2.getText().toString().trim();
                    new Students_login().execute(getString(R.string.AdminIp)+"/Stud_Login", STUDENTS_USER_ID, STUDENTS_PASSWORD);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"You seem to be offline",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public class Students_login extends AsyncTask<String, Void, Boolean> {

        private String Register;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);
                nameValuePairList.add(new BasicNameValuePair("semailid", params[1]));
                nameValuePairList.add(new BasicNameValuePair("spassword", params[2]));
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                HttpResponse response = httpClient.execute(post);
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);

                Register = data;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                Register = "2";

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();

            if (Register.equals("1")) {

                prefs = getSharedPreferences(MenuActivity.class.getSimpleName(),Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(REG_ID,text1.getText().toString());
                editor.putString("email",STUDENTS_USER_ID);

                editor.commit();
                Notification.getRegister(contextFrag,STUDENTS_USER_ID);

                Intent in=new Intent(getApplicationContext(),Result.class);
                startActivity(in);
                overridePendingTransition( R.anim.page_in_left, R.anim.page_out_left);

                finish();

            }
            else if (Register.equals("2")) {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            }

            else if (Register.equals("0")){
                Toast.makeText(getApplicationContext(), "Please enter valid user id & password...", Toast.LENGTH_SHORT).show();
            }
            else
        {
            Toast.makeText(getApplicationContext(), "Network failure...", Toast.LENGTH_SHORT).show();

        }
    }
    }

}
