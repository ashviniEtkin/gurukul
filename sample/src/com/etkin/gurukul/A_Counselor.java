package com.etkin.gurukul;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class A_Counselor extends Fragment {

    private int Color;
    ConnectionDetector cd;


    public A_Counselor() {
        // Required empty public constructor
    }

    public void A_Counselor_color(int color){
        Color=color;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root=inflater.inflate(R.layout.fragment_a__counselor, container, false);
        cd=new ConnectionDetector(getActivity());
        RippleView register=(RippleView)root.findViewById(R.id.counselor_register);
        RippleView list=(RippleView)root.findViewById(R.id.counselor_list);

       /* ImageView img=(ImageView)root.findViewById(R.id.img1);
        Glide.with(MainActivity.context).load("http://24.media.tumblr.com/tumblr_mc65brAmH41rexsomo1_400.gif").into(img);*/
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(),"You seem to be offline !",Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_Counselor_Register.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(),"You seem to be offline !",Toast.LENGTH_SHORT).show();
                }else {


                    Intent intent = new Intent(getActivity(), Counselor_List.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });

        return root;
    }


}
