package com.etkin.gurukul;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class A_Reports extends Fragment {

    RippleView Report;
    private int Color;
    ConnectionDetector cd;


    public A_Reports() {
        // Required empty public constructor
    }

    public void A_Report_color(int color){
        Color=color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_a__reports, container, false);
        cd=new ConnectionDetector(getActivity());
        Report=(RippleView)root.findViewById(R.id.report);
        Report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(), "You seem to be offline !", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getActivity(), A_Report_List.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });


        return root;
    }


}
