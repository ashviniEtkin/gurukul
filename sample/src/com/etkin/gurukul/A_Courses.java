package com.etkin.gurukul;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class A_Courses extends Fragment {

    private int Color;
    ConnectionDetector cd;


    public A_Courses() {
        // Required empty public constructor
    }

    public void A_Courses_color(int color){
        Color=color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_a__courses, container, false);
        cd=new ConnectionDetector(getActivity());
        RippleView NewClass=(RippleView)root.findViewById(R.id.newClass);
        RippleView UpdateCourse=(RippleView)root.findViewById(R.id.updateCourse);
        RippleView NewBatch=(RippleView)root.findViewById(R.id.newBatch);

        NewClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(), "You seem to be offline !", Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_NewClass.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });

        UpdateCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(),"You seem to be offline !",Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_Course_List.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });

        NewBatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isInternet = cd.isConnectingToInternet();
                if (!isInternet)
                {
                    Toast.makeText(getActivity(),"You seem to be offline !",Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(getActivity(), A_NewBatch.class);
                    intent.putExtra("color", Color);
                    startActivity(intent);
                }
            }
        });

        return root;
    }


}
